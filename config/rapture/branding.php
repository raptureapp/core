<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Theme Color
    |--------------------------------------------------------------------------
    |
    | What is the base hue used for interactive elements?
    |
    */
    'color' => 'green',

    /*
    |--------------------------------------------------------------------------
    | Logo (Light Mode)
    |--------------------------------------------------------------------------
    |
    | The logo to use in light mode.
    |
    */
    'logo_light' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo (Dark Mode)
    |--------------------------------------------------------------------------
    |
    | The logo to use in dark mode.
    |
    */
    'logo_dark' => '',

    /*
    |--------------------------------------------------------------------------
    | Dark Mode
    |--------------------------------------------------------------------------
    |
    | Determines how dark mode is activated outside of production.
    | Options "auto", "on", "off"
    |
    */
    'darkmode' => 'auto',

    /*
    |--------------------------------------------------------------------------
    | Theme Selection
    |--------------------------------------------------------------------------
    |
    | Can the user change the theme to their own preference?
    | Options "true", "false"
    |
    */
    'dark_override' => true,
];
