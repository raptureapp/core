const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    content: [
        '../../../vendor/**/*.blade.php',
    ],
    theme: {
        extend: {
            fontFamily: {
                sans: ['Inter', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                'primary': {
                    '50': '#f4fbf2',
                    '100': '#e5f6e2',
                    '200': '#ccecc6',
                    '300': '#a4db9a',
                    '400': '#74c266',
                    '500': '#57b847',
                    '600': '#3c8930',
                    '700': '#326c29',
                    '800': '#2b5625',
                    '900': '#254720',
                    '950': '#10260d',
                },
            }
        },
    },
    variants: {},
    plugins: [
        require('@tailwindcss/forms'),
        require('@tailwindcss/typography'),
    ],
    corePlugins: {
        textOpacity: false,
    },
}
