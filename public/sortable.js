document.addEventListener('livewire:init', () => {
    Livewire.directive('sortable', ({ el, directive, component, cleanup }) => {
        if (directive.modifiers.length > 0) {
            return;
        }

        Array.from(document.querySelectorAll('[wire\\:sortable], [wire\\:sortable\\.target]')).forEach((group) => {
            if (group.closest('[wire\\:sortable]').getAttribute('wire:sortable') !== directive.method) {
                return;
            }

            Sortable.create(group, {
                group: directive.method,
                emptyInsertThreshold: 2,
                scroll: true,
                forceFallback: true,
                fallbackTolerance: 10,
                draggable: '[wire\\:sortable\\.item]',
                filter: '[wire\\:sortable\\.ignore]',
                preventOnFilter: false,
                onEnd: (event) => {
                    if (event.to === event.from && event.oldIndex === event.newIndex) {
                        return;
                    }

                    let items = [];
                    let nodes;
                    let indexes = {};

                    if (group.closest('[wire\\:sortable\\.container]')) {
                        nodes = group.closest('[wire\\:sortable\\.container]').querySelectorAll('[wire\\:sortable\\.item]')
                    } else {
                        nodes = group.closest('[wire\\:sortable]').querySelectorAll('[wire\\:sortable\\.item]')
                    }

                    Array.from(nodes).forEach((el) => {
                        if (el.closest('[wire\\:sortable]').getAttribute('wire:sortable') !== directive.method) {
                            return;
                        }

                        let parent = null;
                        let index = 0;

                        if (el.closest('[wire\\:sortable\\.target]')) {
                            parent = el.closest('[wire\\:sortable\\.target]').getAttribute('wire:sortable.target');
                            index = parent;
                        }

                        if (indexes.hasOwnProperty(index)) {
                            indexes[index] = indexes[index] + 1;
                        } else {
                            indexes[index] = 1;
                        }

                        let dataSet = {
                            order: indexes[index],
                            value: el.getAttribute('wire:sortable.item'),
                            parent: parent,
                        };

                        if (el.closest('[wire\\:sortable\\.data]')) {
                            dataSet = Object.assign(dataSet, JSON.parse(el.closest('[wire\\:sortable\\.data]').getAttribute('wire:sortable.data')));
                        }

                        items.push(dataSet);
                    });

                    component.$wire.call(directive.method, items);
                    component.$wire.$refresh();
                },
            });
        });
    });
});
