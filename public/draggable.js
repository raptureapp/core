document.addEventListener('livewire:init', () => {
    Livewire.directive('draggable', ({ el, directive, component, cleanup }) => {
        if (directive.modifiers.length > 0) {
            return;
        }

        Array.from(document.querySelectorAll('[wire\\:draggable]')).forEach((group) => {
            let groupName = directive.method;

            if (group.hasAttribute('wire:draggable.target')) {
                groupName = group.getAttribute('wire:draggable.target');
            }

            Sortable.create(group, {
                group: {
                    name: groupName,
                    pull: 'clone',
                },
                emptyInsertThreshold: 2,
                scroll: true,
                forceFallback: true,
                fallbackTolerance: 10,
                draggable: '[wire\\:draggable\\.item]',
                filter: '[wire\\:draggable\\.ignore]',
                preventOnFilter: false,
                sort: false,
                onEnd: (event) => {
                    if (event.to === event.from && event.oldIndex === event.newIndex) {
                        return;
                    }

                    const type = event.item.getAttribute('wire:draggable.item');
                    let parent = null;

                    if (event.to.hasAttribute('wire:sortable.target')) {
                        parent = event.to.getAttribute('wire:sortable.target');
                    } else if (event.to.closest('[wire\\:sortable\\.target]')) {
                        parent = event.to.closest('[wire\\:sortable\\.target]').getAttribute('wire:sortable.target');
                    }

                    event.item.remove();

                    component.$wire.call(directive.method, type, event.newIndex, parent);
                },
            });
        });
    });
});
