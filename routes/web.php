<?php

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\Core\Controllers')
    ->group(function () {
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('dashboard/set/{dashboard:slug}', 'DashboardController@show')->name('dashboard.show');
        Route::get('upload/preview/{path}', 'UploadPreviewController')->name('upload.preview');
    });

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\Core\Controllers')
    ->prefix('dashboard')
    ->name('dashboard.')
    ->group(function () {
        Route::get('account', 'AccountSettingsController@index')->name('account.index');
        Route::post('account', 'AccountSettingsController@update')->name('account.update');
        Route::get('branding', 'BrandingController')->name('branding');
    });
