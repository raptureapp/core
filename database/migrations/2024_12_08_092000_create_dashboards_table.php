<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Rapture\Core\Models\Menu;

class CreateDashboardsTable extends Migration
{
    public function up()
    {
        Schema::create('dashboards', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id')->index();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->text('widgets')->nullable();
            $table->boolean('is_default')->default(false);
            $table->unsignedInteger('display_order')->default(0);
            $table->timestamps();
        });

        Menu::where('route', 'dashboard')->update([
            'namespaces' => ['!dashboard', 'dashboard/set'],
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('dashboards');
    }
}
