<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Rapture\Keeper\Models\Permission;

class AddDashboardPermissions extends Migration
{
    public function up()
    {
        Permission::create([
            'package' => 'rapture/core',
            'keyname' => 'core.branding',
            'group' => 'dashboard',
            'description' => 'Modify the dashboard branding',
            'priority' => 0,
            'active' => true,
        ]);
    }

    public function down()
    {
        Permission::where('keyname', 'core.branding')->delete();
    }
}
