<?php

namespace Rapture\Core\Seeder;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $timestamp = Carbon::now();

        DB::table('users')->insert([
            'name' => 'Shout',
            'lname' => 'Media',
            'email' => 'info@havenworkplace.com',
            'password' => bcrypt('password'),
            'created_at' => $timestamp,
            'updated_at' => $timestamp,
        ]);
    }
}
