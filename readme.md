# Rapture

Simple starting dashboard that can be expanded off of to create powerful applications.

## Requirements

- Laravel 10+

## Getting Started

* Add the following snippet to your composer.json

```
    "repositories": [
        {
            "type": "composer",
            "url": "https://repo.rapture.dev"
        }
    ]
```

* Require this package `composer require rapture/core`
* Run the installer `php artisan rapture:install`
* Set up Scout by adding `SCOUT_DRIVER=database` to your `.env`
* If you want to keep all your assets current after each update you can add the following to your composer.json

```
"scripts": {
    "post-update-cmd": [
        "@php artisan rapture:update"
    ]
}
```

### Seeding the database

You may add a default user account by hand by running `php artisan db:seed --class="Rapture\Core\Seeder\UsersTableSeeder"`. Alternately, you may add the `--admin` option to the base install to have the seeder run for you.

### Default user account

* Email: **info@havenworkplace.com**
* Password: **password**

## Compiling Assets

```
postcss ./resources/assets/css/core.css -o ./public/css/core.css
```

## Roadmap

- Instructions for compiling and purging assets against packages
  - Deploy assets from core
