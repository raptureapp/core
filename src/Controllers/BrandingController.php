<?php

namespace Rapture\Core\Controllers;

use App\Http\Controllers\Controller;

class BrandingController extends Controller
{
    public function __invoke()
    {
        return view('rapture::branding');
    }
}
