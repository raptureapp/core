<?php

namespace Rapture\Core\Controllers;

use App\Http\Controllers\Controller;
use Rapture\Core\Models\Dashboard;

class DashboardController extends Controller
{
    public function index()
    {
        return view('rapture::dashboard.index', [
            'key' => 'dashboard',
        ]);
    }

    public function show(Dashboard $dashboard)
    {
        return view('rapture::dashboard.index', [
            'key' => $dashboard->slug,
        ]);
    }
}
