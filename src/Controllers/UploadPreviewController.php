<?php

namespace Rapture\Core\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\File;

class UploadPreviewController extends Controller
{
    public function __invoke(string $path)
    {
        $path = storage_path('app/' . base64_decode($path));
        $file = new File($path);

        if (in_array($file->getExtension(), ['pdf', 'jpg', 'jpeg', 'png', 'gif', 'webp'])) {
            return response()
                ->file($path);
        }

        return response()
            ->download($path);
    }
}
