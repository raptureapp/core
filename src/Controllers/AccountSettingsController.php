<?php

namespace Rapture\Core\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Intervention\Image\Laravel\Facades\Image;
use Rapture\Core\Events\MyAccountUpdated;
use Rapture\Hooks\Facades\Hook;
use \DateTimeZone;

class AccountSettingsController extends Controller
{
    public function index()
    {
        $timezones = collect(DateTimeZone::listIdentifiers())->mapWithKeys(function ($zone) {
            return [$zone => $zone];
        });

        return view('rapture::account.index', compact('timezones'));
    }

    public function update(Request $request)
    {
        $user = auth()->user();

        $validatedData = $this->validate($request, [
            'name' => 'required|max:255',
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
            'password' => 'nullable|min:6',
            'avatar' => 'nullable',
        ]);

        if ($request->filled('password')) {
            $user->password = bcrypt($request->input('password'));
            $user->password_changed_at = now();
            $user->password_changed_by = $user->id;
        }

        $user->avatar = $request->input('avatar');

        if ($request->filled('newPhoto')) {
            $image = Image::read($request->input('newPhoto'))
                ->cover(150, 150)
                ->save();

            $path = Storage::putFile('public/avatars', new File($request->input('newPhoto')));
            $user->avatar = Str::after($path, '/avatars/');
        }

        $user->name = $request->input('name');
        $user->lname = $request->input('lname');
        $user->email = $request->input('email');
        $user->timezone = $request->input('timezone');
        $user->save();

        Hook::dispatch('myaccount.updated', new MyAccountUpdated($user));

        return redirect()
            ->route('dashboard.account.index')
            ->with('status', __('rapture::account.update_success'));
    }
}
