<?php

namespace Rapture\Core;

use Illuminate\Support\Facades\Cache;
use Rapture\Core\Models\Menu;
use Rapture\Core\Models\MenuItem;
use Rapture\Keeper\Traits\Installer as PermissionInstaller;

trait Installer
{
    use PermissionInstaller;

    public $parentMenuItem;

    public function addMenu($options)
    {
        $fields = collect($options);

        $update = $this->defaultOptions()
            ->merge($options)
            ->except('route')
            ->toArray();

        $this->parentMenuItem = Menu::updateOrCreate([
            'package' => $this->name,
            'route' => $fields->get('route'),
        ], $update);

        return $this->parentMenuItem;
    }

    public function addSubMenu($options, $parentRoute = null)
    {
        $fields = collect($options);

        if (!is_null($parentRoute)) {
            $parent = Menu::where('route', $parentRoute)->first();

            if (!is_null($parent)) {
                $this->parentMenuItem = $parent;
            }
        }

        if ($this->parentMenuItem) {
            $options['menu_id'] = $this->parentMenuItem->id;
        }

        $update = $this->defaultOptions()
            ->merge($options)
            ->except('route')
            ->toArray();

        return Menu::updateOrCreate([
            'package' => $this->name,
            'route' => $fields->get('route'),
        ], $update);
    }

    private function defaultOptions()
    {
        return collect([
            'menu_id' => null,
            'icon' => null,
            'permission' => null,
        ]);
    }
}
