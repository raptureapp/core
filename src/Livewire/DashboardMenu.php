<?php

namespace Rapture\Core\Livewire;

use Illuminate\Support\Facades\Route;
use Livewire\Component;
use Rapture\Core\Models\Dashboard;
use Rapture\Core\Models\Menu;
use Rapture\Hooks\Facades\Filter;

class DashboardMenu extends Component
{
    protected $listeners = [
        'refreshMenu' => '$refresh',
    ];

    public function render()
    {
        $validFilter = function ($menu) {
            if ($menu->type === 'route' && !Route::has($menu->route)) {
                return false;
            }

            return $menu->isAllowed();
        };

        $menus = Menu::dashboard()
            ->top()
            ->active()
            ->withCount('children')
            ->orderBy('position')
            ->orderBy('created_at')
            ->get()
            ->filter($validFilter);

        $children = Menu::dashboard()
            ->whereNotNull('menu_id')
            ->active()
            ->orderBy('position')
            ->orderBy('created_at')
            ->get()
            ->filter($validFilter)
            ->groupBy('menu_id');

        foreach ($menus as $menu) {
            $menu->setRelation('children', $children->get($menu->id, collect())->values());
        }

        foreach ($children as $group) {
            foreach ($group as $child) {
                $child->setRelation('children', $children->get($child->id, collect())->values());
            }
        }

        $mainView = Filter::dispatch('dashboard.menu.main', 'rapture::livewire.menu');
        $itemView = Filter::dispatch('dashboard.menu.item', 'rapture::livewire.menu-items');

        $dashboards = Dashboard::where('user_id', auth()->user()->id)
            ->where('is_default', false)
            ->whereNotNull('slug')
            ->orderBy('display_order')
            ->orderBy('id')
            ->get()
            ->map(function ($dashboard) {
                return new Menu([
                    'label' => $dashboard->name,
                    'type' => 'custom',
                    'route' => route('dashboard.show', ['dashboard' => $dashboard]),
                    'namespaces' => ['dashboard/set/' . $dashboard->slug],
                ]);
            });

        if (count($dashboards) > 0) {
            $mainId = $menus->firstWhere('route', 'dashboard')->id;
            $children->put($mainId, $dashboards);
        }

        if (!empty(config('rapture.branding.logo_dark'))) {
            $logo = asset('storage/logos/' . config('rapture.branding.logo_dark'));
        } else {
            $logo = $this->detectLogo();
        }

        if (!empty(config('rapture.branding.logo_light'))) {
            $darkLogo = asset('storage/logos/' . config('rapture.branding.logo_light'));
        } else {
            $darkLogo = $this->detectDarkLogo();
        }

        return view($mainView, [
            'parents' => $menus,
            'children' => $children,
            'itemView' => $itemView,
            'logo' => $logo,
            'darkLogo' => $darkLogo,
        ]);
    }

    private function detectLogo()
    {
        $possibleLogos = ['logo-light.webp', 'logo-light.png', 'logo-light.jpg', 'logo-light.svg', 'logo.webp', 'logo.png', 'logo.jpg', 'logo.svg'];

        foreach ($possibleLogos as $logoFile) {
            if (file_exists(public_path($logoFile))) {
                return asset($logoFile);
            }
        }

        return null;
    }

    private function detectDarkLogo()
    {
        $possibleLogos = ['logo-dark.webp', 'logo-dark.png', 'logo-dark.jpg', 'logo-dark.svg'];

        foreach ($possibleLogos as $logoFile) {
            if (file_exists(public_path($logoFile))) {
                return asset($logoFile);
            }
        }

        return null;
    }
}
