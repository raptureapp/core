<?php

namespace Rapture\Core\Livewire;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Livewire\Attributes\Modelable;
use Livewire\Component;
use Livewire\WithFileUploads;

class FileUpload extends Component
{
    use WithFileUploads;

    public $label = '';
    public $empty = 'Drag and drop here to upload';
    public $hover = 'Drop to upload';
    public $button = 'Select Files';
    public $name = 'files';
    public $folder = 'uploads';
    public $id = '';
    public $single = false;
    public $store = true;
    public $drop = true;
    public $public = false;
    public $files = [];
    public $display = 'list';
    public $icon = 'down-to-line';
    public $uploads = [];
    public $upload;
    public $maxSize;
    public $maxSizeStr = '';
    public $extensions;

    #[Modelable]
    public $value = [];

    public function getExtsProperty()
    {
        if (is_array($this->extensions)) {
            return implode(', ', $this->extensions);
        }

        $exts = explode(',', $this->extensions);
        $exts = array_map('trim', $exts);

        return implode(', ', $exts);
    }

    private function validationRules()
    {
        $rules = [
            'file',
            'max:' . $this->maxSize,
        ];

        if (!empty($this->extensions)) {
            $extensions = $this->extensions;

            if (is_array($extensions)) {
                $extensions = implode(',', $extensions);
            }

            $rules[] = 'mimes:' . $extensions;
            $rules[] = 'extensions:' . $extensions;
        }

        return $rules;
    }

    public function updatedUploads()
    {
        $this->validate([
            'uploads.*' => $this->validationRules(),
        ]);

        foreach ($this->uploads as $upload) {
            $this->storeFile($upload);
        }

        $this->reset('uploads');
    }

    public function updatedUpload()
    {
        $this->validate([
            'upload' => $this->validationRules(),
        ]);

        $this->storeFile($this->upload);
        $this->reset('upload');
    }

    public function storeFile($upload)
    {
        $filename = Str::of($upload->getClientOriginalName())
            ->lower()
            ->replaceMatches('/[^a-z0-9.\-_\s]+/', '')
            ->replace('_', '-')
            ->kebab()
            ->__toString();

        $name = pathinfo($filename, PATHINFO_FILENAME);
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $folder = $this->getPath();

        $tries = 1;

        while (Storage::exists($folder . DIRECTORY_SEPARATOR . $filename)) {
            $tries += 1;
            $filename = $name . '-' . $tries . '.' . $ext;
        }

        $upload->storeAs($folder, $filename);

        if ($this->store) {
            $this->files[] = [
                'file' => $filename,
                'ext' => $ext,
                'name' => $name,
                'type' => $this->fileType($ext),
                'size' => $this->fileSize(Storage::size($folder . DIRECTORY_SEPARATOR . $filename)),
            ];

            $this->value[] = $filename;
        }

        $this->dispatch('file-uploaded', file: $filename);
    }

    public function fileType($ext)
    {
        if (in_array($ext, ['jpg', 'jpeg', 'gif', 'png', 'webp', 'svg'])) {
            return 'image';
        }

        if (in_array($ext, ['pdf', 'doc', 'docx', 'rtf', 'odt', 'wpd', 'xls', 'xlsx', 'csv'])) {
            return 'document';
        }

        if (in_array($ext, ['zip', 'rar', '7zip', 'tar', 'gzip', 'tar.gz', 'tar.bz2'])) {
            return 'archive';
        }

        if (in_array($ext, ['webm', 'mp4', 'mov', 'ogg', 'avi', 'wmv'])) {
            return 'video';
        }

        if (in_array($ext, ['wav', 'mp3', 'aiff', 'flac', 'm4a', 'oga', 'aac', 'wma'])) {
            return 'audio';
        }
    }

    public function fileSize($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = round($bytes / 1073741824, 1) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = round($bytes / 1048576, 1) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = round($bytes / 1024, 1) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' B';
        } else {
            $bytes = '0 B';
        }

        return $bytes;
    }

    public function remove($filename)
    {
        $this->files = array_filter($this->files, function ($value, $key) use ($filename) {
            return $value['file'] !== $filename;
        }, ARRAY_FILTER_USE_BOTH);

        $this->value = array_diff($this->value, [$filename]);

        $this->dispatch('file-removed', file: $filename);
    }

    private function getPath($filename = '')
    {
        $path = [];

        if ($this->public) {
            $path[] = 'public';
        }

        if (!empty($this->folder)) {
            $path[] = $this->folder;
        }

        if (!empty($filename)) {
            $path[] = $filename;
        }

        return implode(DIRECTORY_SEPARATOR, $path);
    }

    public function parseSize($size)
    {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size);
        $size = preg_replace('/[^0-9\.]/', '', $size);

        if ($unit) {
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }

        return round($size);
    }

    public function maxUploadSize()
    {
        $post_max_size = $this->parseSize(ini_get('post_max_size'));
        $upload_max = $this->parseSize(ini_get('upload_max_filesize'));
        $max_size = 0;

        if ($post_max_size > 0) {
            $max_size = $post_max_size;
        }

        if ($upload_max > 0 && $upload_max < $max_size) {
            $max_size = $upload_max;
        }

        if ($this->maxSize && $this->maxSize < $max_size) {
            $max_size = $this->maxSize * 1024;
        }

        $this->maxSize = round($max_size / 1024, 1);
        $this->maxSizeStr = $this->fileSize($max_size);
    }

    public function mount()
    {
        $this->maxUploadSize();

        $files = old($this->name, $this->value);

        $this->files = [];

        if (empty($this->id)) {
            $this->id = Str::of($this->name)->kebab();
        }

        if ($this->single) {
            if ($this->label === 'Files') {
                $this->label = 'File';
            }

            if ($this->button === 'Select Files') {
                $this->button = 'Select File';
            }

            if ($this->name === 'files') {
                $this->name = 'file';
            }
        }

        if (!empty($files)) {
            foreach ($files as $file) {
                $name = pathinfo($file, PATHINFO_FILENAME);
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $path = $this->getPath($file);

                if (!Storage::exists($path)) {
                    continue;
                }

                $this->files[] = [
                    'file' => $file,
                    'ext' => $ext,
                    'name' => $name,
                    'type' => $this->fileType($ext),
                    'size' => $this->fileSize(Storage::size($path)),
                ];
            }
        }
    }

    public function render()
    {
        $previews = [];

        if ($this->display === 'preview' && !$this->public) {
            foreach ($this->value as $file) {
                $previews[$file] = Storage::temporaryUrl(base64_encode($this->folder . '/' . $file), now()->addMinutes(5));
            }
        }

        return view('rapture::livewire.fileupload', compact('previews'));
    }
}
