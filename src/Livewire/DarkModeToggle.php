<?php

namespace Rapture\Core\Livewire;

use Livewire\Component;
use Rapture\Users\Models\UserMeta;

class DarkModeToggle extends Component
{
    public $theme = 'auto';

    public function updatedTheme()
    {
        UserMeta::updateOrCreate([
            'user_id' => auth()->user()->id,
            'meta_key' => 'theme-color',
        ], [
            'meta_value' => $this->theme,
        ]);

        cache(['theme.' . auth()->user()->id => $this->theme]);
    }

    public function mount()
    {
        $this->theme = cache('theme.' . auth()->user()->id);
    }

    public function render()
    {
        return view('rapture::livewire.dark-mode-toggle');
    }
}
