<?php

namespace Rapture\Core\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;

class UserAvatar extends Component
{
    use WithFileUploads;

    public $photo;
    public $avatar;
    public $preview;
    public $fallback;

    public function deletePhoto()
    {
        $this->preview = null;
        $this->photo = null;
        $this->avatar = null;
    }

    public function render()
    {
        return view('rapture::livewire.avatar');
    }
}
