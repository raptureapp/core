<?php

namespace Rapture\Core\Livewire;

use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class BrandingControls extends Component
{
    public $color = 'green';
    public $lightLogo = [];
    public $darkLogo = [];
    public $theme = '';
    public $allowUserSelection = true;

    public function setColor($color)
    {
        $this->color = $color;

        $branding = config('rapture.branding');
        $branding['color'] = $this->color;

        $this->updateConfig($branding);
        $this->redirectRoute('dashboard.branding');
    }

    public function setTheme($theme)
    {
        $this->theme = $theme;

        $branding = config('rapture.branding');
        $branding['darkmode'] = $this->theme;

        $this->updateConfig($branding);
    }

    public function updatedAllowUserSelection($value)
    {
        $this->allowUserSelection = $value;

        $branding = config('rapture.branding');
        $branding['dark_override'] = $this->allowUserSelection;

        $this->updateConfig($branding);
    }

    public function updatingLightLogo()
    {
        if (empty($this->lightLogo)) {
            return;
        }

        Storage::delete('public/logos/' . $this->lightLogo[0]);
    }

    public function updatingDarkLogo()
    {
        if (empty($this->darkLogo)) {
            return;
        }

        Storage::delete('public/logos/' . $this->darkLogo[0]);
    }

    public function updatedLightLogo()
    {
        $branding = config('rapture.branding');

        if (!empty($this->lightLogo)) {
            $branding['logo_light'] = $this->lightLogo[0];
        } else {
            $branding['logo_light'] = '';
        }

        $this->updateConfig($branding);
        $this->dispatch('refreshMenu');
    }

    public function updatedDarkLogo()
    {
        $branding = config('rapture.branding');

        if (!empty($this->darkLogo)) {
            $branding['logo_dark'] = $this->darkLogo[0];
        } else {
            $branding['logo_dark'] = '';
        }

        $this->updateConfig($branding);
        $this->dispatch('refreshMenu');
    }

    private function updateConfig($branding)
    {
        file_put_contents(config_path('rapture/branding.php'), '<?php' . PHP_EOL . PHP_EOL . 'return ' . varExport($branding) . ';' . PHP_EOL);
    }

    public function mount()
    {
        $this->color = config('rapture.branding.color');

        if (!empty(config('rapture.branding.logo_light'))) {
            if (!Storage::exists('public/logos/' . config('rapture.branding.logo_light'))) {
                $branding = config('rapture.branding');
                $branding['logo_light'] = '';

                $this->updateConfig($branding);
            } else {
                $this->lightLogo = [config('rapture.branding.logo_light')];
            }
        }

        if (!empty(config('rapture.branding.logo_dark'))) {
            if (!Storage::exists('public/logos/' . config('rapture.branding.logo_dark'))) {
                $branding = config('rapture.branding');
                $branding['logo_dark'] = '';

                $this->updateConfig($branding);
            } else {
                $this->darkLogo = [config('rapture.branding.logo_dark')];
            }
        }

        $this->theme = config('rapture.branding.darkmode');
        $this->allowUserSelection = config('rapture.branding.dark_override');
    }

    public function render()
    {
        return view('rapture::livewire.branding-controls', [
            'colors' => [
                'red' => 'Red',
                'orange' => 'Orange',
                'amber' => 'Amber',
                'yellow' => 'Yellow',
                'lime' => 'Lime',
                'green' => 'Green',
                'emerald' => 'Emerald',
                'teal' => 'Teal',
                'cyan' => 'Cyan',
                'sky' => 'Sky',
                'blue' => 'Blue',
                'indigo' => 'Indigo',
                'violet' => 'Violet',
                'purple' => 'Purple',
                'fuchsia' => 'Fuchsia',
                'pink' => 'Pink',
                'rose' => 'Rose',
            ],
        ]);
    }
}
