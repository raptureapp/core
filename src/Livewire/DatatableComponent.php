<?php

namespace Rapture\Core\Livewire;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Attributes\Url;
use Livewire\Component;
use Livewire\WithPagination;
use Rapture\Hooks\Facades\Filter;
use Rapture\Users\Models\UserMeta;

class DatatableComponent extends Component
{
    use WithPagination, AuthorizesRequests;

    public $table = '';
    public $defaultDirection = 'asc';
    public $defaultSort;
    public $defaultScope = '';
    public $userPreferences = null;
    public $primaryKey = 'id';
    public $selected = [];
    public $pendingFilters = [];
    public $perPage = 25;
    public $currentPage = [];
    public $customPageSize;
    public $searchable = false;
    public $searchTerm = '';
    public $showEmptyScopes = false;
    public $showLabels = false;
    public $authorized = false;
    public $title = '';

    #[Url(history: true)]
    public $filter = [];

    #[Url(history: true)]
    public $sort = '';

    #[Url(history: true)]
    public $direction = '';

    #[Url(history: true)]
    public $search = '';

    #[Url(history: true)]
    public $scope = '';

    private $columns = [];

    public function columns()
    {
        return [];
    }

    public function actions()
    {
        return [];
    }

    public function operations()
    {
        return [];
    }

    public function scopes()
    {
        return [];
    }

    public function tableName()
    {
        return $this->table;
    }

    private function updatePreference($value)
    {
        $this->userPreferences->meta_value = $value;
        $this->userPreferences->save();
    }

    public function toggleVisibility($key)
    {
        $this->columns = $this->filledColumns()->map(function ($column) use ($key) {
            if ($column->is($key)) {
                $column->visible(!$column->isVisible());
            }

            return $column;
        });

        $this->updatePreference($this->columns->map(function ($column) {
            return collect($column)->only(['key', 'visible', 'order'])->toArray();
        })->toJson());
    }

    public function updateOrder($newOrder)
    {
        $order = collect($newOrder)->mapWithKeys(function ($set) {
            return [$set['value'] => $set['order']];
        })->toArray();

        $this->updatePreference($this->filledColumns()->map(function ($column) use ($order) {
            return collect($column)
                ->only(['key', 'visible', 'order'])
                ->put('order', array_key_exists($column->key, $order) ? $order[$column->key] : 0)
                ->toArray();
        })->toJson());
    }

    public function resetColumns()
    {
        $this->updatePreference(null);
    }

    public function collectColumns()
    {
        if (!empty($this->columns)) {
            return $this->columns;
        }

        $this->columns = collect(Filter::dispatch($this->tableName() . '.columns', $this->columns()))
            ->map(function ($column, $key) {
                $column->order($key);
                return $column;
            })
            ->sortBy('order');

        return $this->columns;
    }

    public function orderedColumns()
    {
        return $this->filledColumns()->sortBy('order');
    }

    public function filledColumns()
    {
        $preference = collect();

        if ($this->userPreferences) {
            $preference = collect(json_decode($this->userPreferences->meta_value, true));
        }

        return $this->collectColumns()->map(function ($column) use ($preference) {
            $columnPreference = $preference->first(function ($col) use ($column) {
                return $col['key'] === $column->key;
            });

            if ($columnPreference) {
                $column->visible($columnPreference['visible']);
                $column->order($columnPreference['order']);
            }

            return $column;
        });
    }

    public function getRecordsPerPageProperty()
    {
        return [25, 50];
    }

    public function getHeadingsProperty()
    {
        return $this->orderedColumns()->filter(function ($column) {
            return ($column->isVisible() || $column->isLocked()) && $column->isWithinScope($this->scope);
        });
    }

    public function getColumnSelectionProperty()
    {
        return $this->filledColumns()->reject(function ($column) {
            return $column->isLocked();
        });
    }

    public function getActiveColumnsProperty()
    {
        return $this->orderedColumns()->filter(function ($column) {
            return $column->isVisible();
        });
    }

    public function getFiltersProperty()
    {
        return $this->filledColumns()->filter(function ($column) {
            return $column->isFilterable();
        });
    }

    public function getActionsProperty()
    {
        return collect(Filter::dispatch($this->tableName() . '.actions', $this->actions()))->filter(function ($action) {
            return $action->hasPermission() && $action->isWithinScope($this->scope);
        });
    }

    public function getScopesProperty()
    {
        return collect(Filter::dispatch($this->tableName() . '.scopes', $this->scopes()))->filter(function ($scope) {
            return $scope->hasPermission();
        })->each(function ($scope) {
            $scope->fetchCount($this->baseQuery());
        })->filter(function ($scope) {
            return $scope->count > 0 || $this->showEmptyScopes;
        });
    }

    public function getOperationsProperty()
    {
        return collect(Filter::dispatch($this->tableName() . '.operations', $this->operations()))->filter(function ($operation) {
            return $operation->hasPermission() && $operation->isWithinScope($this->scope);
        });
    }

    public function getResultsProperty()
    {
        $results = $this->createQuery()->paginate($this->perPage);

        $this->currentPage = $results->pluck($this->primaryKey)->toArray();

        return $results;
    }

    public function sortingColumn()
    {
        return $this->filledColumns()->first(function ($column) {
            return $column->key === $this->sort;
        });
    }

    public function baseQuery()
    {
        return Filter::dispatch($this->tableName() . '.query', $this->query());
    }

    public function createQuery()
    {
        $query = $this->baseQuery();

        if ($this->searchable && !empty($this->search)) {
            return $this->searchedQuery($query);
        }

        $query = $this->scopedQuery($query);
        $query = $this->extendedQuery($query);
        $query = $this->filteredQuery($query);

        return $query->orderBy($this->sortingColumn()->sortQuery($query), $this->direction);
    }

    public function searchedQuery($query)
    {
        return $query->getModel()
            ->search($this->search)
            ->query(function ($query) {
                $query = $this->scopedQuery($query);
                $query = $this->extendedQuery($query);
                $query = $this->filteredQuery($query);

                $query->orderBy($this->sortingColumn()->sortQuery($query), $this->direction);
            });
    }

    public function filteredQuery($query)
    {
        if (empty($this->filter)) {
            return $query;
        }

        $filters = $this->filledColumns()->filter(function ($column) {
            return array_key_exists($column->key, $this->filter) && !empty($this->filter[$column->key]);
        });

        foreach ($filters as $filter) {
            $filter->processFilter($query, $this->filter[$filter->key]);
        }

        return $query;
    }

    public function extendedQuery($query)
    {
        $activeColumns = $this->headings->filter(function ($column) {
            return $column->hasQuery();
        });

        foreach ($activeColumns as $column) {
            $column->processQuery($query);
        }

        return $query;
    }

    public function scopedQuery($query)
    {
        if (empty($this->scope)) {
            return $query;
        }

        $activeScope = $this->scopes->first(function ($scope) {
            return $scope->key === $this->scope;
        });

        if (is_null($activeScope)) {
            return $query;
        }

        $activeScope->processQuery($query);

        return $query;
    }

    public function setPageSize($size)
    {
        $this->perPage = $size;
        $this->setPage(1);

        UserMeta::updateOrCreate([
            'user_id' => auth()->user()->id,
            'meta_key' => $this->tableName() . '.perpage',
        ], [
            'meta_value' => $size,
        ]);
    }

    public function updatedCustomPageSize($value)
    {
        $this->setPageSize($value);
    }

    public function applyFilters()
    {
        $this->filter = collect($this->pendingFilters)->reject(function ($filter) {
            return $filter === '';
        })->toArray();

        $this->setPage(1);
        $this->clearSelection();
    }

    public function applySearch()
    {
        $this->search = $this->searchTerm;
        $this->setPage(1);
        $this->clearSelection();
    }

    public function resetFilters()
    {
        $this->filter = [];
        $this->pendingFilters = [];
    }

    public function sortBy($field)
    {
        $ascending = $this->defaultDirection === 'asc';

        if ($this->sort === $field && $this->direction === $this->defaultDirection) {
            $ascending = !$ascending;
        }

        $this->direction = $ascending ? 'asc' : 'desc';
        $this->sort = $field;
    }

    public function resetSearch()
    {
        $this->searchTerm = '';
        $this->search = '';
    }

    public function applyScope($key)
    {
        $this->scope = $key;
        $this->resetFilters();
        $this->clearSelection();
        $this->resetSearch();
        $this->setPage(1);
    }

    public function mount()
    {
        $this->authorized = auth()->check();

        if ($this->authorized) {
            $this->userPreferences = UserMeta::firstOrCreate([
                'user_id' => auth()->user()->id,
                'meta_key' => $this->tableName(),
            ]);

            $perPage = UserMeta::where([
                'user_id' => auth()->user()->id,
                'meta_key' => $this->tableName() . '.perpage',
            ])->first();

            if ($perPage) {
                $this->perPage = intval($perPage->meta_value);
            }
        }

        $columns = $this->filledColumns();

        $defaultSort = $columns->first(function ($column) {
            return $column->isDefault();
        });

        $primaryKey = $columns->first(function ($column) {
            return $column->isPrimary();
        });

        $defaultScope = $this->scopes->first(function ($scope) {
            return $scope->isDefault();
        });

        if (!is_null($primaryKey)) {
            $this->primaryKey = $primaryKey->key;
        }

        if (!is_null($defaultScope)) {
            $this->defaultScope = $defaultScope->key;
        }

        $this->defaultSort = !is_null($defaultSort) ? $defaultSort->key : $this->primaryKey;
        $this->defaultDirection = !is_null($defaultSort) ? $defaultSort->default : 'asc';

        $this->sort = request()->query('sort', $this->defaultSort);
        $this->direction = request()->query('direction', $this->defaultDirection);
        $this->filter = request()->query('filter', []);
        $this->pendingFilters = request()->query('filter', []);
        $this->searchTerm = request()->query('search', '');
        $this->scope = request()->query('scope', $this->defaultScope);
    }

    public function render()
    {
        return view('rapture::livewire.datatable');
    }

    public function query()
    {
        return null;
    }

    public function paginationView()
    {
        return 'rapture::livewire.pagination';
    }

    public function clearSelection()
    {
        $this->selected = [];
    }

    public function selectAll()
    {
        $this->selected = $this->createQuery()->pluck($this->primaryKey)->toArray();
    }

    public function hasActiveFilter()
    {
        return !empty($this->filter);
    }
}
