<?php

namespace Rapture\Core\Livewire;

use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Rapture\Core\Models\Dashboard;
use Rapture\Packages\Facades\PackageDirectory;
use Rapture\Users\Models\UserMeta;

class DashboardWidgets extends Component
{
    public $slug = 'dashboard';
    public $widgets = [];

    protected $listeners = [
        'refreshWidgets' => 'reload',
    ];

    public function populate()
    {
        $dashboard = Dashboard::where('user_id', auth()->user()->id)
            ->when($this->slug === 'dashboard', fn ($query) => $query->where('is_default', true))
            ->when($this->slug !== 'dashboard', fn ($query) => $query->where('slug', $this->slug))
            ->first();

        if ($this->slug === 'dashboard' && is_null($dashboard)) {
            $dashboard = Dashboard::create([
                'user_id' => auth()->user()->id,
                'is_default' => true,
                'name' => 'Main Dashboard',
                'slug' => 'main-dashboard',
            ]);

            if (empty($dashboard->widgets)) {
                $userWidgets = UserMeta::where([
                    'user_id' => auth()->user()->id,
                    'meta_key' => 'widgets-dashboard',
                ])->first();

                if (!empty($userWidgets->meta_value)) {
                    $dashboard->widgets = $userWidgets->meta_value;
                    $dashboard->save();
                }
            }
        }

        if (empty($dashboard->widgets)) {
            $selection = PackageDirectory::defaultWidgets();
            $keys = $selection;
        } else {
            $selection = json_decode($dashboard->widgets, true);
            $keys = array_map(fn ($item) => $item['id'], $selection);
        }

        $activeSelection = PackageDirectory::widgets()
            ->reduce(fn ($final, $package) => $final->merge($package->widgets), collect())
            ->filter(fn ($widget, $key) => in_array($key, $keys))
            ->map(fn ($widget) => $widget::make())
            ->filter(fn ($widget) => is_null($widget->permission) || Gate::allows($widget->permission, $widget))
            ->filter(fn ($widget) => $widget->condition());

        foreach ($selection as $key) {
            $mainKey = is_array($key) ? $key['id'] : $key;

            if (!$activeSelection->get($mainKey)) {
                continue;
            }

            $widget = $activeSelection->get($mainKey);
            $width = !is_null($widget->minColumns) ? $widget->minColumns : $widget->columns;
            $height = !is_null($widget->minRows) ? $widget->minRows : $widget->rows;

            if (is_array($key)) {
                $width = array_key_exists('w', $key) ? $key['w'] : $width;
                $height = array_key_exists('h', $key) ? $key['h'] : $height;
            }

            $this->widgets[$mainKey] = [
                'columns' => $width,
                'rows' => $height,
            ];
        }
    }

    public function reload()
    {
        $this->reset('widgets');
        $this->populate();
    }

    public function mount()
    {
        $this->populate();
    }

    public function render()
    {
        return view('rapture::livewire.widgets');
    }
}
