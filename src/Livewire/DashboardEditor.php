<?php

namespace Rapture\Core\Livewire;

use Illuminate\Support\Str;
use Livewire\Attributes\Reactive;
use Livewire\Attributes\Validate;
use Livewire\Component;
use Rapture\Core\Models\Dashboard;

class DashboardEditor extends Component
{
    public $dashboard;

    #[Reactive]
    public $isDefault = false;

    #[Validate('required')]
    public $name;

    public function update()
    {
        $this->validate();

        Dashboard::find($this->dashboard)->update([
            'name' => $this->name,
            'slug' => Str::slug($this->name),
        ]);

        $this->dispatch('refreshMenu');
    }

    public function render()
    {
        return view('rapture::livewire.dashboard-editor');
    }
}
