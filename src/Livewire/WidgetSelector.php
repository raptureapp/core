<?php

namespace Rapture\Core\Livewire;

use Illuminate\Support\Facades\Gate;
use Livewire\Attributes\Renderless;
use Livewire\Component;
use Rapture\Core\Models\Dashboard;
use Rapture\Packages\Facades\PackageDirectory;

class WidgetSelector extends Component
{
    public $slug = 'dashboard';
    public $open = false;
    public $tab = 'widgets';
    public $selection = [];
    public $dashboards = [];
    public $editing = null;

    private $packages;

    public function fetchDashboards()
    {
        $this->dashboards = Dashboard::where('user_id', auth()->user()->id)
            ->orderBy('display_order', 'asc')
            ->get()
            ->mapWithKeys(fn($dashboard) => [$dashboard->id => $dashboard]);
    }

    public function updatedOpen()
    {
        $this->reset('selection', 'tab', 'editing');

        if (!$this->open) {
            return;
        }

        $this->fetchDashboards();

        $dashboard = collect($this->dashboards)
            ->when($this->slug === 'dashboard', fn($query) => $query->where('is_default', true))
            ->when($this->slug !== 'dashboard', fn($query) => $query->where('slug', $this->slug))
            ->first();

        $this->editing = $dashboard->id;

        if (empty($dashboard->widgets)) {
            $selection = PackageDirectory::defaultWidgets();
            $keys = $selection;
        } else {
            $selection = json_decode($dashboard->widgets, true);
            $keys = array_map(fn ($item) => $item['id'], $selection);
        }

        $activeSelection = $this->packagesWithWidgets()
            ->reduce(fn ($final, $package) => $final->merge($package->widgets), collect())
            ->filter(fn ($widget, $key) => in_array($key, $keys));

        foreach ($selection as $key) {
            $mainKey = is_array($key) ? $key['id'] : $key;

            if (!$activeSelection->get($mainKey)) {
                continue;
            }

            $widget = $activeSelection->get($mainKey);
            $sizes = $this->getSizes($widget);

            if (is_array($key)) {
                $sizes['w'] = array_key_exists('w', $key) ? $key['w'] : $sizes['w'];
                $sizes['h'] = array_key_exists('h', $key) ? $key['h'] : $sizes['h'];
            }

            $this->selection[$mainKey] = [
                'label' => $widget->label(),
                'sizes' => $sizes,
            ];
        }
    }

    #[Renderless]
    public function storeSelection($results)
    {
        foreach ($results as $result) {
            if (array_key_exists($result['id'], $this->selection) && array_key_exists('w', $result)) {
                $this->selection[$result['id']]['sizes']['w'] = $result['w'];
            }

            if (array_key_exists($result['id'], $this->selection) && array_key_exists('h', $result)) {
                $this->selection[$result['id']]['sizes']['h'] = $result['h'];
            }
        }
    }

    public function store($results)
    {
        Dashboard::find($this->editing)->update([
            'widgets' => json_encode($results),
        ]);

        $this->open = false;
        $this->reset('selection');
        $this->dispatch('refreshWidgets');
    }

    public function remove($key)
    {
        unset($this->selection[$key]);
        $this->dispatch('refreshGrid');
    }

    public function add($key)
    {
        $widget = $this->packagesWithWidgets()
            ->reduce(fn ($final, $package) => $final->merge($package->widgets), collect())
            ->get($key);

        $this->selection[$key] = [
            'label' => $widget->label(),
            'sizes' => $this->getSizes($widget),
        ];

        $this->dispatch('refreshGrid');
    }

    public function getSizes($widget)
    {
        $sizes = [
            'w' => $widget->columns,
            'h' => $widget->rows,
        ];

        if (!is_null($widget->minColumns)) {
            $sizes['min-w'] = $widget->minColumns;
        }

        if (!is_null($widget->maxColumns)) {
            $sizes['max-w'] = $widget->maxColumns;
        }

        if (!is_null($widget->minRows)) {
            $sizes['min-h'] = $widget->minRows;
        }

        if (!is_null($widget->maxRows)) {
            $sizes['max-h'] = $widget->maxRows;
        }

        if (!is_null($widget->minColumns) && !is_null($widget->minRows) && $widget->minColumns === $widget->maxColumns && $widget->minRows === $widget->maxRows) {
            $sizes['no-resize'] = true;
        }

        return $sizes;
    }

    public function render()
    {
        $packages = [];

        if ($this->open) {
            $packages = $this->packagesWithWidgets();
        }

        return view('rapture::livewire.widget-selector', compact('packages'));
    }

    public function packagesWithWidgets()
    {
        if (!empty($this->packages)) {
            return $this->packages;
        }

        $this->packages = PackageDirectory::widgets()->map(function ($package) {
            $package->widgets = collect($package->widgets)
                ->map(fn ($widget) => $widget::make())
                ->filter(fn ($widget) => is_null($widget->permission) || Gate::allows($widget->permission, $widget))
                ->filter(fn ($widget) => $widget->condition());

            return $package;
        });

        return $this->packages;
    }

    public function createDashboard()
    {
        $dashboard = Dashboard::create([
            'user_id' => auth()->user()->id,
        ]);

        $this->dashboards[$dashboard->id] = $dashboard;
    }

    public function deleteDashboard($id)
    {
        Dashboard::find($id)->delete();

        unset($this->dashboards[$id]);

        $this->dispatch('refreshMenu');
    }

    public function setDefault($id)
    {
        Dashboard::where('is_default', true)->update([
            'is_default' => false,
        ]);

        Dashboard::find($id)->update([
            'is_default' => true,
        ]);

        $this->fetchDashboards();

        $this->dispatch('refreshWidgets');
        $this->dispatch('refreshMenu');
    }

    public function openWidgetSelector($id)
    {
        $this->editing = $id;
        $this->tab = 'widgets';
        $this->slug = $this->dashboards[$id]->slug;
        $this->updatedOpen();
    }
}
