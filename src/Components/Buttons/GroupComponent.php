<?php

namespace Rapture\Core\Components\Buttons;

use Illuminate\View\Component;

class GroupComponent extends Component
{
    public function render()
    {
        return view('rapture::components.buttons.group');
    }
}
