<?php

namespace Rapture\Core\Components\Buttons;

use Illuminate\View\Component;

class ButtonComponent extends Component
{
    public function __construct(
        public $size = 'normal',
        public $type = 'button',
        public $href = null,
        public $outline = false,
        public $color = 'default',
        public $icon = null,
        public $iconPosition = 'left',
        public $label = null,
        public $library = 'far',
        public $loading = false,
    ) {
        $this->type = is_null($this->href) ? $this->type : null;

        if ($this->loading) {
            $this->icon = null;
        }

        if ($this->outline) {
            $this->color = 'outline';
        }
    }

    public function render()
    {
        return view('rapture::components.buttons.default');
    }
}
