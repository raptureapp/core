<?php

namespace Rapture\Core\Components\Buttons;

use Illuminate\View\Component;

class GroupButtonComponent extends Component
{
    public function __construct(
        public $active = null,
        public $xActive = null,
        public $size = 'normal',
    ) {
    }

    public function render()
    {
        return view('rapture::components.buttons.group-btn');
    }
}
