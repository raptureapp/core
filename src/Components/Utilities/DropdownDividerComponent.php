<?php

namespace Rapture\Core\Components\Utilities;

use Illuminate\View\Component;

class DropdownDividerComponent extends Component
{
    public function render()
    {
        return view('rapture::components.utilities.dropdown-divider');
    }
}
