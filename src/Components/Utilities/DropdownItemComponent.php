<?php

namespace Rapture\Core\Components\Utilities;

use Illuminate\View\Component;

class DropdownItemComponent extends Component
{
    public function __construct(
        public string $icon = '',
        public string $label = '',
        public string $color = 'normal',
        public string|null $href = null,
        public string|null $type = 'button',
    ) {
        $this->type = is_null($this->href) ? $this->type : null;
    }

    public function render()
    {
        return view('rapture::components.utilities.dropdown-item');
    }
}
