<?php

namespace Rapture\Core\Components\Utilities;

use Illuminate\View\Component;

class DropdownComponent extends Component
{
    public function __construct(
        public $trigger = 'open',
        public $position = 'right',
        public $size = 'w-56',
    ) {
    }

    public function render()
    {
        return view('rapture::components.utilities.dropdown');
    }
}
