<?php

namespace Rapture\Core\Components\Utilities;

use Illuminate\View\Component;

class ExtTypeComponent extends Component
{
    public function __construct(
        public $ext,
    ) {
    }

    public function render()
    {
        return view('rapture::components.utilities.exttype');
    }
}
