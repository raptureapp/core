<?php

namespace Rapture\Core\Components\Utilities;

use Illuminate\View\Component;

class QuickCreateComponent extends Component
{
    public function __construct(
        public $action,
        public $placeholder = null,
        public $name = 'name',
        public $label = null,
        public $icon = null,
    ) {
    }

    public function render()
    {
        return view('rapture::components.utilities.quick-create');
    }
}
