<?php

namespace Rapture\Core\Components\Utilities;

use Illuminate\View\Component;

class DropdownHeadingComponent extends Component
{
    public function __construct(
        public string $label,
    ) {
    }

    public function render()
    {
        return view('rapture::components.utilities.dropdown-heading');
    }
}
