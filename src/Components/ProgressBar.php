<?php

namespace Rapture\Core\Components;

use Illuminate\View\Component;

class ProgressBar extends Component
{
    public function __construct(
        public $progress = 0,
    ) {
        if (is_string($progress)) {
            $this->progress = floatval($progress);
        }

        if ($progress < 1) {
            $this->progress = $progress * 100;
        }
    }

    public function render()
    {
        return view('rapture::components.progress');
    }
}
