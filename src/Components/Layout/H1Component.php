<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\View\Component;

class H1Component extends Component
{
    public function render()
    {
        return view('rapture::components.layout.h1');
    }
}
