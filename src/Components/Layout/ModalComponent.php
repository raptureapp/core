<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\View\Component;

class ModalComponent extends Component
{
    public function __construct(
        public $title = '',
        public $trigger = 'modal',
        public $size = 'normal',
        public $align = 'center',
    ) {
    }

    public function render()
    {
        return view('rapture::components.layout.modal');
    }
}
