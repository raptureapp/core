<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\View\Component;

class H3Component extends Component
{
    public function render()
    {
        return view('rapture::components.layout.h3');
    }
}
