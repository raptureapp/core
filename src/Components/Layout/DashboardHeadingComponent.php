<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\View\Component;

class DashboardHeadingComponent extends Component
{
    public function __construct(
        public $label = '',
        public $noStatus = false,
    ) {
    }

    public function render()
    {
        return view('rapture::components.layout.heading');
    }
}
