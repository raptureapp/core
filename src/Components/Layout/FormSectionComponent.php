<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\View\Component;

class FormSectionComponent extends Component
{
    public function __construct(
        public $title,
        public $desc = '',
        public $wrap = true,
    ) {
    }

    public function render()
    {
        return view('rapture::components.layout.form-section');
    }
}
