<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class TabComponent extends Component
{
    public function __construct(
        public $label,
        public $key = null,
        public $icon = null,
        public $size = 'normal',
        public $badge = null,
    ) {
        if (is_null($this->key)) {
            $this->key = Str::slug($label);
        }
    }

    public function render()
    {
        return view('rapture::components.layout.tab');
    }
}
