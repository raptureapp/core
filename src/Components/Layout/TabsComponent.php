<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Str;
use Illuminate\View\Component;

class TabsComponent extends Component
{
    public function __construct(
        public $key = 'tab',
        public $size = 'normal',
    ) {
    }

    public function render()
    {
        return function (array $data) {
            $tabs = Str::of($data['slot'])
                ->trim()
                ->explode('</button>')
                ->mapWithKeys(function ($tab) {
                    $body = Str::of($tab);

                    if (!$body->contains('tabVariable')) {
                        return [];
                    }

                    $label = $body->between('<span>', '</span>')->toString();
                    $key = $body->betweenFirst('@click="tabVariable = \'', "'")->toString();

                    return [$key => $label];
                });

            $data['slot'] = Str::of($data['slot'])->replace('tabVariable', $this->key)->toHtmlString();
            $data['tabs'] = $tabs;

            return Blade::render('@include("rapture::components.layout.tabs")', $data);
        };
    }
}
