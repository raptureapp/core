<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\View\Component;

class BadgeComponent extends Component
{
    public function __construct(
        public $color = 'blue',
        public $size = 'normal',
    ) {
    }

    public function render()
    {
        return view('rapture::components.layout.badge');
    }
}
