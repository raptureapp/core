<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\View\Component;

class BoxComponent extends Component
{
    public function __construct(
        public string $size = 'normal',
    ) {
    }

    public function render()
    {
        return view('rapture::components.layout.box');
    }
}
