<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\View\Component;

class PanelComponent extends Component
{
    public function __construct(
        public $open = false,
        public $size = 'normal',
    ) {
    }

    public function render()
    {
        return view('rapture::components.layout.panel');
    }
}
