<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\View\Component;

class CardComponent extends Component
{
    public function render()
    {
        return view('rapture::components.layout.card');
    }
}
