<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\View\Component;

class DashboardContainerComponent extends Component
{
    public function __construct(
        public $padding = 'normal',
    ) {
    }

    public function render()
    {
        return view('rapture::components.layout.container');
    }
}
