<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\View\Component;

class DashboardComponent extends Component
{
    public function __construct(
        public $title = '',
    ) {
    }

    public function render()
    {
        return view('rapture::components.layout.dashboard');
    }
}
