<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\View\Component;
use Rapture\Users\Models\UserMeta;

class BlankComponent extends Component
{
    public $theme;

    public function __construct(
        public $title = '',
    ) {
        if (auth()->check() && cache('theme.' . auth()->user()->id) && config('rapture.branding.dark_override')) {
            $this->theme = cache('theme.' . auth()->user()->id);
            return;
        }

        if (config('rapture.branding.darkmode') === 'on') {
            $this->theme = 'dark';
        } elseif (config('rapture.branding.darkmode') === 'off') {
            $this->theme = 'light';
        }

        if (!auth()->check()) {
            return;
        }

        $userPreference = UserMeta::firstOrCreate([
            'user_id' => auth()->user()->id,
            'meta_key' => 'theme-color',
        ]);

        if (config('rapture.branding.dark_override') && !empty($userPreference->meta_value)) {
            $this->theme = $userPreference->meta_value;
        }

        cache(['theme.' . auth()->user()->id => $this->theme]);
    }

    public function render()
    {
        return view('rapture::components.layout.blank');
    }
}
