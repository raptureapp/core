<?php

namespace Rapture\Core\Components\Layout;

use Illuminate\View\Component;

class H2Component extends Component
{
    public function render()
    {
        return view('rapture::components.layout.h2');
    }
}
