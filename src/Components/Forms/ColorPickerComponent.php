<?php

namespace Rapture\Core\Components\Forms;

use Illuminate\View\Component;

class ColorPickerComponent extends Component
{
    public function __construct(
        public $name = '',
        public $value = '',
        public $swatch = true,
        public $align = 'left',
        public $label = null,
        public $required = false,
        public $id = '',
        public $solid = false,
        public $presets = [],
        public $format = 'rgb',
    ) {
    }

    public function render()
    {
        return view('rapture::components.forms.colorpicker');
    }
}
