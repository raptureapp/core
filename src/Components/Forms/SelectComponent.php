<?php

namespace Rapture\Core\Components\Forms;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class SelectComponent extends Component
{
    public function __construct(
        public $name = '',
        public $options = [],
        public $selected = null,
        public $placeholder = null,
        public $value = null,
        public $shape = 'rounded',
        public $prepend = '',
        public $error = false,
        public $inputClass = '',
        public $label = null,
        public $required = false,
        public $id = '',
    )
    {
        $this->selected = old($name, $value ?? $selected);

        if (empty($this->id)) {
            $this->id = Str::random();
        }
    }

    public function render()
    {
        return view('rapture::components.forms.select');
    }
}
