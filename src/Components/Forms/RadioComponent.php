<?php

namespace Rapture\Core\Components\Forms;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class RadioComponent extends Component
{
    public function __construct(
        public $name = '',
        public $checked = false,
        public $label = '',
        public $required = false,
        public $id = '',
        public $inline = false,
        public $error = false,
    ) {
        if (empty($this->id)) {
            $this->id = Str::random();
        }
    }

    public function render()
    {
        return view('rapture::components.forms.radio');
    }
}
