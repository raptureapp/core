<?php

namespace Rapture\Core\Components\Forms;

use Illuminate\View\Component;

class FormComponent extends Component
{
    public $method;
    public $alt;

    public function __construct($method = 'get')
    {
        $this->method = strtolower($method);

        if (!in_array($this->method, ['get', 'post'])) {
            $this->alt = strtoupper($this->method);
            $this->method = 'post';
        }
    }

    public function render()
    {
        return view('rapture::components.form');
    }
}
