<?php

namespace Rapture\Core\Components\Forms;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class PasswordComponent extends Component
{
    public function __construct(
        public $name = 'password',
        public $shape = 'rounded',
        public $reveal = true,
        public $generate = true,
        public $prepend = '',
        public $append = '',
        public $error = false,
        public $label = null,
        public $required = false,
        public $id = '',
    ) {
        if (empty($this->id)) {
            $this->id = Str::random();
        }
    }

    public function render()
    {
        return view('rapture::components.forms.password');
    }
}
