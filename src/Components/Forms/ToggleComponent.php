<?php

namespace Rapture\Core\Components\Forms;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class ToggleComponent extends Component
{
    public function __construct(
        public $label = null,
        public $state = false,
        public $checked = false,
        public $name = null,
        public $value = 1,
        public $id = '',
        public $required = false,
        public $size = 'normal',
        public $description = null,
    ) {
        $this->checked = $this->state ? true : $this->checked;

        if (empty($this->id)) {
            $this->id = Str::random();
        }
    }

    public function render()
    {
        return view('rapture::components.forms.toggle');
    }
}
