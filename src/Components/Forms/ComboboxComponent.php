<?php

namespace Rapture\Core\Components\Forms;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class ComboboxComponent extends Component
{
    public function __construct(
        public $name = '',
        public $options = [],
        public $selected = null,
        public $placeholder = null,
        public $nullable = false,
        public $prepend = false,
        public $value = null,
        public $error = false,
        public $label = null,
        public $required = false,
        public $id = '',
    ) {
        $this->selected = $value ?? $selected;

        if (empty($this->id)) {
            $this->id = Str::random();
        }
    }

    public function render()
    {
        return view('rapture::components.forms.combobox');
    }
}
