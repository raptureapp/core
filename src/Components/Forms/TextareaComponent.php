<?php

namespace Rapture\Core\Components\Forms;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class TextareaComponent extends Component
{
    public function __construct(
        public $name = '',
        public $shape = 'rounded',
        public $value = '',
        public $error = false,
        public $inputClass = '',
        public $label = null,
        public $required = false,
        public $id = '',
    ) {
        if (empty($this->id)) {
            $this->id = Str::random();
        }
    }

    public function render()
    {
        return view('rapture::components.forms.textarea');
    }
}
