<?php

namespace Rapture\Core\Components\Forms;

use Illuminate\View\Component;

class LabelComponent extends Component
{
    public function __construct(
        public $required = false,
    ) {
    }

    public function render()
    {
        return view('rapture::components.forms.label');
    }
}
