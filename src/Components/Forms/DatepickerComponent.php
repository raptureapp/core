<?php

namespace Rapture\Core\Components\Forms;

use Illuminate\View\Component;

class DatepickerComponent extends Component
{
    public $range = 10;

    public function __construct(
        public $name = '',
        public $format = 'YYYY-MM-DD',
        public $value = '',
        public $max = 10,
        public $min = 10,
    ) {
        if ($max == $min) {
            $this->range = $max;
        } else {
            $min = date('Y') - $min;
            $max = date('Y') + $max;

            $this->range = '[' . $min . ',' . $max .']';
        }
    }

    public function render()
    {
        return view('rapture::components.forms.datepicker');
    }
}
