<?php

namespace Rapture\Core\Components\Forms;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class InputComponent extends Component
{
    public function __construct(
        public $name = '',
        public $shape = 'rounded',
        public $value = '',
        public $fill = true,
        public $prepend = '',
        public $append = '',
        public $error = false,
        public $inputClass = '',
        public $label = null,
        public $required = false,
        public $id = '',
        public $mask = null,
    ) {
        if (empty($this->id)) {
            $this->id = Str::random();
        }
    }

    public function render()
    {
        return view('rapture::components.forms.input');
    }
}
