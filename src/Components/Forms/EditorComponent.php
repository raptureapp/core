<?php

namespace Rapture\Core\Components\Forms;

use Illuminate\View\Component;

class EditorComponent extends Component
{
    public function __construct(
        public $name = null,
        public $id = 'editor',
        public $content = '',
        public $placeholder = '',
        public $toolbar = "undo,redo,|,heading,|,bold,italic,underline,|,fontColor,fontBackgroundColor,|,alignment,link,bulletedList,numberedList,blockQuote,insertTable",
        public $slim = false,
        public $wordcount = false,
        public $height = 'h-48',
        public $value = null,
    ) {
        if (is_array($this->toolbar)) {
            $this->toolbar = implode(',', $this->toolbar);
        }

        $this->content = $this->value ?? $this->content;
    }

    public function render()
    {
        return view('rapture::components.forms.editor');
    }
}
