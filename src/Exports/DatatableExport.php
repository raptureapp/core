<?php

namespace Rapture\Core\Exports;

use Livewire\Livewire;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class DatatableExport implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;

    public $datatable;
    public $properties;

    private $instance;

    public function __construct($datatable, $properties)
    {
        $this->datatable = $datatable;
        $this->properties = $properties;
    }

    public function query()
    {
        $class = $this->getComponent();

        return $class->createExportQuery();
    }

    public function headings(): array
    {
        $class = $this->getComponent();

        return $class->getExportableColumnsProperty()
            ->filter(fn ($column) => in_array($column->key, $class->selectedForExport))
            ->map(fn ($column) => $column->label)
            ->toArray();
    }

    public function map($row): array
    {
        $class = $this->getComponent();
        $columns = $class->getExportableColumnsProperty()
            ->filter(fn($column) => in_array($column->key, $class->selectedForExport));

        $result = [];

        foreach ($columns as $column) {
            $result[] = $column->exportString($row);
        }

        return $result;
    }

    public function getComponent()
    {
        if (!is_null($this->instance)) {
            return $this->instance;
        }

        $this->instance = Livewire::current();
        $this->instance->setProperties($this->properties);

        return $this->instance;
    }
}
