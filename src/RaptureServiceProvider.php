<?php

namespace Rapture\Core;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Laravel\Fortify\Fortify;
use Livewire\Livewire;
use Rapture\Core\Actions\ResetUserPassword;
use Rapture\Core\Commands\InstallRaptureCommand;
use Rapture\Core\Commands\RefreshAssetsCommand;
use Rapture\Core\Facades\Resource as ResourceFacade;
use Rapture\Hooks\Facades\Hook;

class RaptureServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'rapture');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'rapture');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../public' => public_path('rapture'),
            ], 'rapture');

            $this->publishes([
                __DIR__ . '/../resources/views/mail' => resource_path('views/vendor/mail/html/themes'),
                __DIR__ . '/../config/fortify.php' => config_path('fortify.php'),
            ], 'rapture-starter');

            $this->publishes([
                __DIR__ . '/../config/rapture.php' => config_path('rapture.php'),
            ], 'config');
        }

        $this->bladeComponents();
        $this->livewireComponents();

        Blade::directive('resource', function ($expression) {
            $groups = collect(explode(',', $expression));

            $groups = $groups->map(function ($item) {
                return substr(trim($item), 1, -1);
            });

            $output = '';
            $components = collect();
            $scripts = collect();
            $styles = collect();

            foreach ($groups as $group) {
                $components = $components->merge(ResourceFacade::components($group));
                $scripts = $scripts->merge(ResourceFacade::scripts($group));
                $styles = $styles->merge(ResourceFacade::styles($group));
            }

            if ($components->count() > 0) {
                $output .= "<?php \$__env->startPush('components'); ?>";
                $output .= $components->unique()->map(function ($item) {
                    if (!Str::startsWith($item, 'http')) {
                        $item = asset($item);
                    }

                    return '<script src="' . $item . '"></script>';
                })->implode('');
                $output .= "<?php \$__env->stopPush(); ?>";
            }

            if ($scripts->count() > 0) {
                $output .= "<?php \$__env->startPush('scripts'); ?>";
                $output .= $scripts->unique()->map(function ($item) {
                    if (!Str::startsWith($item, 'http')) {
                        $item = asset($item);
                    }

                    return '<script src="' . $item . '"></script>';
                })->implode('');
                $output .= "<?php \$__env->stopPush(); ?>";
            }

            if ($styles->count() > 0) {
                $output .= "<?php \$__env->startPush('styles'); ?>";
                $output .= $styles->unique()->map(function ($item) {
                    if (!Str::startsWith($item, 'http')) {
                        $item = asset($item);
                    }

                    return '<link rel="stylesheet" href="' . $item . '">';
                })->implode('');
                $output .= "<?php \$__env->stopPush(); ?>";
            }

            return $output;
        });

        Blade::directive('langAction', function ($expression) {
            return "<?php echo langAction({$expression}); ?>";
        });

        ResourceFacade::register('colorpicker', [
            'components' => ['rapture/colorpicker.js'],
        ]);

        ResourceFacade::register('datepicker', [
            'components' => [
                'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js',
                'https://cdn.jsdelivr.net/npm/pikaday/pikaday.js',
            ],
            'styles' => [
                'https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css',
            ],
        ]);

        ResourceFacade::register('sortable', [
            'scripts' => [
                'https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js',
                'rapture/sortable.js',
            ],
        ]);

        ResourceFacade::register('draggable', [
            'scripts' => [
                'https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js',
                'rapture/draggable.js',
            ],
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallRaptureCommand::class,
                RefreshAssetsCommand::class,
            ]);
        }

        Hook::attach('package.disabled', 'Rapture\Core\Listeners\DisableMenuItems');
        Hook::attach('package.enabled', 'Rapture\Core\Listeners\EnableMenuItems');
        Hook::attach('package.uninstalled', 'Rapture\Core\Listeners\DeleteMenuItems');

        Fortify::loginView(function () {
            return view('rapture::auth.login');
        });

        Fortify::confirmPasswordView(function () {
            return view('rapture::auth.confirm-password');
        });

        Fortify::twoFactorChallengeView(function () {
            return view('rapture::auth.two-factor-challenge');
        });

        Fortify::requestPasswordResetLinkView(function () {
            return view('rapture::auth.forgot');
        });

        Fortify::resetPasswordView(function ($request) {
            return view('rapture::auth.reset', ['request' => $request]);
        });

        Fortify::resetUserPasswordsUsing(ResetUserPassword::class);

        Storage::disk('local')->buildTemporaryUrlsUsing(function ($path, $expiration, $options) {
            return URL::temporarySignedRoute('upload.preview', $expiration, array_merge($options, ['path' => $path]));
        });
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/rapture/branding.php', 'rapture.branding');

        $this->app->singleton('resource', function () {
            return new Resource();
        });
    }

    public function provides()
    {
        return ['resource'];
    }

    public function bladeComponents()
    {
        // Page
        Blade::component('blank', \Rapture\Core\Components\Layout\BlankComponent::class);
        Blade::component('dashboard', \Rapture\Core\Components\Layout\DashboardComponent::class);
        Blade::component('auth', \Rapture\Core\Components\Layout\AuthComponent::class);

        // Simple
        Blade::component('heading', \Rapture\Core\Components\Layout\DashboardHeadingComponent::class);
        Blade::component('container', \Rapture\Core\Components\Layout\DashboardContainerComponent::class);
        Blade::component('form-section', \Rapture\Core\Components\Layout\FormSectionComponent::class);
        Blade::component('box', \Rapture\Core\Components\Layout\BoxComponent::class);
        Blade::component('panel', \Rapture\Core\Components\Layout\PanelComponent::class);
        Blade::component('modal', \Rapture\Core\Components\Layout\ModalComponent::class);
        Blade::component('tabs', \Rapture\Core\Components\Layout\TabsComponent::class);
        Blade::component('tab', \Rapture\Core\Components\Layout\TabComponent::class);
        Blade::component('card', \Rapture\Core\Components\Layout\CardComponent::class);
        Blade::component('h1', \Rapture\Core\Components\Layout\H1Component::class);
        Blade::component('h2', \Rapture\Core\Components\Layout\H2Component::class);
        Blade::component('h3', \Rapture\Core\Components\Layout\H3Component::class);
        Blade::component('badge', \Rapture\Core\Components\Layout\BadgeComponent::class);

        // Buttons
        Blade::component('button', \Rapture\Core\Components\Buttons\ButtonComponent::class);
        Blade::component('button-group', \Rapture\Core\Components\Buttons\GroupComponent::class);
        Blade::component('btn', \Rapture\Core\Components\Buttons\GroupButtonComponent::class);

        // Forms
        Blade::component('form', \Rapture\Core\Components\Forms\FormComponent::class);
        Blade::component('label', \Rapture\Core\Components\Forms\LabelComponent::class);
        Blade::component('checkbox', \Rapture\Core\Components\Forms\CheckboxComponent::class);
        Blade::component('radio', \Rapture\Core\Components\Forms\RadioComponent::class);
        Blade::component('datepicker', \Rapture\Core\Components\Forms\DatepickerComponent::class);
        Blade::component('input', \Rapture\Core\Components\Forms\InputComponent::class);
        Blade::component('password', \Rapture\Core\Components\Forms\PasswordComponent::class);
        Blade::component('select', \Rapture\Core\Components\Forms\SelectComponent::class);
        Blade::component('textarea', \Rapture\Core\Components\Forms\TextareaComponent::class);
        Blade::component('toggle', \Rapture\Core\Components\Forms\ToggleComponent::class);
        Blade::component('colorpicker', \Rapture\Core\Components\Forms\ColorPickerComponent::class);
        Blade::component('combobox', \Rapture\Core\Components\Forms\ComboboxComponent::class);
        Blade::component('html-editor', \Rapture\Core\Components\Forms\EditorComponent::class);

        // Helpers
        Blade::component('quick-create', \Rapture\Core\Components\Utilities\QuickCreateComponent::class);
        Blade::component('dropdown', \Rapture\Core\Components\Utilities\DropdownComponent::class);
        Blade::component('dropdown-item', \Rapture\Core\Components\Utilities\DropdownItemComponent::class);
        Blade::component('dropdown-divider', \Rapture\Core\Components\Utilities\DropdownDividerComponent::class);
        Blade::component('dropdown-heading', \Rapture\Core\Components\Utilities\DropdownHeadingComponent::class);
        Blade::component('ext-type', \Rapture\Core\Components\Utilities\ExtTypeComponent::class);

        // Misc
        Blade::component('progress', \Rapture\Core\Components\ProgressBar::class);
        Blade::component('rapture::partials.statuses', 'statuses');
        Blade::component('rapture::partials.errors', 'errors');
    }

    public function livewireComponents()
    {
        Livewire::component('user-avatar', \Rapture\Core\Livewire\UserAvatar::class);
        Livewire::component('file-upload', \Rapture\Core\Livewire\FileUpload::class);
        Livewire::component('dashboard-menu', \Rapture\Core\Livewire\DashboardMenu::class);
        Livewire::component('dashboard-widgets', \Rapture\Core\Livewire\DashboardWidgets::class);
        Livewire::component('widget-selector', \Rapture\Core\Livewire\WidgetSelector::class);
        Livewire::component('dark-mode-toggle', \Rapture\Core\Livewire\DarkModeToggle::class);
        Livewire::component('dashboard-editor', \Rapture\Core\Livewire\DashboardEditor::class);
        Livewire::component('browser-sessions', \Rapture\Core\Livewire\BrowserSessions::class);
        Livewire::component('branding-controls', \Rapture\Core\Livewire\BrandingControls::class);
    }
}
