<?php

namespace Rapture\Core\Events;

use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class MyAccountUpdated
{
    use Dispatchable, SerializesModels;

    public function __construct(
        public User $user,
    ) {
    }
}
