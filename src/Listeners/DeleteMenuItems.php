<?php

namespace Rapture\Core\Listeners;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Rapture\Core\Models\Menu;

class DeleteMenuItems
{
    public function handle($package)
    {
        Menu::where('package', $package)->delete();

        Cache::forget('dashboard.menu');
    }
}
