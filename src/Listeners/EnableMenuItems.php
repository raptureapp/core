<?php

namespace Rapture\Core\Listeners;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Rapture\Core\Models\Menu;

class EnableMenuItems
{
    public function handle($package)
    {
        Menu::where('package', $package)->update([
            'active' => true,
        ]);

        Cache::forget('dashboard.menu');
    }
}
