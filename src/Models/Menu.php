<?php

namespace Rapture\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;

class Menu extends Model
{
    protected $guarded = [];

    public function children()
    {
        return $this->hasMany(Menu::class);
    }

    public function hasChildren()
    {
        return count($this->children) > 0;
    }

    public function setNamespacesAttribute($value)
    {
        $this->attributes['namespaces'] = json_encode($value);
    }

    public function getNamespacesAttribute()
    {
        return json_decode($this->attributes['namespaces']);
    }

    public function scopeTop($query)
    {
        return $query->whereNull('menu_id');
    }

    public function scopeDashboard($query)
    {
        return $query->where('placement', 'root');
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function url()
    {
        return $this->type === 'custom' ? url($this->route) : route($this->route);
    }

    public function namespaces()
    {
        $namespaces = collect($this->namespaces)->map(function ($item, $key) {
            if (substr($item, 0, 1) === '!') {
                return substr($item, 1);
            }

            return $item . '*';
        });

        if ($this->menu_id === null && $this->hasChildren()) {
            $namespaces = $namespaces->merge($this->children->reduce(function ($carry, $child) {
                return array_merge($carry, $child->namespaces());
            }, []));
        }

        return $namespaces->toArray();
    }

    public function namespaceWithinRequest()
    {
        return request()->is(...$this->namespaces());
    }

    public function isAllowed()
    {
        if (is_null($this->permission)) {
            return true;
        }

        return Gate::allows($this->permission, $this);
    }

    public function opensInNewWindow()
    {
        if (is_null($this->meta)) {
            return false;
        }

        return json_decode($this->meta)?->newWindow ?? false;
    }

    public function isCollapsible()
    {
        if (is_null($this->meta)) {
            return false;
        }

        return json_decode($this->meta)?->collapsible ?? false;
    }
}
