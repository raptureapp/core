<?php

namespace Rapture\Core;

class Resource
{
    protected $styles = [];
    protected $scripts = [];
    protected $components = [];

    public function register($name, $assets)
    {
        $assets = collect($assets);

        $this->styles = array_merge_recursive($this->styles, [$name => $assets->get('styles', [])]);
        $this->scripts = array_merge_recursive($this->scripts, [$name => $assets->get('scripts', [])]);
        $this->components = array_merge_recursive($this->components, [$name => $assets->get('components', [])]);
    }

    public function styles($name)
    {
        $styles = [];

        if (array_key_exists($name, $this->styles)) {
            $styles = $this->styles[$name];
            unset($this->styles[$name]);
        }

        return collect($styles);
    }

    public function scripts($name)
    {
        $scripts = [];

        if (array_key_exists($name, $this->scripts)) {
            $scripts = $this->scripts[$name];
            unset($this->scripts[$name]);
        }

        return collect($scripts);
    }

    public function components($name)
    {
        $components = [];

        if (array_key_exists($name, $this->components)) {
            $components = $this->components[$name];
            unset($this->components[$name]);
        }

        return collect($components);
    }
}
