<?php

namespace Rapture\Core\Columns;

use Rapture\Core\Table\Column;

class ID extends Column
{
    public function setup()
    {
        if (empty($this->key)) {
            $this->key = 'id';
        }

        if (empty($this->label)) {
            $this->label = __('rapture::field.id');
        }

        $this->filter('numeric');
        $this->primary();
    }
}
