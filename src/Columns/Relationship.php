<?php

namespace Rapture\Core\Columns;

use Rapture\Core\Table\Column;

class Relationship extends Column
{
    public $field;
    public $model;
    public $relation;
    public $query;
    public $refine = null;
    public $empty = '';

    private function prepare($model, $relation, $field)
    {
        $this->model = $model;
        $this->relation = $relation;
        $this->field = $field;
        $this->query = new $model;

        $this->filter('collection');
        $this->filterOptions(function () {
            $model = new $this->model;
            $query = $this->model::query();

            if (!is_null($this->refine)) {
                $refine = $this->refine;
                $refine($query);
            }

            return $query->whereNotNull($this->field)->orderBy($this->field)->pluck($this->field, $model->getKeyName());
        });

        $this->query(function ($query) {
            return $query->with($this->relation);
        });
    }

    public function belongsTo($model, $relation, $field)
    {
        $this->prepare($model, $relation, $field);

        $this->render(function ($related) {
            $relation = $this->relation;
            $field = $this->field;

            return $related->$relation ? $related->$relation->$field : $this->empty;
        });

        $this->sortable(function ($query) {
            $model = $query->getModel();
            $relation = $this->relation;
            $relatedModel = $model->$relation();
            $pivot = $relatedModel->getRelated();

            return $pivot::select($this->field)
                ->whereColumn($pivot->getTable() . '.' . $pivot->getKeyName(), $model->getTable() . '.' . $relatedModel->getForeignKeyName());
        });

        return $this;
    }

    public function hasMany($model, $relation, $field)
    {
        $this->prepare($model, $relation, $field);

        $this->filterQuery(function ($query, $value) use ($model) {
            $baseQuery = new $model;
            return $query->whereHas($this->relation, function ($subquery) use ($value, $baseQuery) {
                $subquery->where($baseQuery->getTable() . '.' . $baseQuery->getKeyName(), $value);
            });
        });

        $this->render(function ($related) {
            $relation = $this->relation;

            return $related->$relation ? $related->$relation->pluck($this->field)->implode(', ') : $this->empty;
        });

        $this->sortable(function ($query) {
            if ($query instanceof \Laravel\Scout\Builder) {
                $model = $query->model;
            } else {
                $model = $query->getModel();
            }

            $relation = $this->relation;
            $pivot = $model->$relation();
            $relatedModel = $pivot->getRelated();

            return $pivot->getRelated()::select($this->field)
                ->join($pivot->getTable(), $pivot->getTable() . '.' . $pivot->getRelatedPivotKeyName(), '=', $relatedModel->getTable() . '.' . $pivot->getRelatedKeyName())
                ->whereColumn($pivot->getTable() . '.' . $pivot->getForeignPivotKeyName(), $model->getTable() . '.' . $model->getKeyName())
                ->latest($pivot->getTable() . '.' . $pivot->getParentKeyName())
                ->take(1);
        });

        return $this;
    }

    public function refineOptions($filter)
    {
        $this->refine = $filter;

        return $this;
    }

    public function empty($empty)
    {
        $this->empty = $empty;

        return $this;
    }
}
