<?php

namespace Rapture\Core\Columns;

use Rapture\Core\Table\Column;

class TrueFalse extends Column
{
    public $iconTrue = 'check';
    public $iconFalse = 'times';
    public $check = null;

    public function iconTrue($icon)
    {
        $this->iconTrue = $icon;

        return $this;
    }

    public function iconFalse($icon)
    {
        $this->iconFalse = $icon;

        return $this;
    }

    public function determine($callback)
    {
        $this->check = $callback;

        return $this;
    }

    public function nullable()
    {
        $this->check = function ($data) {
            $key = $this->key;
            return !is_null($data->$key);
        };

        return $this;
    }

    public function check($data)
    {
        if (is_null($this->check)) {
            return $data[$this->key];
        }

        $display = $this->check;

        return $display($data);
    }

    public function display($data)
    {
        if (!is_null($this->render)) {
            parent::display($data);
        }

        return $this->check($data) ? '<em class="far fa-' . $this->iconTrue . ' text-green-600"></em>' : '<em class="far fa-' . $this->iconFalse . ' text-red-600"></em>';
    }

    public function exportString($data)
    {
        if (!is_null($this->export)) {
            parent::exportString($data);
        }

        return $data[$this->key] ? 'true' : 'false';
    }
}
