<?php

namespace Rapture\Core\Columns;

use Rapture\Core\Table\Column;

class Image extends Column
{
    public $circle = false;
    public $src = null;
    public $width = 'w-auto';
    public $height = 'h-auto';

    public function setup()
    {
        $this->disableSort();
        $this->render(function ($model) {
            $key = $this->key;
            $src = $model->$key;

            if (!is_null($this->src)) {
                $render = $this->src;
                $src = $render($model);
            }

            if (empty($src)) {
                return '';
            }

            return '<img src="' . $src . '" class="' . ($this->circle ? 'rounded-full' : '') . ' ' . $this->width . ' ' . $this->height . '" />';
        });

        $this->dontExport();
    }

    public function src($src)
    {
        $this->src = $src;

        return $this;
    }

    public function width($width)
    {
        $this->width = $width;

        return $this;
    }

    public function height($height)
    {
        $this->height = $height;

        return $this;
    }

    public function circle()
    {
        $this->circle = true;

        return $this;
    }
}
