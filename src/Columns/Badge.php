<?php

namespace Rapture\Core\Columns;

use Illuminate\Support\Facades\Blade;
use Rapture\Core\Table\Column;

class Badge extends Column
{
    public $color = 'blue';
    public $colorMethod = 'badgeColors';

    public function setup()
    {
        $this->filter('text');
        $this->render(function ($row) {
            $color = $this->color;
            $method = str($this->key)->studly();

            if (method_exists($row, 'badge' . $method)) {
                $color = call_user_func([$row, 'badge' . $method]);
            } elseif (method_exists($row, $this->colorMethod)) {
                $color = call_user_func([$row, $this->colorMethod]);
            }

            if (method_exists($row, 'column' . $method)) {
                $value = call_user_func([$row, 'column' . $method]);
            } else {
                $value = $row[$this->key];
            }

            return Blade::render('<x-badge color="{{ $color }}">{{ $value }}</x-badge>', [
                'color' => $color,
                'value' => $value,
            ]);
        });
    }

    public function filterApplied($query, $value)
    {
        $query->where($this->key, 'regexp', $value);
    }
}
