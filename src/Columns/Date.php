<?php

namespace Rapture\Core\Columns;

use Carbon\Carbon;
use Rapture\Core\Table\Column;

class Date extends Column
{
    public $format = 'M j, Y';

    public function setup()
    {
        $this->filter('date');
        $this->render(function ($model) {
            $key = $this->key;

            if (is_null($model->$key)) {
                return '';
            }

            if (is_string($model->$key)) {
                $model->$key = Carbon::parse($model->$key);
            }

            return $model->localDate($key)->format($this->format);
        });
    }

    public function format($format)
    {
        $this->format = $format;

        return $this;
    }

    public function filterApplied($query, $value)
    {
        if (array_key_exists('before', $value)) {
            $before = Carbon::parse($value['before'], auth()->user()->timezone())->startOfDay()->utc();
            $query->where($this->key, '<=', $before);
        }

        if (array_key_exists('after', $value)) {
            $after = Carbon::parse($value['after'], auth()->user()->timezone())->endOfDay()->utc();
            $query->where($this->key, '>=', $after);
        }
    }
}
