<?php

namespace Rapture\Core\Columns;

use Rapture\Core\Table\Column;

class Text extends Column
{
    public function setup()
    {
        $this->filter('text');
    }

    public function filterApplied($query, $value)
    {
        $query->where($this->key, 'regexp', $value);
    }
}
