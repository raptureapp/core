<?php

namespace Rapture\Core\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Rapture\Core\Models\Menu;
use Rapture\Hooks\Facades\Hook;

class InstallRaptureCommand extends Command
{
    protected $signature = 'rapture:install
        {--admin : Add a default admin account}
        {--setup : Modify your file system with the necessary pieces}';
    protected $description = 'Install or reinstall rapture along with all of it\'s packages';

    public function handle()
    {
        if (Schema::hasTable('menus') && !$this->confirm('Rapture is installed. Do you want to reinstall?')) {
            $this->info('Aborting');
            return;
        }

        Hook::dispatch('rapture.installing');

        $this->call('migrate:fresh');

        if ($this->option('admin')) {
            $this->call('db:seed', ['--class' => 'Rapture\Core\Seeder\UsersTableSeeder']);
        }

        if ($this->option('setup')) {
            $this->prepareFiles();
            $this->updateEnv();
            $this->setupConfig();
            $this->attachCoreController();
        }

        $this->addDashboardToMenu();

        foreach (config('packages') as $package => $status) {
            if ($status !== 'installed') {
                continue;
            }

            $this->info('Installing: ' . $package);

            $this->call('package:install', [
                'package' => $package,
                '--setup' => $this->option('setup'),
            ]);
        }

        $this->call('vendor:publish', ['--tag' => 'rapture', '--force' => true]);
        $this->call('vendor:publish', ['--tag' => 'rapture-starter']);

        Hook::dispatch('rapture.installed');

        $this->info('Rapture installed!');
    }

    public function prepareFiles()
    {
        $modified = false;
        $composerPath = base_path('composer.json');
        $composer = str(file_get_contents($composerPath));

        if (!$composer->contains('rapture:update')) {
            if ($composer->contains('post-update-cmd')) {
                $scripts = $composer->betweenFirst('"post-update-cmd": [', ']')->beforeLast('"') . '"';
                $composer = $composer->replace($scripts, str($scripts)->append(',')->newLine()->append('            "@php artisan rapture:update"'));
            } else {
                $scripts = $composer->betweenFirst('"scripts": {', '}')->beforeLast(']') . ']';
                $composer = $composer->replace($scripts, str($scripts)->append(',')->newLine()->append('        "post-update-cmd": [
            "@php artisan rapture:update"
        ]'));
            }

            $modified = true;
        }

        if ($modified) {
            file_put_contents($composerPath, $composer);
        }
    }

    public function updateEnv()
    {
        $modified = false;
        $environmentPath = base_path('.env');
        $environment = str(file_get_contents($environmentPath));

        if (!$environment->contains('SCOUT_DRIVER')) {
            $environment = $environment->newLine()->append('SCOUT_DRIVER=database')->newLine();

            $modified = true;
        }

        if (!$environment->contains('SESSION_DRIVER')) {
            $environment = $environment->newLine()->append('SESSION_DRIVER=database')->newLine();

            $modified = true;
        } elseif (!$environment->contains('SESSION_DRIVER=database')) {
            $environment = $environment
                ->replace('SESSION_DRIVER=file', 'SESSION_DRIVER=database')
                ->replace('SESSION_DRIVER=cookie', 'SESSION_DRIVER=database')
                ->replace('SESSION_DRIVER=apc', 'SESSION_DRIVER=database')
                ->replace('SESSION_DRIVER=memcached', 'SESSION_DRIVER=database')
                ->replace('SESSION_DRIVER=redis', 'SESSION_DRIVER=database')
                ->replace('SESSION_DRIVER=dynamodb', 'SESSION_DRIVER=database')
                ->replace('SESSION_DRIVER=array', 'SESSION_DRIVER=database');

            $modified = true;
        }

        if ($modified) {
            file_put_contents($environmentPath, $environment);
        }
    }

    public function addDashboardToMenu()
    {
        Menu::firstOrCreate([
            'package' => 'rapture/core',
            'route' => 'dashboard',
            'label' => 'rapture::dashboard.menu_label',
            'icon' => 'tachometer-alt',
            'namespaces' => ['!dashboard'],
            'position' => -1,
        ]);
    }

    public function setupConfig()
    {
        if (file_exists(base_path('config/rapture'))) {
            return;
        }

        $gitignorePath = base_path('.gitignore');
        $gitignore = str(file_get_contents($gitignorePath));
        $modified = false;

        if (!$gitignore->contains('config/rapture')) {
            $gitignore = $gitignore->newLine()->append('/config/rapture')->newLine();
            $modified = true;
        }

        if ($modified) {
            file_put_contents($gitignorePath, $gitignore);
        }

        Storage::makeDirectory('config/rapture');
    }

    public function attachCoreController()
    {
        $modified = false;
        $controllerPath = base_path('app/Http/Controllers/Controller.php');
        $controller = str(file_get_contents($controllerPath));

        if (!$controller->contains('Rapture\Core\Controllers\BaseController')) {
            $replacement = str('namespace App\Http\Controllers;')
                ->newLine()
                ->newLine()
                ->append('use Rapture\Core\Controllers\BaseController;');

            $controller = $controller->replace('namespace App\Http\Controllers;', $replacement);
            $controller = $controller->replace('abstract class Controller', 'class Controller extends BaseController');

            $modified = true;
        }

        if ($modified) {
            file_put_contents($controllerPath, $controller);
        }
    }
}
