<?php

namespace Rapture\Core\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class RefreshAssetsCommand extends Command
{
    protected $signature = 'rapture:update';
    protected $description = 'Update rapture specific assets';

    public function handle()
    {
        $this->call('vendor:publish', ['--tag' => 'rapture', '--force' => true]);
        $this->call('vendor:publish', ['--tag' => 'rapture-starter']);
        $this->info('Assets published!');
    }
}
