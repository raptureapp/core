<?php

function langAction($action, $model)
{
    return __('rapture::actions.' . $action . '_model', [
        'model' => $model,
    ]);
}

function langAlert($alert, $model)
{
    return __('rapture::alert.' . $alert, [
        'model' => $model,
    ]);
}
