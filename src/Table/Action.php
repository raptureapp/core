<?php

namespace Rapture\Core\Table;

use Illuminate\Support\Facades\Gate;
use Rapture\Core\Table\Features\HasScopes;

class Action
{
    use HasScopes;

    public $primary = false;
    public $route = null;
    public $label = '';
    public $permission = null;
    public $permissionRow = false;
    public $callback = null;
    public $render = null;
    public $newWindow = false;
    public $conditions = [];
    public $confirm = null;

    public function __construct($label = '')
    {
        $this->label = $label;
    }

    public static function make($label = '')
    {
        return new static($label);
    }

    public static function show($type, $label = '')
    {
        if (empty($label)) {
            $label = __('rapture::actions.view');
        }

        $instance = new static($label);
        $instance->route('dashboard.' . $type . '.show');
        $instance->render('<em class="far fa-eye"></em>');

        return $instance;
    }

    public static function edit($type, $label = '')
    {
        if (empty($label)) {
            $label = __('rapture::actions.edit');
        }

        $instance = new static($label);
        $instance->permission($type . '.edit');
        $instance->route('dashboard.' . $type . '.edit');
        $instance->render('<em class="far fa-pencil"></em>');

        return $instance;
    }

    public static function delete($type, $label = '')
    {
        if (empty($label)) {
            $label = __('rapture::actions.delete');
        }

        $instance = new static($label);
        $instance->permission($type . '.destroy');
        $instance->callback('delete');
        $instance->render('<em class="far fa-trash"></em>');
        $instance->confirm('Are you sure you want to delete?');

        return $instance;
    }

    public function primary()
    {
        $this->primary = true;

        return $this;
    }

    public function render($closure)
    {
        $this->render = $closure;

        return $this;
    }

    public function permission($permission, $rowSpecific = false)
    {
        $this->permission = $permission;
        $this->permissionRow = $rowSpecific;

        return $this;
    }

    public function route($route)
    {
        $this->route = $route;

        return $this;
    }

    public function callback($callback)
    {
        $this->callback = $callback;

        return $this;
    }

    public function newWindow()
    {
        $this->newWindow = true;

        return $this;
    }

    public function condition($condition)
    {
        $this->conditions[] = $condition;

        return $this;
    }

    public function confirm($message)
    {
        $this->confirm = $message;

        return $this;
    }

    public function isPrimary()
    {
        return $this->primary;
    }

    public function hasCallback()
    {
        return !empty($this->callback);
    }

    public function hasRoute()
    {
        return !empty($this->route);
    }

    public function allowed($row)
    {
        if (!$this->passesConditions($row)) {
            return false;
        }

        if (is_null($this->permission) || !$this->permissionRow) {
            return true;
        }

        return Gate::allows($this->permission, $row);
    }

    public function hasPermission()
    {
        if (is_null($this->permission) || $this->permissionRow) {
            return true;
        }

        return Gate::allows($this->permission, null);
    }

    public function opensInNewWindow()
    {
        return $this->newWindow;
    }

    public function passesConditions($data)
    {
        if (empty($this->conditions)) {
            return true;
        }

        foreach ($this->conditions as $condition) {
            if (!$condition($data)) {
                return false;
            }
        }

        return true;
    }

    public function display($data)
    {
        if (is_null($this->render)) {
            return $this->label;
        }

        if (is_string($this->render)) {
            return $this->render;
        }

        $display = $this->render;

        return $display($data);
    }
}
