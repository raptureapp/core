<?php

namespace Rapture\Core\Table;

use Illuminate\Support\Facades\Gate;
use Rapture\Core\Table\Features\HasScopes;

class Operation
{
    use HasScopes;

    public $label = '';
    public $permission = null;
    public $callback = null;
    public $render = null;
    public $confirm = null;
    public $keybinding = null;

    public function __construct($label = '')
    {
        $this->label = $label;
    }

    public static function make($label = '')
    {
        return new static($label);
    }

    public function render($closure)
    {
        $this->render = $closure;

        return $this;
    }

    public function permission($permission)
    {
        $this->permission = $permission;

        return $this;
    }

    public function callback($callback)
    {
        $this->callback = $callback;

        return $this;
    }

    public function confirm($message = 'Are you sure you want to perform this action?')
    {
        $this->confirm = $message;

        return $this;
    }

    public function keybinding($keybinding)
    {
        $this->keybinding = $keybinding;

        return $this;
    }

    public function hasPermission()
    {
        if (is_null($this->permission)) {
            return true;
        }

        return Gate::allows($this->permission, 'blank');
    }

    public function display()
    {
        if (is_null($this->render)) {
            return $this->label;
        }

        if (is_string($this->render)) {
            return $this->render . '<span class="text-sm">' . $this->label . '</span>';
        }

        $display = $this->render;

        return $display();
    }
}
