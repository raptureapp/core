<?php

namespace Rapture\Core\Table;

use Rapture\Core\Exports\DatatableExport;
use Rapture\Core\Jobs\NotifyUserOfCompletedExport;

trait Exportable
{
    public $exportOpen = false;
    public $exportable = true;
    public $selectedForExport = [];

    public function updatedExportOpen()
    {
        if (!$this->exportOpen) {
            return;
        }

        $this->selectAllColumnsForExport();
    }

    public function getExportableColumnsProperty()
    {
        return $this->filledColumns()->reject(function ($column) {
            return $column->isLocked() || !$column->exportable;
        });
    }

    public function selectAllColumnsForExport()
    {
        $this->selectedForExport = $this->exportableColumns->pluck('key')->toArray();
    }

    public function deselectAllColumnsForExport()
    {
        $this->selectedForExport = [];
    }

    public function createExportQuery()
    {
        $query = $this->baseQuery();

        if ($this->searchable && !empty($this->search)) {
            return $this->searchedExportQuery($query);
        }

        $query = $this->scopedQuery($query);
        $query = $this->extendedExportQuery($query);
        $query = $this->filteredQuery($query);

        return $query->orderBy($this->sortingColumn()->sortQuery($query), $this->direction);
    }

    public function searchedExportQuery($query)
    {
        return $query->getModel()
            ->search($this->search)
            ->query(function ($query) {
                $query = $this->scopedQuery($query);
                $query = $this->extendedExportQuery($query);
                $query = $this->filteredQuery($query);

                $query->orderBy($this->sortingColumn()->sortQuery($query), $this->direction);
            });
    }

    public function extendedExportQuery($query)
    {
        $activeColumns = $this->exportableColumns
            ->filter(fn ($column) => in_array($column->key, $this->selectedForExport) && $column->hasQuery());

        foreach ($activeColumns as $column) {
            $column->processQuery($query);
        }

        return $query;
    }

    public function export()
    {
        $filepath = 'public/exports/table-' . date('Y-m-d-h-i-s') . '.csv';

        (new DatatableExport(get_called_class(), $this->getProperties()))
            ->store($filepath);

        // (new DatatableExport(get_called_class(), $this->getProperties()))
        //     ->queue($filepath)
        //     ->chain([
        //         new NotifyUserOfCompletedExport(request()->user(), $filename),
        //     ]);

        $this->exportOpen = false;

        return response()->download(storage_path('app/' . $filepath));
    }

    public function getInitializedPropertyValue(\ReflectionProperty $property)
    {
        if (method_exists($property, 'isInitialized') && !$property->isInitialized($this)) {
            return null;
        }

        return $property->getValue($this);
    }

    public function getProperties()
    {
        $publicProperties = array_filter((new \ReflectionClass($this))->getProperties(), function ($property) {
            return $property->isPublic() && ! $property->isStatic();
        });

        $data = [];

        $accepted = array_merge([
            'search',
            'filter',
            'scope',
        ], $this->exportProperties());

        foreach ($publicProperties as $property) {
            $name = $property->getName();

            if (!in_array($name, $accepted)) {
                continue;
            }

            $data[$name] = $this->getInitializedPropertyValue($property);
        }

        return $data;
    }

    public function setProperties($data)
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function exportProperties()
    {
        return [];
    }
}
