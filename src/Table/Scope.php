<?php

namespace Rapture\Core\Table;

use Illuminate\Support\Facades\Gate;

class Scope
{
    public $key = '';
    public $label = '';
    public $permission = null;
    public $query = null;
    public $render = null;
    public $default = false;
    public $count;

    public function __construct($key = '', $label = '')
    {
        $this->key = $key;
        $this->label = $label;
    }

    public static function make($key = '', $label = '')
    {
        return new static($key, $label);
    }

    public function render($closure)
    {
        $this->render = $closure;

        return $this;
    }

    public function permission($permission)
    {
        $this->permission = $permission;

        return $this;
    }

    public function query($query)
    {
        $this->query = $query;

        return $this;
    }

    public function defaultScope()
    {
        $this->default = true;

        return $this;
    }

    public function fetchCount($query)
    {
        $this->processQuery($query);
        $this->count = $query->count();
    }

    public function hasPermission()
    {
        if (is_null($this->permission)) {
            return true;
        }

        return Gate::allows($this->permission, 'blank');
    }

    public function isDefault()
    {
        return $this->default;
    }

    public function display()
    {
        if (is_null($this->render)) {
            return $this->label;
        }

        if (is_string($this->render)) {
            return $this->render;
        }

        $display = $this->render;

        return $display();
    }

    public function processQuery($query)
    {
        if (!$this->query) {
            return;
        }

        $customQuery = $this->query;
        $customQuery($query);
    }
}
