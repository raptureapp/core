<?php

namespace Rapture\Core\Table;

use Rapture\Core\Table\Features\HasScopes;

class Column
{
    use HasScopes;

    public $primary = false;
    public $visible = false;
    public $locked = false;
    public $order = 0;
    public $render = null;
    public $default = null;
    public $query = null;
    public $sortable = true;
    public $sortableRaw = null;
    public $filter = null;
    public $filterOptions = null;
    public $filterQuery = null;
    public $export = null;
    public $classes = [];
    public $linkable = null;
    public $opensInNewWindow = false;
    public $exportable = true;

    public function __construct(
        public $key = '',
        public $label = ''
    ) {
        $this->setup();
    }

    public static function make($key = '', $label = '')
    {
        return new static($key, $label);
    }

    public function setup()
    {
        //
    }

    public function label($label)
    {
        $this->label = $label;

        return $this;
    }

    public function primary()
    {
        $this->primary = true;

        return $this;
    }

    public function locked()
    {
        $this->locked = true;

        return $this;
    }

    public function visible($visible = true)
    {
        $this->visible = $visible;

        return $this;
    }

    public function render($closure)
    {
        $this->render = $closure;

        return $this;
    }

    public function sortable($query = null)
    {
        $this->sortable = true;
        $this->sortableRaw = $query;

        return $this;
    }

    public function disableSort()
    {
        $this->sortable = false;

        return $this;
    }

    public function defaultSort($direction = 'asc')
    {
        $this->default = $direction;

        return $this;
    }

    public function filter($type = 'text')
    {
        $this->filter = $type;

        return $this;
    }

    public function query($query)
    {
        $this->query = $query;

        return $this;
    }

    public function url($url)
    {
        $this->linkable = $url;

        return $this;
    }

    public function newWindow()
    {
        $this->opensInNewWindow = true;

        return $this;
    }

    public function dontExport()
    {
        $this->exportable = false;

        return $this;
    }

    /**
     * @deprecated
     */
    public function collection($options = [])
    {
        $this->filter('collection');
        $this->filterOptions($options);

        return $this;
    }

    public function boolean()
    {
        return $this->filter('boolean');
    }

    public function filterQuery($query)
    {
        $this->filterQuery = $query;

        if (is_null($this->filter)) {
            $this->filter();
        }

        return $this;
    }

    public function filterOptions($options)
    {
        $this->filterOptions = $options;

        return $this;
    }

    public function order($order)
    {
        $this->order = $order;

        return $this;
    }

    public function export($callback)
    {
        $this->export = $callback;

        return $this;
    }

    public function classes($classes)
    {
        $this->classes = array_merge($this->classes, is_array($classes) ? $classes : explode(' ', $classes));

        return $this;
    }

    public function is($key)
    {
        return $this->key == $key;
    }

    public function isVisible()
    {
        return $this->visible;
    }

    public function isLocked()
    {
        return $this->locked;
    }

    public function isSortable()
    {
        return $this->sortable;
    }

    public function isDefault()
    {
        return !is_null($this->default);
    }

    public function isPrimary()
    {
        return $this->primary;
    }

    public function isFilterable()
    {
        return !is_null($this->filter);
    }

    public function sortQuery($query)
    {
        if (is_callable($this->sortableRaw)) {
            $sort = $this->sortableRaw;
            return $sort($query);
        }

        return $this->sortableRaw ?? $this->key;
    }

    public function hasCustomFilterQuery()
    {
        return !is_null($this->filterQuery);
    }

    public function hasQuery()
    {
        return !is_null($this->query);
    }

    public function getFilterOptions()
    {
        $options = $this->filterOptions;

        if ($this->filter === 'collection' && empty($options)) {
            return [];
        }

        if (!is_callable($options)) {
            return $options;
        }

        return $options();
    }

    public function isLinked()
    {
        return !is_null($this->linkable);
    }

    public function display($data)
    {
        if (!is_null($this->render)) {
            $display = $this->render;

            return $display($data, $this->key);
        }

        if (method_exists($data, 'column' . str($this->key)->studly())) {
            return call_user_func([$data, 'column' . str($this->key)->studly()]);
        }

        return $data[$this->key];
    }

    public function linkTarget($data)
    {
        if (is_callable($this->linkable)) {
            $callback = $this->linkable;

            return $callback($data, $this->key);
        }

        return $this->linkable;
    }

    public function opensInNewWindow()
    {
        return $this->opensInNewWindow;
    }

    public function exportString($data)
    {
        if (!is_null($this->export)) {
            $callback = $this->export;
            return $callback($data, $this->key);
        }

        return $this->display($data);
    }

    public function processFilter($query, $value)
    {
        if ($this->hasCustomFilterQuery()) {
            $customFilter = $this->filterQuery;
            $customFilter($query, $value, $this->key);
            return;
        }

        if (method_exists($this, 'filterApplied')) {
            $this->filterApplied($query, $value);
            return;
        }

        switch ($this->filter) {
            case 'text':
                $query->where($this->key, 'regexp', $value);
                break;
            default:
                $query->where($this->key, $value);
                break;
        }
    }

    public function processQuery($query)
    {
        $customQuery = $this->query;
        $customQuery($query);
    }

    public function rowClasses()
    {
        return implode(' ', $this->classes);
    }
}
