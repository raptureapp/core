<?php

namespace Rapture\Core\Table\Features;

trait HasScopes
{
    public $scoped = [];

    public function withinScope($scope)
    {
        $this->scoped[] = $scope;

        return $this;
    }

    public function withinScopes($scopes)
    {
        $this->scoped = array_merge($this->scoped, $scopes);

        return $this;
    }

    public function isWithinScope($scope)
    {
        if (empty($this->scoped)) {
            return true;
        }

        return in_array($scope, $this->scoped);
    }

    public function isScoped()
    {
        return !empty($this->scoped);
    }
}
