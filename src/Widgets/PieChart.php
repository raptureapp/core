<?php

namespace Rapture\Core\Widgets;

class PieChart extends BaseWidget
{
    public function data()
    {
        return '';
    }

    public function labels()
    {
        return [];
    }

    public function render()
    {
        return view('rapture::widgets.pie-chart', $this->getData());
    }

    protected function getData()
    {
        $data = $this->data();

        return [
            'data' => $data,
            'label' => $this->label(),
            'labels' => $this->labels(),
            'theme' => cache('theme.' . auth()->user()->id),
            'actions' => $this->actions(),
        ];
    }
}
