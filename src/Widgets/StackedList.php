<?php

namespace Rapture\Core\Widgets;

class StackedList extends BaseWidget
{
    public $description = '';
    public $icon = '';
    public $minRows = 2;
    public $maxColumns = 1;

    public function description()
    {
        return $this->description;
    }

    public function data()
    {
        return [];
    }

    public function display($data)
    {
        return $data;
    }

    public function render()
    {
        return view('rapture::widgets.list', $this->getData());
    }

    protected function getData()
    {
        $data = $this->data();

        return [
            'data' => $data,
            'value' => $this->display($data),
            'label' => $this->label(),
            'description' => $this->description(),
            'actions' => $this->actions(),
            'empty' => $this->empty(),
        ];
    }
}
