<?php

namespace Rapture\Core\Widgets;

class LineChart extends BaseWidget
{
    public function data()
    {
        return '';
    }

    public function xAxis()
    {
        return [];
    }

    public function render()
    {
        return view('rapture::widgets.line-chart', $this->getData());
    }

    protected function getData()
    {
        $data = $this->data();

        return [
            'data' => $data,
            'label' => $this->label(),
            'xAxis' => $this->xAxis(),
            'theme' => cache('theme.' . auth()->user()->id),
        ];
    }
}
