<?php

namespace Rapture\Core\Widgets;

class SingleStat extends BaseWidget
{
    public $minRows = 1;
    public $maxRows = 1;
    public $minColumns = 1;
    public $maxColumns = 1;

    public function data()
    {
        return '';
    }

    public function display($data)
    {
        return $data;
    }

    public function trend($data)
    {
        return null;
    }

    public function render()
    {
        return view('rapture::widgets.stat', $this->getData());
    }

    protected function getData()
    {
        $data = $this->data();

        return [
            'data' => $data,
            'value' => $this->display($data),
            'label' => $this->label(),
            'trend' => $this->trend($data),
        ];
    }
}
