<?php

namespace Rapture\Core\Widgets;

use Illuminate\Support\Facades\Blade;
use Livewire\Attributes\Reactive;
use Livewire\Component;

class BaseWidget extends Component
{
    public $label = '';
    public $purpose = '';
    public $minRows = null;
    public $maxRows = null;
    public $minColumns = null;
    public $maxColumns = null;
    public $permission = null;
    public $empty = '';

    #[Reactive]
    public $columns = 1;

    #[Reactive]
    public $rows = 1;

    public static function make()
    {
        return new static();
    }

    public function empty()
    {
        if (!empty($this->empty)) {
            return $this->empty;
        }

        return Blade::render('<div class="hidden"></div>');
    }

    public function label()
    {
        return $this->label;
    }

    public function purpose()
    {
        return $this->purpose;
    }

    public function condition()
    {
        return true;
    }

    public function actions()
    {
        return [];
    }
}
