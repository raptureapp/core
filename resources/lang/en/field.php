<?php

return [
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'password_conf' => 'Confirm Password',
    'required' => 'Required',
    'created' => 'Created',
    'updated' => 'Updated',
    'fix_errors' => 'Please fix the following errors',
    'id' => 'ID',
];
