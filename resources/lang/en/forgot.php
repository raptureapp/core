<?php

return [
    'title' => 'Reset Password',
    'field_email' => 'Email Address',
    'form_submit' => 'Send Link',
];
