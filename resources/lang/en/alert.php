<?php

return [
    'created' => ':Model was successfully created!',
    'updated' => ':Model was successfully updated!',
    'deleted' => ':Model was successfully deleted!',
];
