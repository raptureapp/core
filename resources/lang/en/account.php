<?php

return [
    'title' => 'Account Settings',
    'menu_label' => 'My Account',
    'update_success' => 'Your account was successfully updated!',
];
