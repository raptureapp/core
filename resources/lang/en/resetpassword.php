<?php

return [
    'title' => 'Reset Password',
    'field_email' => 'Email Address',
    'field_password' => 'Password',
    'field_password_conf' => 'Confirm Password',
    'form_submit' => 'Reset Password',
];
