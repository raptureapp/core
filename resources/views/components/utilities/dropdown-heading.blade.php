<p {{ $attributes->class(['p-2 text-xs font-semibold text-gray-500 dark:text-gray-400 uppercase tracking-wider']) }}>{{ $label }}</p>
