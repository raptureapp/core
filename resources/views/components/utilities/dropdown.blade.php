<div x-show="{{ $trigger }}" x-transition:enter="transition ease-out duration-100" x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-95" role="menu" aria-orientation="vertical" tabindex="-1" @keydown.tab="{{ $trigger }} = false" @keydown.enter.prevent="{{ $trigger }} = false" @keyup.space.prevent="{{ $trigger }} = false" @click.away="{{ $trigger }} = false" style="display: none;" {{ $attributes->class([
        "absolute z-10 origin-top-right rounded-lg bg-white dark:bg-slate-900 shadow-md border border-black-50 dark:border-slate-700 focus:outline-none px-2 py-2 space-y-0.5 mt-2",
        "right-0 origin-top-right" => $position === 'right',
        "left-0 origin-top-left" => $position === 'left',
        $size,
    ]) }}>
    {{ $slot }}
</div>
