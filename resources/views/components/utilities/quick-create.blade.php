<form method="post" action="{{ $action }}" x-data="{ open: false, canSubmit: false }"{{ $attributes->merge(['class' => 'relative']) }}>
    @csrf

    <x-input :name="$name" :placeholder="$placeholder" class="transition-all duration-300 ease-in-out" x-show="open" x-transition:enter-start="w-4" x-transition:enter-end="w-64" x-transition:leave="absolute top-0 right-0" x-transition:leave-start="w-64" x-transition:leave-end="w-4" @input="canSubmit = $refs.primary.value.length > 0" x-ref="primary" @keydown.escape="open = false; $refs.primary.value = ''; canSubmit = false" ::class="{ 'pr-10': open && !canSubmit, 'pr-20': canSubmit }" style="display: none;" />

    <button type="button" @click.prevent="open = false; $refs.primary.value = ''; canSubmit = false" class="absolute top-0 right-0 white-space-no-wrap py-2 px-3 leading-5 border border-transparent transition-all duration-300 ease-in-out focus:outline-none" x-show="open" :class="{ 'mr-10': canSubmit, 'mr-0': !canSubmit }" style="display: none;">
        <em class="far fa-times" aria-hidden="true"></em>
    </button>

    <button type="submit" class="absolute top-0 right-0 overflow-hidden white-space-no-wrap text-center leading-5 border-t border-b border-transparent py-2 transition-all duration-300 ease-in-out text-white bg-primary-700 hover:bg-primary-900 focus:outline-none focus:border-primary-700 focus:ring-primary active:bg-primary-900 rounded-r-lg w-0 dark:bg-primary-800 dark:hover:bg-primary-900" :class="{ 'w-10': canSubmit, 'w-0': !canSubmit }">
        <em class="far fa-check" aria-hidden="true"></em>
    </button>

    <button class="py-2 px-3 border border-transparent font-medium rounded-lg text-white bg-primary-700 hover:bg-primary-900 focus:outline-none focus:border-primary-700 focus:ring-primary active:bg-primary-900 flex items-center whitespace-nowrap transition-all duration-300 ease-in-out leading-5 space-x-3 dark:bg-primary-800 dark:hover:bg-primary-900 text-sm" type="button" @click.prevent="open = true; setTimeout(function () { $refs.primary.focus() }, 150)" x-show="!open" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="absolute top-0 right-0" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0">
        @if ($icon)
            <em class="far fa-{{ $icon }} text-sm" aria-hidden="true"></em>
        @endif
        @if ($label)
            <span>{{ $label }}</span>
        @endif
        {{ $slot }}
    </button>
</form>
