<{!! !is_null($href) ? 'a href="' . $href . '"' : 'button type="' . $type . '"' !!} {{ $attributes->class([
    'flex items-center w-full rounded-md px-2 py-1 text-sm hover:text-white hover:bg-primary-700 dark:hover:bg-slate-700 transition group',
    'text-gray-600 dark:text-slate-300' => $color === 'normal',
    'text-red-600' => $color === 'danger',
    ]) }} role="menuitem">
    @if ($icon)
        <em aria-hidden="true" @class([
            'far fa-' . $icon . ' w-5 transition text-center mr-2.5 group-hover:text-white dark:group-hover:text-slate-300',
            'text-gray-500 dark:text-slate-500' => $color === 'normal',
            'text-red-600' => $color === 'danger',
        ])></em>
    @endif
    {{ $slot }}
    @lang($label)
</{{ !is_null($href) ? 'a' : 'button' }}>
