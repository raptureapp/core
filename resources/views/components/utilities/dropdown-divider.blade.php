<div {{ $attributes->class(['-mx-2 py-2.5']) }}>
    <div class="border-t border-black-50 dark:border-slate-700"></div>
</div>
