<div {{ $attributes->class('bg-gray-100 dark:bg-slate-800 rounded-full overflow-hidden') }}>
    <div class="h-full bg-primary-600 dark:bg-primary-700 rounded-full" style="width: {{ $progress }}%"></div>
</div>
