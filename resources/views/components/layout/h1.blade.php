<h1 {{ $attributes->class(['text-2xl font-semibold leading-7 sm:text-3xl sm:leading-9 sm:truncate']) }}>{{ $slot }}</h1>
