<div {{ $attributes->class([
    'rounded-lg border border-black-50 dark:border-slate-700 bg-white dark:bg-slate-900 px-4 sm:px-6',
    'py-4 sm:py-6' => $size === 'normal',
    'py-2 sm:py-3' => $size === 'small',
    'py-1 sm:py-1.5' => $size === 'xsmall',
]) }}>
    {{ $slot }}
</div>
