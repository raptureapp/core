@props(['heading'])

<div {{ $attributes->class([
    'rounded-lg border border-black-50 dark:border-slate-700 px-4 sm:px-6 bg-white dark:bg-slate-900',
    ]) }}>
    <div x-data="{ open: {{ $open ? 'true' : 'false' }} }">
        <div {{ $heading->attributes->class([
            'cursor-pointer select-none',
            'py-3 sm:py-4' => $size === 'normal',
            'py-2 sm:py-3' => $size === 'small',
            ])->merge([
                '@click' => $attributes->prepends('open = !open;'),
            ]) }} :class="{ 'rounded-lg' : !open, 'rounded-t-lg': open }">
            {{ $heading }}
        </div>
        <div x-show="open" wire:sortable.ignore style="display: none;" x-collapse>
            <div class="py-4 sm:py-6 rounded-b-lg border-t border-black-50 dark:border-slate-700">
                {{ $slot }}
            </div>
        </div>
    </div>
</div>
