<h2 {{ $attributes->class(['text-xl font-medium leading-6 sm:truncate']) }}>{{ $slot }}</h2>
