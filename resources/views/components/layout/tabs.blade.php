<div {{ $attributes->class('border-b border-black-50 dark:border-slate-700') }}>
    <div class="sm:hidden pb-4 {{ $size === 'small' ? 'pt-2' : 'pt-4' }}">
        <x-select name="tabs" :options="$tabs" x-model="{{ $key }}" />
    </div>
    <div class="hidden sm:block">
        <nav class="-mb-px max-w-full overflow-x-auto flex {{ $size === 'small' ? 'space-x-6' : 'space-x-8' }}" aria-label="Tabs">
            {{ $slot }}
        </nav>
    </div>
</div>
