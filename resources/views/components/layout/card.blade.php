@props([
    'heading',
    'footer',
])

<div {{ $attributes->class(['border border-black-50 dark:border-slate-700 sm:rounded-lg bg-white dark:bg-slate-900 px-4 sm:px-6 flex flex-col']) }}>
    @isset($heading)
        <div {{ $heading->attributes->class(['py-4 font-medium text-lg sm:rounded-t-lg border-b border-black-50 dark:border-slate-700 flex-shrink-0']) }}>
            {{ $heading }}
        </div>
    @endisset

    <div class="py-4 sm:py-6 flex-1">
        {{ $slot }}
    </div>

    @isset($footer)
        <div {{ $footer->attributes->class(['py-4 sm:rounded-b-lg border-t border-black-50 dark:border-slate-700 flex-shrink-0']) }}>
            {{ $footer }}
        </div>
    @endisset
</div>
