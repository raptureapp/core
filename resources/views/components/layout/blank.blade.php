<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="{{ $theme }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title ? $title . ' | ' : '' }}{{ config('app.name', 'Haven') }}</title>

    @resource('rapture')

    @prepend('styles')
        @production
            <link rel="stylesheet" href="{{ asset('rapture/css/core.css') }}">
        @else
            <link rel="stylesheet" href="{{ asset('rapture/css/dev.css') }}">
            <script src="https://cdn.tailwindcss.com?plugins=forms,typography"></script>
            <script>
                tailwind.config = {
                    darkMode: ['variant', [
                        '@media (prefers-color-scheme: dark) { &:not(.light *) }',
                        '&:is(.dark *)',
                    ]],
                    theme: {
                        extend: {
                            fontFamily: {
                                sans: ['Inter', 'sans-serif'],
                            },
                            colors: {
                                'primary': tailwind.colors.{{ config('rapture.branding.color') }},
                                'light': '#F8F8F9',
                                'black': {
                                    '50': '#d3d6dc',
                                    '100': '#b6b7ba',
                                    '200': '#939598',
                                    '300': '#626469',
                                    '400': '#43464c',
                                    '500': '#14181f',
                                    '600': '#12161c',
                                    '700': '#0e1116',
                                    '800': '#0b0d11',
                                    '900': '#080a0d',
                                },
                                'gray': {
                                    '50': '#f1f2f4',
                                    '100': '#d3d6dc',
                                    '200': '#bdc2cb',
                                    '300': '#9fa6b4',
                                    '400': '#8d95a5',
                                    '500': '#707a8f',
                                    '600': '#666f82',
                                    '700': '#505766',
                                    '800': '#3e434f',
                                    '900': '#2f333c',
                                },
                            },
                        },
                    },
                }
            </script>
        @endproduction
    @endprepend

    @stack('styles')

    <script src="https://cdnjs.cloudflare.com/polyfill/v3/polyfill.min.js?flags=gated&features=es2015"></script>
    @livewireStyles
</head>
<body {{ $attributes->class('antialiased') }}>
    {{ $slot }}

    @stack('components')
    @livewireScripts
    @stack('scripts')
</body>
</html>
