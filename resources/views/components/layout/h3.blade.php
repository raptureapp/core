<h3 {{ $attributes->class(['font-semibold leading-6 sm:truncate']) }}>{{ $slot }}</h3>
