<div {{ $attributes->class([
        'max-w-7xl mx-auto px-4 sm:px-6 lg:px-8',
        'py-6 lg:py-8' => $padding === 'normal',
    ]) }}>
    {{ $slot }}
</div>
