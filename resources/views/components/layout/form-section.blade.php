<div {{ $attributes->class(['xl:grid xl:grid-cols-3 xl:gap-6']) }}>
    <div class="xl:col-span-1">
        <x-h3>{{ $title }}</x-h3>
        <div class="text-gray-600 dark:text-slate-400">
            @if (!empty($desc))
                <p class="mt-1 text-sm">
                    {{ $desc }}
                </p>
            @endif
            {{ $description ?? '' }}
        </div>
    </div>
    <div class="mt-4 xl:mt-0 xl:col-span-2">
        @if ($wrap)
            <x-box>{{ $slot }}</x-box>
        @else
            {{ $slot }}
        @endif
    </div>
</div>
