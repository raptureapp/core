@props([
    'footer',
    'header',
    'actions',
])

<div x-id="['modal-title']" :aria-labelledby="$id('modal-title')" {{ $attributes
    ->merge([
        'x-show' => $trigger,
        'style' => 'display: none',
        'role' => 'dialog',
        'aria-modal' => 'true',
        '@keydown.window.escape.stop' => $trigger . ' = false',
        '@open-modal.window' => 'if ($event.detail === ' . $trigger . ') { ' . $trigger . ' = true }',
        '@close-modal.window' => 'if ($event.detail === ' . $trigger . ') { ' . $trigger . ' = false }',
    ])
    ->class([
        'fixed inset-0 z-50 flex justify-center p-4 sm:p-8',
        'items-start' => $align === 'top',
        'items-center' => $align === 'center',
        'items-end' => $align === 'bottom',
    ]) }}>
    <div x-show="{{ $trigger }}" x-transition.opacity class="fixed inset-0 bg-black-500/20 dark:bg-black-700/70" @click="{{ $trigger }} = false"></div>

    <div x-show="{{ $trigger }}" x-transition @class([
        "relative w-full bg-light dark:bg-slate-900 text-greyscale-900 dark:text-slate-200 rounded-lg max-h-full border border-black-50 dark:border-slate-700 flex flex-col",
        "max-w-3xl" => $size === 'normal',
        "max-w-5xl" => $size === 'large',
    ])>
        <div class="px-4 sm:px-6 py-4 bg-white dark:bg-slate-800 flex items-center justify-between rounded-t-lg border-b border-black-50 dark:border-slate-700 flex-shrink-0">
            @isset($header)
                <div :id="$id('modal-title')" {{ $header->attributes }}>
                    {{ $header }}
                </div>
            @else
                <h2 class="text-lg font-medium text-greyscale-900 dark:text-slate-100" :id="$id('modal-title')">{{ $title }}</h2>
            @endisset
            <div class="flex items-center gap-4">
                @isset($actions)
                    <div {{ $actions->attributes }}>
                        {{ $actions }}
                    </div>
                @endisset
                <button type="button" class="text-lg text-gray-500 dark:text-slate-300" @click="{{ $trigger }} = false">
                    <em class="far fa-times"></em>
                </button>
            </div>
        </div>

        <div class="flex-1 overflow-y-auto">
            {{ $slot }}
        </div>

        @isset($footer)
            <div {{ $footer->attributes->class(['px-4 sm:px-6 py-4 bg-white dark:bg-slate-800 border-t border-black-50 dark:border-slate-700 rounded-b-lg']) }}>
                {{ $footer }}
            </div>
        @endisset
    </div>
</div>
