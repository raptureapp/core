<button type="button" @click="tabVariable = '{{ $key }}'" {{ $attributes->class([
    'group inline-flex items-center border-b-2 font-medium transition',
    'py-2.5 px-1 text-sm' => $size === 'small',
    'py-4 px-2 text-base' => $size === 'normal',
    ]) }} :class="{ 'border-primary-500 text-primary-600 dark:text-primary-500 dark:border-primary-400': tabVariable === '{{ $key }}', 'border-transparent text-gray-500 hover:text-gray-700 hover:border-black-50 dark:text-slate-300 dark:hover:text-white': tabVariable !== '{{ $key }}' }">
    @if ($icon)
        <em class="fa-regular fa-{{ $icon }} -ml-0.5 mr-3 transition" :class="{ 'text-primary-500 dark:text-primary-700': tabVariable === '{{ $key }}', 'text-gray-400 group-hover:text-gray-500 dark:text-slate-400 dark:group-hover:text-slate-200': tabVariable !== '{{ $key }}' }"></em>
    @endif
    <span>{{ $label }}</span>
    @if (!is_null($badge))
        <span class="ml-3 hidden rounded-full py-0.5 px-2.5 text-xs font-medium md:inline-block transition" :class="{ 'bg-primary-100 dark:bg-primary-700/50 text-primary-800 dark:text-primary-200 ring-primary-700/20 dark:ring-primary-400/30': tabVariable === '{{ $key }}', 'bg-gray-100 dark:bg-gray-400/20 text-gray-800 dark:text-slate-300 group-hover:bg-gray-200 group-hover:text-gray-700 dark:group-hover:bg-gray-300/20 dark:group-hover:text-white': tabVariable !== '{{ $key }}' }">{{ $badge }}</span>
    @endif
</button>
