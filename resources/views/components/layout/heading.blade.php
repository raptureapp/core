<div {{ $attributes->whereDoesntStartWith('class') }}>
    <div {{ $attributes->whereStartsWith('class')->class([
        'max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 pt-6 lg:pt-8',
    ]) }}>
        @if ($label)
            <x-h1>{{ $label }}</x-h1>
        @endif

        {{ $slot }}
    </div>

    @if (!$noStatus)
        <x-statuses />
    @endif
</div>
