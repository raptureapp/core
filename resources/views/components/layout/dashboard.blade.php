<x-blank :title="$title">
    <div {{ $attributes }}>
        <div class="h-screen print:h-auto flex overflow-hidden bg-light dark:bg-slate-950 selection:text-white selection:bg-primary-800 text-gray-900 dark:text-slate-100" x-data="{ sidebarOpen: false }" @keydown.window.escape="sidebarOpen = false">
            <livewire:dashboard-menu />

            <div class="print:block w-0 flex-1 overflow-y-auto">
                <div class="relative z-30 flex h-16 bg-white dark:bg-slate-900 print:hidden border-b border-black-50 dark:border-slate-700 sticky top-0">
                    <button @click.stop="sidebarOpen = true" class="px-4 focus:outline-none focus:bg-gray-100 focus:text-gray-600 lg:hidden">
                        <span class="sr-only">Open sidebar</span>
                        <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h7" />
                        </svg>
                    </button>
                    <div class="flex-1 flex justify-between gap-4 sm:gap-6 lg:gap-8 max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                        <div class="flex-1 flex items-center">
                            @hook('dashboard.top.left')
                        </div>
                        <div class="flex items-center space-x-4">
                            @hook('dashboard.top.right')

                            <div class="relative" x-data="{ open: false }">
                                <button class="flex items-center justify-left text-left text-xs focus:outline-none focus:ring focus:ring-primary-600 dark:focus:ring-primary-700 space-x-3 p-2 rounded-lg" id="user-menu" aria-label="User menu" aria-haspopup="true" @click="open = !open" x-bind:aria-expanded="open" aria-expanded="false">
                                    <img class="h-8 w-8 rounded-full" src="{{ auth()->user()->avatar() }}" alt="{{ auth()->user()->name }}" />
                                    <div class="hidden sm:block">
                                        <p class="text-greyscale-900 font-medium">{{ auth()->user()->fullName() }}</p>
                                        <p class="text-greyscale-500 dark:text-slate-400 mt-0.5">{{ auth()->user()->email }}</p>
                                    </div>
                                </button>

                                <x-dropdown role="menu" aria-orientation="vertical" aria-labelledby="user-menu">
                                    <div class="sm:hidden text-xs px-2 pt-2">
                                        <p class="text-greyscale-900 font-medium">{{ auth()->user()->fullName() }}</p>
                                        <p class="text-greyscale-500 dark:text-slate-400 mt-0.5">{{ auth()->user()->email }}</p>
                                    </div>
                                    <x-dropdown-divider class="sm:hidden" />
                                    @if (config('rapture.branding.dark_override'))
                                        <div class="px-1 sm:pt-1">
                                            <livewire:dark-mode-toggle />
                                        </div>
                                        <x-dropdown-divider />
                                    @endif
                                    <x-dropdown-item icon="address-card" label="rapture::account.menu_label" :href="route('dashboard.account.index')" />
                                    @if (auth()->user()->id === 1)
                                        <x-dropdown-item icon="puzzle-piece" label="packages::package.plural" :href="route('dashboard.packages.index')" />
                                    @endif
                                    @can('core.branding')
                                        <x-dropdown-item icon="paintbrush-pencil" label="Branding" :href="route('dashboard.branding')" />
                                    @endcan
                                    @hook('dashboard.account.menu')
                                    <x-dropdown-divider />
                                    <x-dropdown-item icon="power-off" label="Logout" color="danger" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" />

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                        @csrf
                                    </form>
                                </x-dropdown>
                            </div>
                        </div>
                    </div>
                </div>

                <main class="relative focus:outline-none" tabindex="0">
                    {{ $slot }}
                </main>
            </div>
        </div>
    </div>
</x-blank>
