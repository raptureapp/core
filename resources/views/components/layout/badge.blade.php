<span {{ $attributes->class([
    'inline-flex items-center rounded-md font-medium ring-1 ring-inset',
    'px-4 py-1.5 text-base' => $size === 'large',
    'px-3 py-1 text-sm' => $size === 'normal',
    'px-2 py-1 text-xs' => $size === 'small',
    'px-1.5 py-0.5 text-xs' => $size === 'xsmall',
    'bg-gray-50 dark:bg-gray-800/40 text-gray-800 dark:text-slate-300 ring-gray-500/20 dark:ring-gray-400/50' => $color === 'gray',
    'bg-red-100 dark:bg-red-800/30 text-red-800 dark:text-red-200 ring-red-700/20 dark:ring-red-400/30' => $color === 'red',
    'bg-orange-100 dark:bg-orange-800/20 text-orange-800 dark:text-orange-200 ring-orange-700/20 dark:ring-orange-400/30' => $color === 'orange',
    'bg-yellow-100 dark:bg-yellow-800/10 text-yellow-800 dark:text-yellow-200 ring-yellow-700/20 dark:ring-yellow-400/30' => $color === 'yellow',
    'bg-green-100 dark:bg-green-800/30 text-green-800 dark:text-green-200 ring-green-700/20 dark:ring-green-400/30' => $color === 'green',
    'bg-blue-100 dark:bg-blue-800/30 text-blue-800 dark:text-blue-200 ring-blue-700/20 dark:ring-blue-400/40' => $color === 'blue',
    'bg-indigo-100 dark:bg-indigo-800/20 text-indigo-800 dark:text-indigo-200 ring-indigo-700/20 dark:ring-indigo-400/40' => $color === 'indigo',
    'bg-purple-100 dark:bg-purple-800/20 text-purple-800 dark:text-purple-200 ring-purple-700/20 dark:ring-purple-400/30' => $color === 'purple',
    'bg-pink-100 dark:bg-pink-800/20 text-pink-800 dark:text-pink-200 ring-pink-700/20 dark:ring-pink-400/30' => $color === 'pink',
    'bg-primary-100 dark:bg-primary-800/20 text-primary-800 dark:text-primary-200 ring-primary-700/40 dark:ring-primary-400/30' => $color === 'primary',
]) }}>{{ $slot }}</span>
