<x-blank :title="$title">
    <div class="min-h-screen bg-light dark:bg-slate-950 flex flex-col justify-center py-12 sm:px-6 lg:px-8 text-gray-900 dark:text-slate-200">
        <div class="sm:mx-auto sm:w-full sm:max-w-sm">
            @if (session('status'))
                <div class="bg-green-700 dark:bg-green-800">
                    <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8 py-4 mb-4">
                        <div class="flex">
                            <div class="flex-shrink-0">
                                <svg class="h-5 w-5 text-white dark:text-gray-200 opacity-75" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                                </svg>
                            </div>
                            <div class="ml-3">
                                <h3 class="text-sm leading-5 font-medium text-white dark:text-gray-200">
                                    {{ session('status') }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="bg-red-800 dark:bg-red-900 rounded-lg">
                    <div class="max-w-7xl mx-auto px-4 sm:px-6 py-4 mb-4">
                        <div class="text-sm leading-5 text-white">
                            <ul class="space-y-2">
                                @foreach ($errors->all() as $error)
                                <li><em class="far fa-caret-right mr-1"></em> {{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif

            <x-box>
                <x-h2 class="mb-8">
                    {{ $title }}
                </x-h2>

                {{ $slot }}
            </x-box>
        </div>
    </div>
</x-blank>
