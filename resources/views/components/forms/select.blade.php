<div {{ $attributes->whereStartsWith(['class', 'x-show', 'x-transition', 'style']) }}>
    @if ($label)
        <x-label for="{{ $id }}" :required="$required" class="mb-1">{{ $label }}</x-label>
    @endif
    <div @class([
        'relative' => !empty($prepend) || !empty($append),
    ])>
        @if ($prepend)
            <div class="pointer-events-none absolute inset-y-0 left-0 pl-3 flex items-center text-gray-500 dark:text-slate-300">
                {{ $prepend }}
            </div>
        @endif
        <select @if ($name) name="{{ $name }}" @endif id="{{ $id }}" {{ $attributes->whereDoesntStartWith(['class', 'x-show', 'x-transition', 'style'])->class([
            'pr-10 py-2 text-base focus:outline-none focus:ring-primary-600 sm:text-sm dark:bg-slate-900 dark:text-white dark:focus:ring-primary-700 cursor-pointer w-full text-black-500 dark:text-white',
            'pl-10' => !empty($prepend) && strlen($prepend) != 1,
            'pl-8' => !empty($prepend) && strlen($prepend) === 1,
            'pl-3' => empty($prepend),
            $shape => $shape !== 'square' && $shape !== 'rounded',
            'rounded-lg' => $shape === 'rounded',
            'border-red-400 dark:border-red-600 focus:border-red-400 dark:focus:border-red-600' => $error,
            'border-black-50 dark:border-slate-700 focus:border-primary-500 dark:focus:border-primary-600' => !$error,
            $inputClass,
            ]) }}>
            @if (!empty($placeholder))
                <option value="">{{ $placeholder }}</option>
            @endif
            @foreach ($options as $key => $value)
                @if (!is_null($selected) && $key == $selected)
                    <option value="{{ $key }}" selected>{{ $value }}</option>
                @else
                    <option value="{{ $key }}">{{ $value }}</option>
                @endif
            @endforeach
            {{ $slot }}
        </select>
    </div>
</div>
