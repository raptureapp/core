<label {{ $attributes->class([
    'block text-sm font-medium leading-5 text-gray-700 dark:text-slate-200',
]) }}>
    {{ $slot }}

    @if ($required)
        <span class="text-red-400">*</span>
    @endif
</label>
