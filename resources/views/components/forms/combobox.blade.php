<div x-data="combobox"
    @if ($selected || old($name))
        x-init="init('{{ old($name, $selected) }}')"
    @endif
    @click="prepare()"
    @click.outside="close()"
    wire:ignore
    {{ $attributes->whereStartsWith(['class', 'x-show', 'x-transition', 'style']) }}>
    @if ($label)
        <x-label for="{{ $id }}" :required="$required" class="mb-1">{{ $label }}</x-label>
    @endif

    <div class="relative">
        <input type="hidden" @if ($name) name="{{ $name }}" @endif @if (count($attributes->whereStartsWith('x-model')->getAttributes()) > 0) x-modelable="selected" @elseif (count($attributes->whereStartsWith('wire:model')->getAttributes()) === 0) x-model="selected" @else x-modelable="selected" @endif x-ref="result" {{ $attributes->whereStartsWith(['wire', 'x-']) }}>

        <div class="relative">
            @if ($prepend)
                <div class="pointer-events-none absolute inset-y-0 left-0 pl-3 flex items-center text-gray-500 dark:text-slate-300">
                    {{ $prepend }}
                </div>
            @endif
            <input type="text" x-bind="searchField" x-model="search" x-ref="search" placeholder="{{ $placeholder ?? 'Select an option' }}" autocomplete="off" id="{{ $id }}" {{ $attributes->whereDoesntStartWith(['class', 'wire', 'x-'])->class([
                'py-2 border placeholder-gray-500 dark:placeholder-gray-300 focus:outline-none focus:ring-primary-600 sm:text-sm leading-5 dark:bg-slate-900 dark:text-white dark:focus:ring-primary-700 rounded-lg w-full text-black-500 dark:text-white',
                'pl-10' => !empty($prepend) && strlen($prepend) != 1,
                'pl-8' => !empty($prepend) && strlen($prepend) === 1,
                'pl-3' => empty($prepend),
                'pr-10' => !$nullable,
                'pr-16' => $nullable,
                'border-red-400 dark:border-red-600 focus:border-red-400 dark:focus:border-red-600' => $error,
                'border-black-50 dark:border-slate-700 focus:border-primary-500 dark:focus:border-primary-600' => !$error,
            ]) }}>
            @if ($nullable)
            <button type="button" class="absolute inset-y-0 right-0 mr-9 flex items-center px-1.5" x-show="selected !== ''" style="display: none;" @click="unset()">
                <em class="text-gray-500 dark:text-slate-300 far fa-times"></em>
            </button>
            @endif
            <div class="absolute inset-y-0 right-0 pr-3.5 flex items-center pointer-events-none">
                <em class="text-gray-500 dark:text-slate-300 far fa-angle-down"></em>
            </div>
        </div>

        <div x-show="displaying" style="display: none;" class="absolute z-10 top-full w-full bg-white dark:bg-slate-900 border border-black-50 dark:border-slate-700 shadow-md rounded-lg mt-2 max-h-72 overflow-auto py-2" x-ref="selection">
            @foreach ($options as $key => $option)
                <button type="button" @click="select('{{ $key }}')" :class="{ 'bg-primary-600 text-white dark:bg-primary-700': selected == '{{ $key }}', 'hover:bg-gray-50 dark:hover:bg-slate-800': selected != '{{ $key }}', 'hidden': search != preview && !`{{ $option }}`.toLowerCase().includes(search.toLowerCase()) }" class="text-sm truncate py-1.5 px-3 w-full text-left transition" tabindex="-1" data-key="{{ $key }}">
                    {{ $option }}
                </button>
            @endforeach
        </div>
    </div>

    @once
        @push('components')
            <script>
                document.addEventListener('alpine:init', () => {
                    Alpine.data('combobox', () => ({
                        displaying: false,
                        selected: '',
                        preview: '',
                        search: '',

                        init(key) {
                            // x-model
                            this.$watch('selected', () => {
                                const option = this.$refs.selection.querySelector(`[data-key="${this.selected}"]`);

                                if (!option) {
                                    return;
                                }

                                this.preview = option.innerText.trim();
                                this.search = option.innerText.trim();
                            });

                            // value/old
                            if (key) {
                                this.set(key);
                            }
                        },

                        set(key = '') {
                            this.selected = key;
                        },

                        unset() {
                            this.selected = '';
                            this.preview = '';
                            this.search = '';
                            this.$refs.search.focus();
                        },

                        select(key) {
                            this.set(key);
                            this.$refs.result.dispatchEvent(new CustomEvent('input', {
                                bubbles: true,
                                detail: key,
                            }));

                            setTimeout(() => {
                                this.displaying = false;
                            }, 50);
                        },

                        prepare() {
                            this.displaying = true;
                        },

                        close() {
                            this.displaying = false;
                        },

                        searchField: {
                            ['@focus']() {
                                this.displaying = true;
                                this.search = this.preview;
                            },
                            ['@input']() {
                                this.prepare();
                            },
                            ['@keydown.escape.stop']() {
                                this.displaying = false;
                                this.search = this.preview;
                                this.$refs.search.blur();
                            },
                            ['@keydown.enter.prevent.stop']() {
                            },
                        },
                    }))
                })
            </script>
        @endpush
    @endonce
</div>
