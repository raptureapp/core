<div x-data="{ input: 'password', value: '', caps: false, init() {
    $refs.password.addEventListener('keydown', (event) => {
        this.caps = event.getModifierState && event.getModifierState('CapsLock');
    });
} }" {{ $attributes->whereStartsWith(['class', 'x-show', 'x-transition', 'style']) }}>
    @if ($label)
        <x-label for="{{ $id }}" :required="$required" class="mb-1">{{ $label }}</x-label>
    @endif
    <div @class([
        'flex',
        $shape => $shape !== 'square' && $shape !== 'rounded',
        'rounded-lg' => $shape === 'rounded',
    ])>
        <x-input class="w-full" x-model="value" x-show="input === 'text'" :fill="false" :shape="$generate || $reveal ? 'rounded-l-lg' : 'rounded'" :error="$error" :prepend="$prepend" :append="$append" {{ $attributes->whereDoesntStartWith(['class', 'x-show', 'x-transition', 'style', 'wire', 'x-model']) }} />
        <x-input x-ref="password" type="password" name="{{ $name }}" class="w-full" x-model="value" x-show="input === 'password'" :fill="false"  :shape="$generate || $reveal ? 'rounded-l-lg' : 'rounded'" :error="$error" :prepend="$prepend" :append="$append" id="{{ $id }}" {{ $attributes->whereDoesntStartWith(['class', 'x-show', 'x-transition', 'style']) }} />
        @if ($generate)
            <button class="-ml-px relative inline-flex items-center px-4 py-2 border border-black-50 text-sm leading-5 font-medium text-gray-700 dark:text-slate-200 dark:bg-slate-900 dark:border-slate-700 hover:bg-black-50 dark:hover:bg-slate-800 focus:outline-none focus:ring-primary-600 focus:border-primary-500 focus:z-10 transition ease-in-out dark:focus:ring-primary-700 dark:focus:border-primary-600 last:rounded-r-lg" type="button" title="Generate Password" @click="let chars = '0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ'; value = ''; for (let i = 0; i <= 12; i += 1) { let random = Math.floor(Math.random() * chars.length); value += chars.substring(random, random + 1); }">
                <span class="sr-only">Generate Password</span>
                <i class="w-5 far fa-key" aria-hidden="true"></i>
            </button>
        @endif
        @if ($reveal)
            <button class="-ml-px relative inline-flex items-center px-4 py-2 border border-black-50 text-sm leading-5 font-medium text-gray-700 dark:text-slate-200 dark:bg-slate-900 dark:border-slate-700 hover:bg-black-50 dark:hover:bg-slate-800 focus:outline-none focus:ring-primary-600 focus:border-primary-500 focus:z-10 transition ease-in-out dark:focus:ring-primary-700 dark:focus:border-primary-600 last:rounded-r-lg" type="button" @click="input = input === 'password' ? 'text' : 'password'" title="Toggle Visibility">
                <span class="sr-only">Toggle Visibility</span>
                <i class="w-5 far fa-eye" aria-hidden="true" x-show="input === 'password'"></i>
                <i class="w-5 far fa-eye-slash" aria-hidden="true" x-show="input === 'text'" style="display: none;"></i>
            </button>
        @endif
    </div>
    <p x-show="caps" style="display: none;" class="mt-2 text-sm text-red-600 dark:text-red-500">Caps lock is enabled</p>
</div>
