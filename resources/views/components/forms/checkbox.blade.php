<div {{ $attributes->whereStartsWith(['class', 'x-show', 'x-transition', 'style'])->class([
    'inline-flex items-center' => $inline,
    ]) }}>
    @if ($label)
        <x-label for="{{ $id }}" :required="$required" @class([
            'mb-1' => !$inline,
            'order-1 ml-3' => $inline,
        ])>{{ $label }}</x-label>
    @endif
    <div @class([
        'leading-3',
    ])>
        <input type="checkbox" @if ($name) name="{{ $name }}" @endif id="{{ $id }}" {{ $attributes->class([
            'border-2 h-5 w-5 text-primary-600 rounded-md focus:ring-primary-600 dark:focus:ring-primary-700 dark:bg-slate-900 transition',
            'border-red-400 dark:border-red-600 focus:border-red-400 dark:focus:border-red-600' => $error,
            'border-black-50 dark:border-slate-700 focus:border-primary-500 dark:focus:border-primary-600' => !$error,
        ]) }} {{ old($name, $checked) ? 'checked' : '' }} />
    </div>
</div>
