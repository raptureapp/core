<div id="{{ $id }}" wire:ignore x-data="htmleditor" x-init="create('{{ $placeholder }}', '{{ $toolbar }}')" x-modelable="content" {{ $attributes->class([
    'html-editor border border-black-50 dark:border-slate-700 rounded-lg bg-white dark:bg-slate-900',
    ]) }}>

    <x-input type="hidden" name="{{ $name }}" :value="$content" data-content />

    <div @class([
        'rounded-t-lg border-b border-black-50 dark:border-slate-700',
        'py-1.5 px-2 sm:px-4' => !$slim,
        'py-1 px-1.5 sm:px-2' => $slim,
    ]) data-toolbar></div>
    <div @class([
        'max-w-none prose dark:prose-invert',
        'py-2 px-2 sm:px-4' => !$slim,
        'py-1 px-1.5 sm:px-2' => $slim,
    ])>
        <div data-editor class="{{ $height }}"></div>
    </div>
    @if ($wordcount)
    <div @class([
        'text-gray-700 dark:text-slate-300 rounded-b-lg flex items-center justify-end text-sm space-x-4',
        'py-2 px-2 sm:px-4' => !$slim,
        'py-1.5 px-1.5 sm:px-2' => $slim,
    ]) data-counts>
        <span x-html="`${words} words`"></span>
        <span x-html="`${characters} characters`"></span>
    </div>
    @endif

    @pushOnce('styles')
        <script src="{{ asset('rapture/ckeditor.js') }}"></script>
    @endPushOnce

    @pushOnce('scripts')
    <script>
        function htmleditor() {
            let editor;
            let hasChanged = false;

            return {
                content: '',
                characters: 0,
                words: 0,
                create(placeholder = '', toolbar = '') {
                    DecoupledEditor
                        .create(this.$root.querySelector('[data-editor]'), {
                            placeholder,
                            toolbar: toolbar.split(','),
                            wordCount: {
                                onUpdate: stats => {
                                    this.characters = stats.characters;
                                    this.words = stats.words;
                                },
                            },
                            findAndReplace: {
                                uiType: 'dropdown',
                            },
                            mediaEmbed: {
                                removeProviders: [
                                    'twitter',
                                    'flickr',
                                ],
                            },
                            removePlugins: ["MediaEmbedToolbar"],
                            link: {
                                decorators: {
                                    openInNewTab: {
                                        mode: 'manual',
                                        label: 'Open in a new tab',
                                        attributes: {
                                            target: '_blank',
                                            rel: 'noopener noreferrer',
                                        },
                                    },
                                },
                            },
                        })
                        .then(newEditor => {
                            const toolbarContainer = this.$root.querySelector('[data-toolbar]');
                            toolbarContainer.appendChild(newEditor.ui.view.toolbar.element);

                            editor = newEditor;

                            if (this.content !== null && this.content.length > 0) {
                                editor.setData(this.content);
                            } else if (this.$root.querySelector('[data-content]')) {
                                editor.setData(this.$root.querySelector('[data-content]').value);
                            }

                            editor.model.document.on('change:data', () => {
                                hasChanged = true;
                            });

                            editor.editing.view.document.on('change:isFocused', (evt, data, isFocused) => {
                                if (!isFocused && hasChanged) {
                                    this.content = editor.getData();
                                    this.$dispatch('input', editor.getData());

                                    if (this.$root.querySelector('[data-content]')) {
                                        this.$root.querySelector('[data-content]').value = editor.getData();
                                    }
                                }
                            });

                            this.$watch('content', (value, oldValue) => {
                                if (oldValue == value || hasChanged) {
                                    hasChanged = false;
                                    return;
                                }

                                editor.setData(value);
                            });
                        })
                        .catch(error => {
                            console.error(error);
                        });
                },
            }
        }
    </script>
    @endPushOnce

    @pushOnce('styles')
        <style>
            .html-editor {
                --ck-border-radius: 0.25rem;
                --ck-color-toolbar-border: transparent;
                --ck-color-base-border: transparent;
                --ck-focus-ring: none;
                --ck-inner-shadow: transparent;
                --ck-color-toolbar-background: transparent;
                --ck-color-labeled-field-label-background: transparent;
            }

            .ck-powered-by-balloon {
                display: none !important;
            }

            html.dark .html-editor {
                --ck-color-dropdown-panel-background: #1e293b;
                --ck-color-list-background: #1e293b;
                --ck-color-list-button-hover-background: #1e293b;
                --ck-color-text: #f3f4f6;
                --ck-color-button-default-hover-background: #1e293b;
                --ck-color-split-button-hover-background: #1e293b;
                --ck-color-button-default-active-background: #1e293b;
                --ck-color-button-on-active-background: #1e293b;
                --ck-color-split-button-hover-border: #334155;
                --ck-color-dropdown-panel-border: #334155;
                --ck-color-engine-placeholder-text: #f3f4f6;
                --ck-color-button-on-background: #1e293b;
                --ck-color-button-on-hover-background: #1e293b;
                --ck-color-button-on-color: #ffffff;
                --ck-color-input-background: #475569;
                --ck-color-input-border: #9ca3af;
            }

            @media (prefers-color-scheme: dark) {
                html:not(.light) .html-editor {
                    --ck-color-dropdown-panel-background: #1e293b;
                    --ck-color-list-background: #1e293b;
                    --ck-color-list-button-hover-background: #1e293b;
                    --ck-color-text: #f3f4f6;
                    --ck-color-button-default-hover-background: #1e293b;
                    --ck-color-split-button-hover-background: #1e293b;
                    --ck-color-button-default-active-background: #1e293b;
                    --ck-color-button-on-active-background: #1e293b;
                    --ck-color-split-button-hover-border: #334155;
                    --ck-color-dropdown-panel-border: #334155;
                    --ck-color-engine-placeholder-text: #f3f4f6;
                    --ck-color-button-on-background: #1e293b;
                    --ck-color-button-on-hover-background: #1e293b;
                    --ck-color-button-on-color: #ffffff;
                    --ck-color-input-background: #475569;
                    --ck-color-input-border: #9ca3af;
                }
            }
        </style>
    @endPushOnce
</div>
