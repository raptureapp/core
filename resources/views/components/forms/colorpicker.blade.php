<div x-data="colorPicker('{{ $value }}', '{{ $format }}')" x-modelable="selected" wire:ignore {{ $attributes->whereDoesntStartWith(':') }}>
    @if ($label)
        <x-label for="{{ $id }}" :required="$required" class="mb-1">{{ $label }}</x-label>
    @endif
    @if ($swatch)
    <div x-data="{ picking: false }" class="inline-block relative">
        <button type="button" class="p-1 border border-black-50 dark:border-slate-700 rounded-lg" @click="picking = !picking">
            <span class="block h-7 w-7 rounded" :style="`background-color: ${selected}`"></span>
        </button>
        <div class="absolute top-full mt-2 {{ $align === 'left' ? 'left-0' : 'right-0' }} z-10 pt-2 px-2 -mx-2" x-show="picking" @click.away="picking = false" style="display: none;">
    @endif

    <div class="bg-white dark:bg-slate-900 rounded-lg border border-black-50 dark:border-slate-700 w-72">
        <div class="pt-4 px-4">
            <div class="w-64 h-48 w-full relative select-none" @mousedown="enablePaletteEditing($event)" @mousemove="adjustPalette($event)" @mouseup.window="disablePaletteEditing()" style="background: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyIDIiPjxwYXRoIGZpbGw9IndoaXRlIiBkPSJNMSwwSDJWMUgxVjBaTTAsMUgxVjJIMFYxWiIvPjxwYXRoIGZpbGw9ImdyYXkiIGQ9Ik0wLDBIMVYxSDBWMFpNMSwxSDJWMkgxVjFaIi8+PC9zdmc+DQo='); background-size: 0.5em;">
                <div class="w-full h-full" :style="paletteStyle()"></div>
                <div class="absolute border-2 border-black-50 dark:border-slate-700 rounded-full w-4 h-4 transform -translate-y-2/4 -translate-x-2/4 pointer-events-none" :style="palettePointer()"></div>
            </div>
        </div>

        <div class="p-4">
            <div class="flex items-center space-x-5">
                <div class="w-8 h-8 p-1 border border-black-50 dark:border-slate-700 rounded-lg">
                    <div class="w-full h-full rounded" :style="`background-color: ${selected}`"></div>
                </div>
                <div class="flex-1 space-y-1">
                    <div class="relative select-none py-1" @mousedown="enableHueEditing($event)" @mousemove="adjustHue($event)" @mouseup.window="disableHueEditing()" @mouseleave="disableHueEditing()">
                        <div class="w-full h-2 rounded-full" style="background: linear-gradient(90deg,red,#ff0,#0f0,#0ff,#00f,#f0f,red);"></div>
                        <div class="absolute top-1/2 transform -translate-y-2/4 -translate-x-2/4 h-4 w-4 border-2 border-black-50 dark:border-slate-700 rounded-full pointer-events-none" :style="hueStyle()"></div>
                    </div>

                    @if (!$solid)
                        <div class="relative select-none py-1" @mousedown="enableOpacityEditing($event)" @mousemove="adjustOpacity($event)" @mouseup.window="disableOpacityEditing()" @mouseleave="disableOpacityEditing()">
                            <div class="w-full h-2 rounded-full" style="background: linear-gradient(90deg,transparent,#000),url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyIDIiPjxwYXRoIGZpbGw9IndoaXRlIiBkPSJNMSwwSDJWMUgxVjBaTTAsMUgxVjJIMFYxWiIvPjxwYXRoIGZpbGw9ImdyYXkiIGQ9Ik0wLDBIMVYxSDBWMFpNMSwxSDJWMkgxVjFaIi8+PC9zdmc+DQo=')"></div>
                            <div class="absolute top-1/2 transform -translate-y-2/4 -translate-x-2/4 h-4 w-4 border-2 border-black-50 rounded-full pointer-events-none" :style="opacityStyle()"></div>
                        </div>
                    @endif
                </div>
            </div>

            <x-input x-model="hex" @blur="hexToHSL(); updateSelection();" prepend="#" class="mt-4" />
            <input type="hidden" x-model="selected" @if ($name) name="{{ $name }}" @endif {{ $attributes->whereStartsWith(':') }} />

            @if (count($presets) > 0)
                <x-label class="mb-1 mt-4">Presets</x-label>
                <div class="grid grid-cols-4 gap-2">
                    @foreach ($presets as $preset)
                        <button type="button" class="aspect-square rounded-lg border border-white dark:border-slate-700" style="background-color: {{ $preset }};" @click="selectPreset('{{ $preset }}')"></button>
                    @endforeach
                </div>
            @endif
        </div>
    </div>

    @if ($swatch)
        </div>
    </div>
    @endif

    @once
        @push('components')
            <script>
                document.addEventListener('alpine:init', () => {
                    Alpine.data('colorPicker', (selected, format = 'rgb') => ({
                        hex: '',
                        rgb: '',
                        base: 'rgb(255, 0, 0)',
                        opacity: 100,
                        selected,
                        format,

                        hue: 0,
                        lightness: 0.5,
                        saturation: 1,

                        x: 100,
                        y: 0,
                        hueBase: 0,
                        opacityEditing: false,
                        hueEditing: false,
                        paletteEditing: false,

                        init() {
                            if (!this.selected) {
                                // Account for x-model
                                setTimeout(() => {
                                    if (this.selected) {
                                        this.prepareSelection(this.selected);
                                    }
                                }, 50)
                                return;
                            }

                            this.prepareSelection(this.selected);
                        },
                        selectPreset(value) {
                            this.prepareSelection(value);
                            this.updateSelection();
                        },
                        prepareSelection(value) {
                            if (value.substr(0, 1) === '#') {
                                this.hex = value.substr(1);
                                this.hexToHSL();
                                this.rgb = this.HSLToRGB(this.hue, this.saturation, this.lightness);
                            } else {
                                const rgb = value.split("(")[1].split(")")[0].split(",");
                                let opacity = 1;

                                if (rgb.length > 3) {
                                    opacity = parseFloat(rgb.pop().trim());

                                    if (opacity < 1) {
                                        rgb.push(opacity * 255);
                                    }
                                }

                                this.hex = rgb.map((value) => {
                                    let hex = parseInt(value.trim(), 10).toString(16);

                                    if (hex.length == 1) {
                                        hex = `0${hex}`;
                                    }

                                    return hex;
                                }).join('');

                                this.hexToHSL();
                            }
                        },

                        opacityStyle() {
                            return `background: rgba(0, 0, 0, ${this.opacity / 100}); left: ${this.opacity}%;`;
                        },
                        hueStyle() {
                            return `background: ${this.base}; left: ${this.hueBase}%;`;
                        },
                        paletteStyle() {
                            if (this.opacity < 100) {
                                const opacity = this.opacity / 100;
                                return `background: linear-gradient(to top, rgba(0, 0, 0, ${opacity}), transparent), linear-gradient(to left, ${this.HSLToRGB(this.hue, 1, 0.5)}, rgba(255, 255, 255, ${opacity}));`;
                            }

                            return `background: linear-gradient(to top, rgb(0, 0, 0), transparent), linear-gradient(to left, ${this.base}, rgb(255, 255, 255));`;
                        },
                        palettePointer() {
                            return `background-color: ${this.selected}; top: ${this.y}%; left: ${this.x}%;`;
                        },

                        updateSelection() {
                            this.rgb = this.HSLToRGB(this.hue, this.saturation, this.lightness);
                            this.hex = this.HSLToHex(this.hue, this.saturation, this.lightness);

                            if (this.format === 'hex') {
                                this.selected = `#${this.hex}`;
                            } else {
                                this.selected = this.rgb;
                            }

                            this.$el.dispatchEvent(new CustomEvent('input', {
                                bubbles: true,
                                detail: this.selected,
                            }));
                        },

                        enablePaletteEditing(event) {
                            this.updatePalette(event);
                            this.paletteEditing = true;
                        },
                        disablePaletteEditing() {
                            this.paletteEditing = false;
                        },
                        adjustPalette(event) {
                            if (this.paletteEditing) {
                                this.updatePalette(event);
                            }
                        },
                        updatePalette(event) {
                            this.x = Math.round(event.offsetX / event.target.clientWidth * 100);
                            this.y = Math.round(event.offsetY / event.target.clientHeight * 100);

                            this.x = Math.min(Math.max(this.x, 0), 100);
                            this.y = Math.min(Math.max(this.y, 0), 100);

                            const hsv_value = 1 - (this.y / 100);
                            const hsv_saturation = this.x / 100;

                            this.lightness = (hsv_value / 2) * (2 - hsv_saturation);
                            this.saturation = (hsv_value * hsv_saturation) / (1 - Math.abs(2 * this.lightness - 1));
                            this.updateSelection();
                        },

                        enableHueEditing(event) {
                            this.updateHue(event);
                            this.hueEditing = true;
                        },
                        disableHueEditing() {
                            this.hueEditing = false;
                        },
                        adjustHue(event) {
                            if (this.hueEditing) {
                                this.updateHue(event);
                            }
                        },
                        updateHue(event) {
                            const difference = event.offsetX / event.target.clientWidth;

                            this.hue = Math.round(difference * 360);
                            this.hueBase = Math.round(difference * 100);
                            this.base = this.HSLToRGB(this.hue, 1, 0.5, false);
                            this.updateSelection();
                        },

                        enableOpacityEditing(event) {
                            this.updateOpacity(event);
                            this.opacityEditing = true;
                        },
                        disableOpacityEditing() {
                            this.opacityEditing = false;
                        },
                        adjustOpacity(event) {
                            if (this.opacityEditing) {
                                this.updateOpacity(event);
                            }
                        },
                        updateOpacity(event) {
                            this.opacity = Math.round(event.offsetX / event.target.clientWidth * 100);
                            this.updateSelection();
                        },

                        HSLToRGB(h, s = 1, l = 0.5, withAlpha = true) {
                            const channels = this.convertHSLToRGB(h, s, l);

                            if (this.opacity < 100 && withAlpha) {
                                const opacity = this.opacity / 100;

                                return `rgba(${channels.join(', ')}, ${opacity})`;
                            }

                            return `rgb(${channels.join(', ')})`;
                        },

                        HSLToHex(h, s = 1, l = 0.5, withAlpha = true) {
                            const channels = this.convertHSLToRGB(h, s, l);

                            if (this.opacity < 100 && withAlpha) {
                                const opacity = Math.round(this.opacity / 100 * 255);
                                channels.push(opacity);
                            }

                            return channels.map((value) => {
                                let hex = value.toString(16);

                                if (hex.length == 1) {
                                    hex = `0${hex}`;
                                }

                                return hex;
                            }).join('');
                        },

                        convertHSLToRGB(h, s, l) {
                            let c = (1 - Math.abs(2 * l - 1)) * s,
                                x = c * (1 - Math.abs((h / 60) % 2 - 1)),
                                m = l - c/2,
                                r = 0,
                                g = 0,
                                b = 0;

                            if (0 <= h && h < 60) {
                                r = c; g = x; b = 0;
                            } else if (60 <= h && h < 120) {
                                r = x; g = c; b = 0;
                            } else if (120 <= h && h < 180) {
                                r = 0; g = c; b = x;
                            } else if (180 <= h && h < 240) {
                                r = 0; g = x; b = c;
                            } else if (240 <= h && h < 300) {
                                r = x; g = 0; b = c;
                            } else if (300 <= h && h < 360) {
                                r = c; g = 0; b = x;
                            }

                            r = Math.round((r + m) * 255);
                            g = Math.round((g + m) * 255);
                            b = Math.round((b + m) * 255);

                            return [r, g, b];
                        },

                        hexToHSL() {
                            if (![3,6,8].includes(this.hex.length)) {
                                return;
                            }

                            const H = this.hex;

                            let r = 0, g = 0, b = 0, o = 255;

                            if (H.length == 3) {
                                r = "0x" + H[0] + H[0];
                                g = "0x" + H[1] + H[1];
                                b = "0x" + H[2] + H[2];
                            } else if (H.length == 6) {
                                r = "0x" + H[0] + H[1];
                                g = "0x" + H[2] + H[3];
                                b = "0x" + H[4] + H[5];
                            } else if (H.length == 8) {
                                r = "0x" + H[0] + H[1];
                                g = "0x" + H[2] + H[3];
                                b = "0x" + H[4] + H[5];
                                o = "0x" + H[6] + H[7];
                            }

                            r /= 255;
                            g /= 255;
                            b /= 255;
                            o /= 255;

                            let cmin = Math.min(r,g,b),
                                cmax = Math.max(r,g,b),
                                delta = cmax - cmin,
                                h = 0,
                                s = 0,
                                l = 0;

                            if (delta == 0)
                                h = 0;
                            else if (cmax == r)
                                h = ((g - b) / delta) % 6;
                            else if (cmax == g)
                                h = (b - r) / delta + 2;
                            else
                                h = (r - g) / delta + 4;

                            h = Math.round(h * 60);

                            if (h < 0)
                                h += 360;

                            l = (cmax + cmin) / 2;
                            s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));

                            this.hue = h;
                            this.saturation = s;
                            this.lightness = l;

                            this.hueBase = Math.round(this.hue / 3.6);
                            this.opacity = Math.round(o * 100);
                            this.base = this.HSLToRGB(this.hue, 1, 0.5, false);

                            const hsv_value = this.saturation * Math.min(this.lightness, 1 - this.lightness) + this.lightness;
                            const hsv_saturation = hsv_value ? 2 - 2 * this.lightness / hsv_value : 0;

                            this.y = Math.round((1 - hsv_value) * 100);
                            this.x = Math.round(hsv_saturation * 100);
                        }
                    }))
                })
            </script>
        @endpush
    @endonce
</div>
