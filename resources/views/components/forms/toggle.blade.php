<div {{ $attributes->class([
    'flex',
    'items-center' => !$description,
]) }} x-data="{ on: {{ $checked ? 'true' : 'false' }} }" @click.stop="on = !on; $dispatch('input', on)" x-modelable="on">
    @if ($name)
        <input type="checkbox" class="hidden" name="{{ $name }}" value="{{ $value }}" id="{{ $id }}" :checked="on">
    @endif
    <button type="button" @class([
        "relative inline-flex flex-shrink-0 cursor-pointer transition-colors ease-in-out focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500 py-1 px-1 rounded-md",
        "h-5 w-9" => $size === 'small',
        "h-6 w-12" => $size === 'normal',
    ]) role="switch" aria-checked="false" x-ref="switch" x-state:on="Enabled" x-state:off="Not Enabled" :class="{ 'bg-gray-200 dark:bg-slate-700': !on, 'bg-primary-600 dark:bg-primary-800': on }" :aria-checked="on.toString()" @keydown.space.prevent="on = !on">
        @if ($label)
            <span class="sr-only">{{ $label }}</span>
        @endif
        <span aria-hidden="true" :class="{ '{{ $size === 'normal' ? 'translate-x-6' : 'translate-x-4' }}': on, 'translate-x-0': !on }" @class([
            "pointer-events-none inline-block h-full bg-white dark:bg-slate-300 transform transition ease-in-out rounded",
            "w-3" => $size === 'small',
            "w-4" => $size === 'normal',
        ])></span>
    </button>
    <div @class([
        'ml-3' => $label || $description,
    ])>
        @if ($label)
            <x-label :for="$name ? $id : null" :required="$required">{{ $label }}</x-label>
        @endif
        @if ($description)
            <p class="text-xs mt-0.5 dark:text-slate-400 text-gray-600">{{ $description }}</p>
        @endif
    </div>
</div>
