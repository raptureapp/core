<x-input x-data="{}" x-init="new Pikaday({ field: $el, format: '{{ $format }}', yearRange: {{ $range }}, onSelect: function() { $el.dispatchEvent(new CustomEvent('input', {
    bubbles: true,
    detail: this.getMoment().format('{{ $format }}'),
})); } })" :name="$name" :value="$value" {{ $attributes }}>
    <x-slot:append>
        <em class='far fa-calendar'></em>
    </x-slot:append>
</x-input>

@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('rapture/pikaday.css') }}">
        <style>
            .pika-lendar .pika-label:after {
                content:" \25be";
            }
        </style>
    @endpush
    @push('components')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>
    @endpush
@endonce
