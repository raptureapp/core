<div {{ $attributes->whereStartsWith(['class', 'x-show', 'x-transition', 'style']) }}>
    @if ($label)
        <x-label for="{{ $id }}" :required="$required" class="mb-1">{{ $label }}</x-label>
    @endif
    <div @class([
        'relative' => !empty($prepend) || !empty($append),
    ])>
        @if ($prepend)
            <div class="pointer-events-none absolute inset-y-0 left-0 pl-3 flex items-center text-gray-500 dark:text-slate-300">
                {{ $prepend }}
            </div>
        @endif
        @if ($append)
            <div class="pointer-events-none absolute inset-y-0 right-0 pr-3 flex items-center text-gray-500 dark:text-slate-300">
                {{ $append }}
            </div>
        @endif
        <input @if ($name) name="{{ $name }}" @endif id="{{ $id }}" {{ $attributes->whereDoesntStartWith(['class', 'x-show', 'x-transition', 'style'])
        ->merge([
            'type' => 'text',
            'autocomplete' => 'off',
            'x-data' => !is_null($mask),
            'x-mask' => !is_null($mask) ? $mask : false,
        ])
        ->class([
            'py-2 border placeholder-gray-500 dark:placeholder-slate-400 focus:outline-none focus:ring-primary-600 sm:text-sm leading-5 dark:bg-slate-900 dark:focus:ring-primary-700 w-full text-black-500 dark:text-white',
            'pl-10' => !empty($prepend) && strlen($prepend) != 1,
            'pl-8' => !empty($prepend) && strlen($prepend) === 1,
            'pl-3' => empty($prepend),
            'pr-10' => !empty($append) && strlen($append) != 1,
            'pr-8' => !empty($append) && strlen($append) === 1,
            'pr-3' => empty($append),
            $shape => $shape !== 'square' && $shape !== 'rounded',
            'rounded-lg' => $shape === 'rounded',
            'border-red-400 dark:border-red-600 focus:border-red-400 dark:focus:border-red-600' => $error,
            'border-black-50 dark:border-slate-700 focus:border-primary-500 dark:focus:border-primary-600' => !$error,
            $inputClass,
        ]) }} value="{{ $fill ? old($name, $value) : $value }}" />
    </div>

    @if (!is_null($mask))
        @pushOnce('components')
        <script defer src="https://cdn.jsdelivr.net/npm/@alpinejs/mask@3.x.x/dist/cdn.min.js"></script>
        @endPushOnce
    @endif
</div>
