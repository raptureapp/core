<div {{ $attributes->whereStartsWith(['class', 'x-show', 'x-transition', 'style']) }}>
    @if ($label)
        <x-label for="{{ $id }}" :required="$required" class="mb-1">{{ $label }}</x-label>
    @endif
    <textarea @if ($name) name="{{ $name }}" @endif id="{{ $id }}" {{ $attributes->whereDoesntStartWith(['class', 'x-show', 'x-transition', 'style'])->class([
        'block py-2 border placeholder-gray-500 dark:placeholder-slate-400 focus:outline-none focus:ring-primary-600 sm:text-sm leading-5 dark:bg-slate-900 dark:text-white dark:focus:ring-primary-700 w-full text-black-500 dark:text-white',
        $shape => $shape !== 'square' && $shape !== 'rounded',
        'rounded-lg' => $shape === 'rounded',
        'border-red-400 dark:border-red-600 focus:border-red-400 dark:focus:border-red-600' => $error,
        'border-black-50 dark:border-slate-700 focus:border-primary-500 dark:focus:border-primary-600' => !$error,
        $inputClass,
    ]) }}>{{ old($name, $value ?? $slot) }}</textarea>
</div>
