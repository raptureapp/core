<form method="{{ $method }}" {{ $attributes }}>
    @csrf
    @if ($alt)
        @method($alt)
    @endif

    {{ $slot }}
</form>
