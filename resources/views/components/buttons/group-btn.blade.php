<button {{ $attributes->merge(['type' => 'button'])->class([
        'relative border focus:z-10 focus:outline-none focus:ring-1 focus:ring-primary-500 focus:border-primary-500 dark:focus:ring-primary-700 dark:focus:border-primary-600 transition-all ease-in-out first:rounded-l-lg last:rounded-r-lg leading-5',
        'py-1 px-2 text-xs space-x-2' => $size === 'xsmall',
        'py-1.5 px-3 text-sm space-x-3' => $size === 'small',
        'py-2 px-4 text-base space-x-4' => $size === 'normal',
        'py-2.5 px-5 text-lg space-x-5' => $size === 'large',
        'bg-primary-700 dark:bg-primary-800 border-primary-900 dark:border-primary-600 text-white' => $active,
        'bg-white dark:bg-slate-900 border-black-50 dark:border-slate-700 text-gray-700 dark:text-white hover:bg-primary-100 dark:hover:bg-primary-900 hover:text-primary-900 dark:hover:text-primary-100 hover:border-primary-700 dark:hover:border-primary-600 hover:z-10' => (is_null($active) && is_null($xActive)) || !$active,
    ]) }} @if (!is_null($xActive)) :class="{ 'bg-primary-700 dark:bg-primary-800 border-primary-900 dark:border-primary-600 text-white': {{ $xActive }}, 'bg-white dark:bg-slate-900 border-black-50 dark:border-slate-700 text-gray-700 dark:text-white hover:bg-primary-100 dark:hover:bg-primary-900 hover:text-primary-900 dark:hover:text-primary-100 hover:border-primary-700 dark:hover:border-primary-600 hover:z-10': !({{ $xActive }}) }" @endif>
    <span class="font-medium leading-5 inline-flex items-center space-x-3">
        {{ $slot }}
    </span>
</button>
