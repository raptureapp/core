<{!! !is_null($href) ? 'a href="' . $href . '"' : 'button type="' . $type . '"' !!} {{ $attributes->class([
    'border font-medium focus:outline-none focus:border-gray-700 focus:ring-gray transition-all ease-in-out',
    'inline-block text-center' => is_null($icon) && !is_null($href) && !$loading,
    'inline-flex items-center' => !is_null($icon) || $loading,
    'py-1 px-2 text-xs space-x-2 rounded-md' => $size === 'xsmall',
    'py-2 px-3 text-sm space-x-3 rounded-lg' => $size === 'small',
    'py-2 px-4 text-base space-x-4 rounded-lg' => $size === 'normal',
    'py-2.5 px-5 text-lg space-x-5 rounded-lg' => $size === 'large',
    'border-transparent text-white bg-zinc-500 dark:bg-slate-700 hover:bg-zinc-700 dark:hover:bg-slate-600' => $color === 'default',
    'border-black-50 dark:border-slate-700 text-gray-700 dark:text-white hover:bg-primary-100 dark:hover:bg-primary-900 hover:text-primary-900 dark:hover:text-primary-100 hover:border-primary-700 dark:hover:border-primary-600' => $color === 'outline',
    'border-transparent text-white bg-primary-700 hover:bg-primary-900 dark:bg-primary-800 dark:hover:bg-primary-900' => $color === 'primary',
    'bg-white dark:bg-slate-900 border-black-50 dark:border-slate-700 text-gray-700 dark:text-white hover:bg-primary-100 dark:hover:bg-primary-900 hover:text-primary-900 dark:hover:text-primary-100 hover:border-primary-700 dark:hover:border-primary-600' => $color === 'filled',
]) }}>
    @if ($loading)
        <em class="{{ $library }} fa-spinner fa-pulse" aria-hidden="true"></em>
    @elseif (!is_null($icon) && $iconPosition === 'left')
        <em class="{{ $library }} fa-{{ $icon }}" aria-hidden="true"></em>
    @endif
    @if (!is_null($label))
        <span>{{ $label }}</span>
    @endif
    {{ $slot }}
    @if (!$loading && !is_null($icon) && $iconPosition === 'right')
        <em class="far fa-{{ $icon }}" aria-hidden="true"></em>
    @endif
</{{ !is_null($href) ? 'a' : 'button' }}>

