<div {{ $attributes->class('relative inline-flex rounded-lg -space-x-[1px]') }}>
    {{ $slot }}
</div>
