<x-dashboard title="Branding">
    <x-heading label="Branding" />

    <livewire:branding-controls />
</x-dashboard>
