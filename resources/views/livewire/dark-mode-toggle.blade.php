<div x-data="{ theme: @entangle('theme').live, setTheme(theme) { this.theme = theme; document.documentElement.classList.remove('dark', 'light', 'auto'); document.documentElement.classList.add(theme); } }">
    <x-button-group class="w-full">
        <x-btn class="flex-1" @click="setTheme('dark')" x-active="theme === 'dark'" title="Dark Mode">
            <em class="far fa-moon"></em>
        </x-btn>
        <x-btn class="flex-1" @click="setTheme('light')" x-active="theme === 'light'" title="Light Mode">
            <em class="far fa-sun"></em>
        </x-btn>
        <x-btn class="flex-1" @click="setTheme('auto')" x-active="theme === 'auto'" title="System Preference">
            <em class="far fa-eclipse"></em>
        </x-btn>
    </x-button-group>
</div>
