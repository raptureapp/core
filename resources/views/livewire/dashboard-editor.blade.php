<x-panel size="small">
    <x-slot:heading class="flex items-center justify-between">
        <p>{{ $name }}</p>

        <div class="flex items-center space-x-2">
            @if (!$isDefault)
                <x-button size="small" outline wire:click="$parent.setDefault('{{ $dashboard }}')" @click.stop="">Set as Default</x-button>
            @endif
            <x-button size="small" outline wire:click="$parent.openWidgetSelector('{{ $dashboard }}')" @click.stop="">Customize</x-button>
            <x-button size="small" outline>Rename</x-button>
            @if (!$isDefault)
                <x-button size="small" outline wire:click="$parent.deleteDashboard('{{ $dashboard }}')" @click.stop="" wire:confirm="Are you sure you want to delete this dashboard?">Delete</x-button>
            @endif
        </div>
    </x-slot:heading>

    <form wire:submit="update">
        <x-input label="Name" wire:model="name" />

        <x-button color="primary" type="submit" size="small" class="mt-6">Update</x-button>
    </form>
</x-panel>
