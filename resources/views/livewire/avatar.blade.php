<div x-data="{ photoName: null, photoPreview: null }" class="mt-6 flex items-center">
    <input type="hidden" name="avatar" value="{{ old('avatar', $avatar) }}">
    <input type="hidden" name="newPhoto" value="{{ old('newPhoto', $photo ? $photo->getRealPath() : '') }}">

    <input type="file" class="hidden" wire:model.live="photo" x-ref="photo" x-on:change="
        photoName = $refs.photo.files[0].name;
        const reader = new FileReader();
        reader.onload = (e) => {
            photoPreview = e.target.result;
        };
        reader.readAsDataURL($refs.photo.files[0]);" />

    <div x-show="! photoPreview">
        <span class="block rounded-full w-20 h-20 bg-cover bg-no-repeat bg-center" style="background-image: url('{{ $preview ?? $fallback }}');">
        </span>
    </div>

    <div x-show="photoPreview" style="display: none;">
        <span class="block rounded-full w-20 h-20 bg-cover bg-no-repeat bg-center" :style="'background-image: url(\'' + photoPreview + '\');'">
        </span>
    </div>

    <div class="flex items-center space-x-2 ml-4">
        <x-button size="small" @click.prevent="$refs.photo.click()" outline>
            Select Photo
        </x-button>

        @if ($avatar || $photo)
            <x-button wire:click="deletePhoto" @click.prevent="photoPreview = null" size="small" outline>
                Remove Photo
            </x-button>
        @endif
    </div>
</div>
