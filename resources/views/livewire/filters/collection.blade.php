<x-select wire:model="pendingFilters.{{ $filter->key }}" :label="$filter->label">
    <option value=""></option>
    @foreach ($filter->getFilterOptions() as $key => $option)
    <option value="{{ $key }}">{{ $option }}</option>
    @endforeach
</x-select>
