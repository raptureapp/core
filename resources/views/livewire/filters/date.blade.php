<div class="sm:flex space-y-4 sm:space-y-0 sm:space-x-6">
    <x-datepicker wire:model="pendingFilters.{{ $filter->key }}.after" label="{{ $filter->label }} After" class="flex-1" />
    <x-datepicker wire:model="pendingFilters.{{ $filter->key }}.before" label="{{ $filter->label }} Before" class="flex-1" />
</div>
