<x-container class="space-y-8">
    <x-form-section title="Theme" desc="Change the colour of your dashboard">
        <div class="grid grid-cols-5 gap-2">
            @foreach ($colors as $key => $label)
                <x-button :outline="$color !== $key" class="inline-flex items-center" size="small" wire:click="setColor('{{ $key }}')">
                    <div class="w-5 h-5 my-1 bg-{{ $key }}-500 rounded-md"></div>
                    <span>{{ $label }}</span>
                </x-button>
            @endforeach
        </div>
    </x-form-section>

    <x-form-section title="Logo" desc="Upload your logo">
        <div class="flex items-start space-x-6">
            <div class="flex-1">
                <livewire:file-upload id="brand-logo-light" label="Light Logo" single display="preview" wire:model.live="lightLogo" public folder="logos" />
            </div>
            <div class="flex-1">
                <livewire:file-upload id="brand-logo-dark" label="Dark Logo" single display="preview" wire:model.live="darkLogo" public folder="logos" />
            </div>
        </div>
    </x-form-section>

    <x-form-section title="Dark Mode" desc="Choose if you want to use dark mode">
        <x-label>Starting Mode</x-label>
        <x-button-group class="mt-1">
            <x-btn :active="$theme === 'auto'" wire:click="setTheme('auto')">
                <em class="far fa-eclipse"></em>
                <span class="text-sm">User Preference</span>
            </x-btn>
            <x-btn :active="$theme === 'on'" wire:click="setTheme('on')">
                <em class="far fa-moon"></em>
                <span class="text-sm">Start on Dark</span>
            </x-btn>
            <x-btn :active="$theme === 'off'" wire:click="setTheme('off')">
                <em class="far fa-sun"></em>
                <span class="text-sm">Start on Light</span>
            </x-btn>
        </x-button-group>

        <x-toggle label="Allow the user to pick their own mode" class="mt-6" wire:model.live="allowUserSelection" />
    </x-form-section>
</x-container>
