<div x-data="{ isExpanded: {{ $menu->namespaceWithinRequest() ? 'true' : 'false' }} }">
    <a href="{{ $menu->url() }}" class="group flex items-center px-3 py-1.5 leading-5 ease-in-out transition focus:outline-none text-sm rounded-md {{ $menu->namespaceWithinRequest() ? 'text-primary-900 bg-primary-700/10 dark:text-primary-100 dark:bg-primary-900/30' : 'text-gray-900 hover:text-white hover:bg-primary-700 dark:hover:bg-slate-800 dark:text-slate-300 dark:hover:text-white' }}">
        @if ($menu->icon)
            <em aria-hidden="true" class="far fa-{{ $menu->icon }} text-base mr-2 w-6 text-center transition {{ $menu->namespaceWithinRequest() ? 'text-primary-900 dark:text-slate-300' : 'text-gray-500 group-hover:text-white dark:text-slate-500 dark:group-hover:text-slate-300' }}"></em>
        @endif
        <span class="flex-1">@lang($menu->label)</span>
        @if ($children->has($menu->id))
            <em class="far fa-angle-right w-6 h-7 flex items-center justify-center transform" @click.prevent="isExpanded = !isExpanded" :class="{ 'rotate-90 text-inherit': isExpanded }"></em>
        @endif
    </a>
    @if ($children->has($menu->id))
        <div x-show="isExpanded" style="display: none;" x-collapse>
            <div class="text-sm pt-4 pb-3 relative">
                <div class="border-l border-black-50 dark:border-slate-700 absolute top-4 bottom-3 left-0 ml-6 transform -translate-x-full"></div>
                <div class="space-y-4 lg:space-y-3">
                    @foreach ($children[$menu->id] as $child)
                        <a href="{{ $child->url() }}" class="group flex items-center pl-5 ml-6 border-l pr-3 ease-in-out transition focus:outline-none relative transform -translate-x-px {{ $child->namespaceWithinRequest() ? 'border-primary-700 dark:border-slate-400 text-primary-900 dark:text-white' : 'border-transparent text-gray-700 hover:text-gray-900 dark:text-slate-300 hover:dark:text-white hover:border-gray-900' }}">@lang($child->label)</a>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
</div>
