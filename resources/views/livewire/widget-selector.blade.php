<div x-data="editor">
    <x-button size="large" @click="modal = true" outline>Customize</x-button>

    <x-modal title="Customize Dashboard" size="large" align="top">
        @if (empty($packages))
            <div class="flex justify-center items-center z-10 bg-white dark:bg-slate-900 text-xl opacity-75 dark:text-white py-8">
                <em class="far fa-spinner fa-pulse mr-3"></em>
                <p>Loading</p>
            </div>
        @else
            <x-tabs class="mx-4 sm:mx-6 mt-2" size="small">
                <x-tab label="Widgets" icon="grid-2-plus" size="small" />
                <x-tab label="My Dashboards" icon="gauge" size="small" :badge="count($dashboards)" />

                {{-- <div class="flex-1 flex items-end justify-end space-x-6">
                    <x-tab label="Sharing" icon="share-nodes" size="small" />
                    <x-tab label="Discover" icon="binoculars" size="small" />
                </div> --}}
            </x-tabs>

            <div class="flex" x-show="tab === 'widgets'">
                <div class="flex-1 p-4 sm:p-6 min-h-96">
                    <x-h3>Your Dashboard</x-h3>
                    <div class="grid-stack min-h-full mt-2" x-ref="container">
                        @foreach ($selection as $key => $details)
                        <div class="grid-stack-item" gs-id="{{ $key }}" @foreach ($details['sizes'] as $type => $value) gs-{{ $type}}="{{ $value }}" @endforeach>
                            <div class="grid-stack-item-content">
                                <div class="absolute text-gray-700 dark:text-slate-400 top-0 right-0 flex items-center p-2 text-sm">
                                    <button type="button" wire:click="remove('{{ $key }}')"><em class="far fa-trash"></em></button>
                                </div>
                                <div class="absolute text-gray-700 dark:text-slate-400 bottom-0 right-0 pb-1 pr-1">
                                    @if (!array_key_exists('no-resize', $details['sizes']))
                                        <svg fill="none" viewBox="0 0 24 24" width="16" height="16" xmlns="http://www.w3.org/2000/svg"><path d="m21 15-6 6m6-13-13 13" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></svg>
                                    @endif
                                </div>
                                <div class="rounded-md bg-white dark:bg-slate-800 border border-black-50 dark:border-slate-600 h-full flex items-center justify-center font-medium cursor-move px-3 py-2 text-center">
                                    {{ $details['label'] }}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="w-1/3 p-4 sm:p-6 border-l border-black-50 dark:border-slate-700">
                    <x-h3>Widgets Available</x-h3>
                    <div class="space-y-1 mt-2">
                        @foreach ($packages as $package)
                            <div x-data="{ expanded: false }">
                                <button type="button" class="group flex items-center justify-between px-3 py-2 ease-in-out transition focus:outline-none text-sm font-medium bg-white dark:bg-slate-900 text-gray-900 dark:text-slate-100 w-full border border-black-50 dark:border-slate-700" @click="expanded = !expanded" :class="{ 'rounded-t-lg': expanded, 'rounded-lg': !expanded }">
                                    <span class="text-left">{{ $package->fullName }}</span>
                                    <x-badge color="green" size="small" class="min-w-6 justify-center">{{ count($package->widgets) }}</x-badge>
                                </button>

                                <div x-show="expanded" style="display: none;" class="divide-y divide-black-50 dark:divide-slate-700 bg-white dark:bg-slate-900 rounded-b-lg px-3 border-x border-b border-black-50 dark:border-slate-700">
                                    @foreach ($package->widgets as $key => $widget)
                                        <button type="button" class="py-3 text-sm w-full text-left" wire:click="add('{{ $key }}')">
                                            <p class="font-medium">{{ $widget->label() }}</p>
                                            @if (!empty($widget->purpose()))
                                                <p class="mt-1 text-sm text-gray-600 dark:text-slate-400">{{ $widget->purpose() }}</p>
                                            @endif
                                        </button>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="p-4 sm:p-6" x-show="tab === 'my-dashboards'" style="display: none;">
                @if (count($dashboards) > 0)
                    <div class="space-y-2 mb-4">
                        @foreach ($dashboards as $dashboard)
                            <livewire:dashboard-editor :dashboard="$dashboard->id" :name="$dashboard->name" :isDefault="$dashboard->is_default" :key="'dashboard-' . $dashboard->id" />
                        @endforeach
                    </div>
                @endif

                <x-button wire:click="createDashboard" icon="plus" label="New Dashboard" size="small" />
            </div>

            <div class="p-4 sm:p-6" x-show="tab === 'sharing'" style="display: none;">
                <x-toggle label="Share this dashboard" />

                <x-toggle label="Set as the default for everyone" class="mt-4" description="This will become the default for anyone who hasn't modified their dashboard" />
                {{-- <x-toggle label="Restrict to User Group" class="mt-4" description="Only users from this group can discover this dashboard" /> --}}
                <x-toggle label="Lock dashboard" class="mt-4" description="Enabling will stop this dashboard from being overwritten by others" />
            </div>

            <div class="p-4 sm:p-6" x-show="tab === 'discover'" style="display: none;">
            </div>

            <x-slot:footer class="flex items-center justify-end" x-show="tab === 'widgets'">
                <x-button color="primary" @click="updateDashboard">Update Dashboard</x-button>
            </x-slot:footer>
        @endif
    </x-modal>

    <script src="{{ asset('rapture/gridstack.js') }}"></script>
    <link href="{{ asset('rapture/gridstack.min.css') }}" rel="stylesheet"/>
    <style type="text/css">
        .grid-stack {
            border-right: 1px solid #d3d6dc;
            border-bottom: 1px solid #d3d6dc;
            background-size: 33.3333% 120px;
            background-image:
                linear-gradient(to right, #d3d6dc 1px, transparent 1px),
                linear-gradient(to bottom, #d3d6dc 1px, transparent 1px);
        }

        @media (prefers-color-scheme: dark) {
            html:not(.light) .grid-stack {
                border-right: 1px solid #334155;
                border-bottom: 1px solid #334155;
                background-image:
                    linear-gradient(to right, #334155 1px, transparent 1px),
                    linear-gradient(to bottom, #334155 1px, transparent 1px);
            }
        }

        html.dark .grid-stack {
            border-right: 1px solid #334155;
            border-bottom: 1px solid #334155;
            background-image:
                linear-gradient(to right, #334155 1px, transparent 1px),
                linear-gradient(to bottom, #334155 1px, transparent 1px);
        }

        .gs-3 > .grid-stack-item {
            width: 33.333%;
        }
        .gs-3 > .grid-stack-item[gs-x="1"] {
            left: 33.333%;
        }
        .gs-3 > .grid-stack-item[gs-w="2"] {
            width: 66.667%;
        }
        .gs-3 > .grid-stack-item[gs-x="2"] {
            left: 66.667%;
        }
        .gs-3 > .grid-stack-item[gs-w="3"] {
            width: 100%;
        }
        .grid-stack-item > .ui-resizable-se {
            transform: rotate(0);
            background: none;
        }
    </style>
    <script type="text/javascript">
        let grid = null;

        document.addEventListener('alpine:init', () => {
            Alpine.data('editor', () => ({
                modal: @entangle('open').live,
                tab: @entangle('tab'),
                init() {
                    if (this.modal) {
                        this.setupGrid();
                    }

                    this.$watch('modal', () => {
                        if (this.modal) {
                            return;
                        }

                        this.destroyGrid(true);
                    });

                    this.$watch('tab', () => {
                        if (this.tab === 'widgets') {
                            this.destroyGrid();

                            setTimeout(() => {
                                this.setupGrid();
                            }, 5);
                        }
                    });

                    Livewire.on('refreshGrid', () => {
                        this.destroyGrid();

                        setTimeout(() => {
                            this.setupGrid();
                        }, 5);
                    });

                    Livewire.hook('morph.updated', ({ el, component }) => {
                        if (this.modal && this.tab === 'widgets') {
                            this.destroyGrid();

                            setTimeout(() => {
                                this.setupGrid();
                            }, 5);
                        }
                    });
                },
                destroyGrid(force = false) {
                    if (grid === null) {
                        return;
                    }

                    grid.destroy(force);
                    grid = null;
                },
                setupGrid() {
                    if (grid || !this.modal) {
                        return;
                    }

                    grid = GridStack.init({
                        column: 3,
                        cellHeight: 120,
                        margin: '0.5rem',
                        alwaysShowResizeHandle: true,
                    }, this.$refs.container);

                    grid.on('resizestop', (event, ui) => {
                        this.$wire.storeSelection(grid.save(false));
                    });
                },
                updateDashboard() {
                    this.$wire.store(grid.save(false));
                },
            }))
        })
    </script>
</div>
