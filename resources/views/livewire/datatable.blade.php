<x-container x-data="datatable()" @keyup.window.escape="clearSelection">
    @if (!empty($title))
        <x-h2 class="mb-4">{{ $title }}</x-h2>
    @endif

    <div class="rounded-lg relative border border-black-50 dark:border-slate-700 bg-white dark:bg-slate-900">
        <div class="sm:flex sm:justify-between sm:items-center border-b border-black-50 dark:border-slate-700 py-4 px-5">
            <div>
                @if ($this->scopes->count() > 0)
                    <div class="xl:hidden mb-2 sm:mb-0">
                        <label for="tabs" class="sr-only">Select a scope</label>
                        <x-select id="tabs" name="tabs" wire:model.live="scope">
                            @foreach ($this->scopes as $item)
                                <option value="{{ $item->key }}">{{ $item->label }} ({{ $item->count }})</option>
                            @endforeach
                        </x-select>
                    </div>
                    <div class="hidden xl:block">
                        <nav class="flex space-x-1" aria-label="Tabs">
                            @foreach ($this->scopes as $item)
                                <button type="button" class="group inline-flex items-center py-2 px-4 font-medium text-sm transition rounded-lg {{ $scope === $item->key ? 'text-primary-900 bg-primary-700/10 dark:text-primary-100 dark:bg-primary-900/30' : 'bg-transparent hover:bg-gray-50 dark:hover:bg-slate-800 text-gray-900 dark:text-slate-300' }}" wire:click="applyScope('{{ $item->key }}')">
                                    {!! $item->display() !!} ({{ $item->count }})
                                </button>
                            @endforeach
                        </nav>
                    </div>
                @endif
            </div>
            <div class="sm:flex sm:items-center">
                @if ($searchable)
                    <x-form method="get" class="sm:mr-4 mb-2 sm:mb-0 sm:w-52" wire:submit.prevent="applySearch">
                        <x-input id="search" placeholder="Search" type="search" name="search" wire:model="searchTerm">
                            <x-slot:prepend>
                                <em class="far fa-search"></em>
                            </x-slot:prepend>
                        </x-input>
                    </x-form>
                @endif

                <div class="flex items-center">
                    @isset($exportable)
                        <div x-data="{ exportModal: @entangle('exportOpen').live }">
                            <x-button-group class="mr-4">
                                <x-btn title="Export" @click="exportModal = true">
                                    <em class="far fa-download"></em>
                                </x-btn>
                            </x-button-group>

                            <x-modal title="Build an Export" trigger="exportModal">
                                <div class="p-4 sm:p-6">
                                    <div class="flex justify-between items-center mb-2">
                                        <x-h3>Select Fields</x-h3>
                                        <x-button-group>
                                            <x-btn wire:click="selectAllColumnsForExport" size="xsmall">Select All</x-btn>
                                            <x-btn wire:click="deselectAllColumnsForExport" size="xsmall">Deselect All</x-btn>
                                        </x-button-group>
                                    </div>

                                    @foreach($this->exportableColumns as $column)
                                        <div class="py-1.5 text-gray-700 dark:text-gray-100 flex items-center space-x-4">
                                            <x-checkbox wire:model.live="selectedForExport" :value="$column->key" />
                                            <span>
                                                {{ $column->label }}
                                            </span>
                                        </div>
                                    @endforeach
                                </div>
                                <x-slot:footer class="flex items-center justify-between">
                                    <div class="text-sm">
                                        {{ count($selectedForExport) }} Column{{ count($selectedForExport) != 1 ? 's' : '' }}
                                    </div>
                                    <x-button wire:click="export" color="primary">Export</x-button>
                                </x-slot:footer>
                            </x-modal>
                        </div>
                    @endisset

                    <div class="relative" x-data="{ open: false }">
                        <x-button-group>
                            @if ($this->filters->count() > 0)
                            <x-btn class="relative" @click="filterModal = true" title="Filter">
                                <em class="far fa-filter"></em>
                                @if ($showLabels)
                                <span class="text-sm">Filter</span>
                                @endif
                                @if ($this->hasActiveFilter())
                                <span class="absolute top-0 left-0 block h-2 w-2 transform -translate-y-1/2 -translate-x-1/2 rounded-full ring-2 ring-white dark:ring-gray-700 bg-red-400 dark:bg-red-600 z-10"></span>
                                @endif
                            </x-btn>
                            @endif

                            @if ($authorized)
                            <x-btn @click="columnModal = true" title="Columns">
                                <em class="far fa-columns"></em>
                                @if ($showLabels)
                                <span class="text-sm">Columns</span>
                                @endif
                            </x-btn>

                            <x-btn class="relative" aria-haspopup="true" :aria-expanded="open" aria-expanded="false" @click="open = !open" title="Customize">
                                <em class="far fa-wrench"></em>
                                @if ($showLabels)
                                <span class="text-sm">Customize</span>
                                @endif
                            </x-btn>
                            @endif
                        </x-button-group>

                        <x-dropdown class="z-20 w-60">
                            <div class="p-2">
                                <x-label class="mb-1">Records per page</x-label>
                                <x-button-group>
                                    @foreach ($this->recordsPerPage as $option)
                                    <x-btn :active="$perPage === $option" wire:click="setPageSize({{ $option }})" class="text-sm">
                                        {{ $option }}
                                    </x-btn>
                                    @endforeach
                                    <x-input type="number" step="5" min="5" placeholder="Custom" wire:model.live="customPageSize" shape="rounded-r-lg" />
                                </x-button-group>
                            </div>
                        </x-dropdown>
                    </div>
                </div>
            </div>
        </div>
        <div class="overflow-x-auto">
            <div class="align-middle inline-block min-w-full overflow-hidden rounded">
                <div wire:loading.flex class="justify-center items-center absolute top-0 left-0 w-full h-full z-10 bg-white dark:bg-slate-900 text-xl opacity-75">
                    <em class="far fa-spinner fa-pulse mr-3"></em>
                    <p>Loading</p>
                </div>
                <table class="min-w-full">
                    <thead>
                        <tr class="border-b border-black-50 dark:border-slate-700">
                            @if ($this->operations->count() > 0)
                                <th class="pl-5 py-4 w-9 leading-none relative">
                                    <x-checkbox ::checked="isPageSelected()" />
                                    <button type="button" class="absolute top-0 left-0 w-full h-full" @click.prevent="togglePage()"></button>
                                </th>
                            @endif
                            @foreach ($this->headings as $heading)
                                <th class="whitespace-nowrap">
                                    @if ($heading->isSortable())
                                        <button type="button" wire:click="sortBy('{{ $heading->key }}')" class="w-full px-5 py-4 leading-none font-medium text-black dark:text-white focus:outline-none text-left border-none text-sm">
                                            {{ $heading->label }}
                                            @if ($sort === $heading->key)
                                                <em class="far {{ $direction === 'asc' ? 'fa-sort-down' : 'fa-sort-up' }} ml-1"></em>
                                            @else
                                                <em class="far fa-sort ml-1 opacity-75"></em>
                                            @endif
                                        </button>
                                    @else
                                        <span class="block w-full px-5 py-4 leading-none font-medium text-black dark:text-white focus:outline-none text-left border-none text-sm">{{ $heading->label }}</span>
                                    @endif
                                </th>
                            @endforeach
                            @if ($this->actions->count() > 0)
                                <th class="px-5 py-4"></th>
                            @endif
                        </tr>
                    </thead>
                    <tbody class="divide-y divide-black-50 dark:divide-slate-800">
                        @foreach ($this->results as $data)
                            <tr class="transition hover:bg-gray-50 dark:hover:bg-slate-800" wire:key="row-{{ $data->$primaryKey }}" :class="{ 'bg-primary-100 hover:bg-primary-100 dark:bg-primary-900/70 dark:hover:bg-primary-900/70': selected.includes('{{ $data->$primaryKey }}') }">
                                @if ($this->operations->count() > 0)
                                    <td class="pl-5 whitespace-nowrap">
                                        <x-checkbox value="{{ $data->$primaryKey }}" x-model="selected" />
                                    </td>
                                @endif
                                @foreach($this->headings as $row)
                                    <td class="px-5 py-4 cursor-pointer {{ $row->rowClasses() }}" @if ($this->actions->count() > 0 && !$row->isLinked()) @click="$event.target.closest('tr').querySelector('[data-primary]').click()" @endif>
                                        @if ($row->isLinked())
                                            <a href="{{ $row->linkTarget($data) }}" @if ($row->opensInNewWindow()) target="_blank" @endif class="text-primary-600 underline">
                                                {!! $row->display($data) !!}
                                            </a>
                                        @else
                                            {!! $row->display($data) !!}
                                        @endif
                                    </td>
                                @endforeach
                                @if ($this->actions->count() > 0)
                                    <td class="px-5 py-4 whitespace-nowrap text-right font-medium w-0">
                                        <div class="flex items-center justify-end space-x-3 dark:text-slate-300">
                                            @foreach ($this->actions as $action)
                                                @if($action->allowed($data))
                                                    @if ($action->hasCallback())
                                                        <button type="button" wire:click="{{ $action->callback }}({{ $data->$primaryKey }})" title="{{ $action->label }}" @if ($action->isPrimary()) data-primary @endif @if ($action->confirm) wire:confirm="{{ $action->confirm }}" @endif>{!! $action->display($data) !!}</button>
                                                    @elseif ($action->hasRoute())
                                                        <a href="{{ route($action->route, $data) }}" title="{{ $action->label }}" @if ($action->isPrimary()) data-primary @endif @if ($action->opensInNewWindow()) target="_blank" @endif @if ($action->confirm) wire:confirm="{{ $action->confirm }}" @endif>{!! $action->display($data) !!}</a>
                                                    @else
                                                        {!! $action->display($data) !!}
                                                    @endif
                                                @endif
                                            @endforeach
                                        </div>
                                    </td>
                                @endif
                            </tr>
                        @endforeach

                        @if(count($this->results) <= 0)
                            <tr>
                                <td colspan="{{ count($this->headings) + ($this->operations->count() > 0 ? 2 : 1) }}" class="text-center px-5 py-6">
                                    No Results
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        @if ($this->results->count() > 0)
            <div class="border-t border-black-50 dark:border-slate-700 px-5">
                {{ $this->results->onEachSide(2)->links() }}
            </div>
        @endif
    </div>

    @if ($this->operations->count() > 0)
        <div class="sticky bottom-8 text-center mt-4 mx-5 bg-white dark:bg-slate-900 border border-black-50 dark:border-slate-700 rounded-lg drop-shadow-md py-3 px-4 flex justify-between items-center" style="display: none;" x-show="selected.length > 0" @foreach ($this->operations as $operation) @if ($operation->keybinding) wire:keydown.window.{{ $operation->keybinding }}="{{ $operation->callback }}" @endif @endforeach>
            <x-button-group>
                @foreach ($this->operations as $operation)
                    @if ($operation->confirm)
                        <x-btn wire:click="{{ $operation->callback }}" title="{{ $operation->label }}" wire:confirm="{{ $operation->confirm }}">
                            {!! $operation->display() !!}
                        </x-btn>
                    @else
                        <x-btn wire:click="{{ $operation->callback }}" title="{{ $operation->label }}">
                            {!! $operation->display() !!}
                        </x-btn>
                    @endif
                @endforeach
            </x-button-group>

            <x-button-group>
                <x-btn title="Clear Selection" @click="selected = []">
                    <em class="far fa-square-check"></em>
                    <span class="text-sm">
                        Clear (<span x-html="selected.length"></span>)
                    </span>
                </x-btn>
            </x-button-group>
        </div>
    @endif

    @if ($this->filters->count() > 0)
        <form method="get" wire:submit.prevent="applyFilters" @submit="filterModal = false">
            <x-modal title="Filter" trigger="filterModal">
                <div class="p-4 sm:p-6">
                    <div class="space-y-4">
                        @foreach ($this->filters as $filter)
                            @include('rapture::livewire.filters.' . $filter->filter)
                        @endforeach
                    </div>
                </div>

                <x-slot:footer class="flex items-center justify-end space-x-4">
                    <x-button color="primary" type="submit">Apply</x-button>
                    <x-button wire:click="resetFilters" @click="filterModal = false" outline>Clear Filters</x-button>
                </x-slot:footer>
            </x-modal>
        </form>
    @endif

    <x-modal title="Column Selection" trigger="columnModal">
        <div class="flex">
            <div class="flex-1 border-r border-black-50 dark:border-slate-700">
                <x-h3 class="pb-2.5 px-4 md:px-6 pt-4">Available</x-h3>
                <div class="max-h-80 overflow-auto px-4 md:px-6 pb-4">
                    @foreach($this->columnSelection as $column)
                    <div class="py-1.5 text-gray-700 dark:text-gray-100 flex items-center justify-between">
                        <span>
                            {{ $column->label }}
                        </span>
                        <x-toggle :checked="$column->isVisible()" wire:click="toggleVisibility('{{ $column->key }}')" size="small" wire:ignore />
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="flex-1">
                <div class="flex items-end justify-between pb-4 pt-4 px-4 md:px-6">
                    <x-h3>Active</x-h3>
                    <p class="text-xs font-medium text-gray-500 dark:text-slate-300">Drag to reorder</p>
                </div>
                <div wire:sortable="updateOrder" class="space-y-1 max-h-80 overflow-auto px-4 md:px-6 pb-6">
                    @foreach($this->activeColumns as $column)
                    <div class="px-4 py-1.5 rounded-lg border border-black-50 dark:border-slate-700 bg-white dark:bg-slate-900 cursor-ns-resize select-none flex items-center space-x-3" wire:sortable.item="{{ $column->key }}" wire:key="column-{{ $column->key }}">
                        <em class="far fa-grip-dots-vertical cursor-ns-resize text-gray-400 dark:text-slate-300"></em>
                        <div>{{ $column->label }}
                            @if ($column->isScoped())
                                <div class="text-xs text-gray-600 dark:text-slate-400">Visible in {{ implode(', ', $column->scoped) }}</div>
                            @endif
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <x-slot:footer class="flex items-center justify-end space-x-4">
            <x-button wire:click="resetColumns" outline>Reset Columns</x-button>
        </x-slot:footer>
    </x-modal>

    @resource('sortable')

    @push('scripts')
    <script>
        function datatable() {
            return {
                selected: @entangle('selected'),
                page: @entangle('currentPage').live,
                filterModal: false,
                columnModal: false,
                togglePage() {
                    const currentPage = this.currentPage();

                    if (this.isPageSelected()) {
                        this.selected = this.currentSelection().filter(number => !currentPage.includes(number));
                    } else {
                        const selection = this.currentSelection();
                        const missing = currentPage.filter(item => !selection.includes(item));
                        const newgroup = selection.concat(missing).filter((value, index, self) => self.indexOf(value) === index);
                        this.selected = newgroup;
                    }
                },
                isPageSelected() {
                    const currentSelection = this.currentSelection();

                    if (currentSelection.length === 0) {
                        return false;
                    }

                    const currentPage = this.currentPage();
                    const selection = currentPage.filter(number => currentSelection.includes(number));
                    return selection.length === currentPage.length;
                },
                currentSelection() {
                    return Object.values(Object.assign({}, this.selected)).map(item => item.toString());
                },
                currentPage() {
                    return Object.values(Object.assign({}, this.page)).map(item => item.toString());
                },
                clearSelection() {
                    this.selected = [];
                },
            };
        }
    </script>
    @endpush
</x-container>
