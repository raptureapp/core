<div class="flex-shrink-0 bg-white dark:bg-slate-900 lg:flex lg:flex-col lg:w-72 border-r border-black-50 dark:border-slate-700">
    <div class="lg:hidden fixed inset-0 flex z-40" x-show="sidebarOpen" style="display: none;">
        <div @click="sidebarOpen = false" x-show="sidebarOpen" x-transition:enter="transition-opacity ease-linear" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition-opacity ease-linear" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" class="fixed inset-0" aria-hidden="true">
            <div class="absolute inset-0 bg-black-500/20 dark:bg-black-700/70"></div>
        </div>

        <div x-show="sidebarOpen" x-transition:enter="transition ease-in-out transform" x-transition:enter-start="-translate-x-full" x-transition:enter-end="translate-x-0" x-transition:leave="transition ease-in-out transform" x-transition:leave-start="translate-x-0" x-transition:leave-end="-translate-x-full" class="relative flex-1 flex flex-col max-w-xs w-full bg-white dark:bg-slate-900">
            <div class="absolute top-0 right-0 -mr-14 p-1">
                <button x-show="sidebarOpen" @click="sidebarOpen = false" class="flex items-center justify-center h-12 w-12 rounded-full focus:outline-none focus:bg-gray-600">
                    <span class="sr-only">Close sidebar</span>
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
            <div class="flex items-center flex-shrink-0 py-4 px-8 min-h-16">
                <a href="{{ route('dashboard') }}" class="uppercase text-xl">
                    @if ($darkLogo)
                        <img src="{{ $darkLogo }}" alt="{{ config('app.name', 'Rapture') }}" class="hidden dark:block">
                    @else
                        <div class="hidden dark:block">
                            @filter('dashboard.logo', config('app.name', 'Rapture'))
                        </div>
                    @endif

                    @if ($logo)
                        <img src="{{ $logo }}" alt="{{ config('app.name', 'Rapture') }}" class="dark:hidden">
                    @else
                        <div class="dark:hidden">
                            @filter('dashboard.logo', config('app.name', 'Rapture'))
                        </div>
                    @endif
                </a>
            </div>
            <div class="flex-1 overflow-y-auto">
                <nav class="py-4 px-3 space-y-1">
                    @include($itemView)
                </nav>
            </div>
        </div>
        <div class="flex-shrink-0 w-14" aria-hidden="true"></div>
    </div>

    <div class="hidden lg:flex items-center flex-shrink-0 py-4 px-8 min-h-16">
        <a href="{{ route('dashboard') }}" class="uppercase text-xl">
            @if ($darkLogo)
                <img src="{{ $darkLogo }}" alt="{{ config('app.name', 'Rapture') }}" class="hidden dark:block">
            @else
                <div class="hidden dark:block">
                    @filter('dashboard.logo', config('app.name', 'Rapture'))
                </div>
            @endif

            @if ($logo)
                <img src="{{ $logo }}" alt="{{ config('app.name', 'Rapture') }}" class="dark:hidden">
            @else
                <div class="dark:hidden">
                    @filter('dashboard.logo', config('app.name', 'Rapture'))
                </div>
            @endif
        </a>
    </div>
    <div class="hidden lg:block flex-1 overflow-y-auto">
        <nav class="py-4 px-4 space-y-1">
            @include($itemView)
        </nav>
    </div>
</div>
