<x-form-section title="Browser Sessions" desc="Logout your active sessions on other browsers and devices." class="mt-8">
    <div x-data="{ modal: @entangle('modal').live }">
        @if (count($this->sessions) > 0)
            <div class="space-y-6">
                @foreach ($this->sessions as $session)
                    <div class="flex items-center gap-4">
                        <div>
                            @if ($session->agent->isDesktop())
                                <em class="fa fa-desktop text-gray-500 dark:text-slate-400 text-2xl w-7 text-center"></em>
                            @else
                                <em class="fa fa-mobile text-gray-500 dark:text-slate-400 text-2xl w-7 text-center"></em>
                            @endif
                        </div>

                        <div>
                            <p>{{ $session->agent->platform() ? $session->agent->platform() : __('Unknown') }} - {{ $session->agent->browser() ? $session->agent->browser() : __('Unknown') }}</p>

                            <div class="mt-0.5">
                                <div class="text-sm text-gray-500 dark:text-slate-400">
                                    {{ $session->ip_address }},

                                    @if ($session->is_current_device)
                                        <span class="text-green-500 dark:text-green-600 font-semibold">{{ __('This device') }}</span>
                                    @else
                                        {{ __('Last active') }} {{ $session->last_active }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif

        <div class="flex items-center mt-5">
            <x-button @click="modal = true" outline>
                Remove Sessions
            </x-button>
        </div>

        <x-modal title="Confirm Logout">
            <form wire:submit="logoutOtherBrowserSessions" class="p-6" id="session-logout">
                <x-password label="Current Password" wire:model="password" :reveal="false" :generate="false" :error="$errors->has('password')" />
            </form>

            <x-slot:footer class="text-right">
                <x-button type="submit" color="primary" wire:loading.attr="disabled" form="session-logout">
                    Logout
                </x-button>
            </x-slot:footer>
        </x-modal>
    </div>
</x-form-section>
