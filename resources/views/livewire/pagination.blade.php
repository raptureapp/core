<nav class="border-t border-transparent px-4 flex items-center justify-center lg:justify-between flex-wrap sm:px-0">
    @if ($paginator->hasPages())
        <div class="flex">
            @foreach ($elements as $element)
                @if (is_string($element))
                    <div class="-mt-px border-t-2 border-transparent p-4 inline-flex items-center text-sm leading-5 font-medium text-gray-500" aria-disabled="true">{{ $element }}</div>
                @endif

                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <div class="-mt-px border-t-2 border-primary-500 dark:border-primary-600 p-4 inline-flex items-center text-sm leading-5 font-medium text-primary-600 dark:text-primary-500" aria-current="page">{{ $page }}</div>
                        @else
                            <button type="button" class="-mt-px border-t-2 border-transparent p-4 inline-flex items-center text-sm leading-5 font-medium text-gray-500 dark:text-slate-300 hover:text-gray-700 hover:dark:text-slate-100 hover:border-black-50 focus:outline-none focus:text-gray-700 focus:border-gray-400 transition-all ease-in-out" wire:click="gotoPage({{ $page }})">{{ $page }}</button>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </div>
        <div class="hidden flex-1 2xl:flex justify-end">
            @if (!$paginator->onFirstPage())
                <button type="button" class="-mt-px border-t-2 border-transparent p-4 inline-flex items-center text-sm leading-5 font-medium text-gray-500 hover:text-gray-700 hover:border-black-50 focus:outline-none focus:text-gray-700 focus:border-gray-400 transition-all ease-in-out duration-150 dark:text-slate-300 hover:dark:text-slate-100" wire:click="previousPage" rel="prev" aria-label="@lang('pagination.previous')">
                    @lang('pagination.previous')
                </button>
            @endif

            @if ($paginator->hasMorePages())
                <button type="button" class="-mt-px border-t-2 border-transparent p-4 inline-flex items-center text-sm leading-5 font-medium text-gray-500 hover:text-gray-700 hover:border-black-50 focus:outline-none focus:text-gray-700 focus:border-gray-400 transition-all ease-in-out duration-150 dark:text-slate-300 hover:dark:text-slate-100" wire:click="nextPage" rel="next" aria-label="@lang('pagination.next')">
                    @lang('pagination.next')
                </button>
            @endif
        </div>
    @endif
    <div class="w-full text-center lg:text-left lg:flex-1 lg:w-0 lg:order-first">
        <p class="text-sm text-gray-700 dark:text-slate-300 leading-5 pt-2 pb-4 lg:pt-4">
            <span>{!! __('Showing') !!}</span>
            <span class="font-medium">{{ $paginator->firstItem() }}</span>
            <span>{!! __('to') !!}</span>
            <span class="font-medium">{{ $paginator->lastItem() }}</span>
            <span>{!! __('of') !!}</span>
            <span class="font-medium">{{ $paginator->total() }}</span>
            <span>{!! __('results') !!}</span>
        </p>
    </div>
</nav>
