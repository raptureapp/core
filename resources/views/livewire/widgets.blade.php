<div class="grid grid-cols-1 gap-4 lg:grid-cols-2 xl:grid-cols-3 lg:gap-6">
    @hook('dashboard.widgets.important')

    @foreach ($widgets as $key => $position)
        @livewire('widget-' . $key, $position, key('widget-' . $key))
    @endforeach

    @hook('dashboard.widgets')

    @pushOnce('styles')
        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    @endpushOnce
</div>
