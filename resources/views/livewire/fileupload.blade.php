<div>
    <div x-data="fileUpload($wire.id, {{ $single ? 'false' : 'true' }})"
        x-on:livewire-upload-start="isUploading = true"
        x-on:livewire-upload-finish="isUploading = false"
        x-on:livewire-upload-error="isUploading = false"
        x-on:livewire-upload-progress="progress = $event.detail.progress">

        @if ($label)
            <x-label class="mb-1">{{ $label }}</x-label>
        @endif
        @if ($drop)
            <div class="border border-black-50 dark:border-slate-700 rounded-lg mb-2 bg-white dark:bg-slate-900"
                x-on:drop="highlight = false"
                x-on:drop.prevent="processUpload($event)"
                x-on:dragover.prevent="onHover($event)"
                x-on:dragleave.prevent="onExit($event)"
                :class="{ 'ring-1 ring-primary-500': highlight }">
                @if (!empty($files) && $display !== 'none')
                    @if ($display === 'preview')
                        <div @class([
                            'grid gap-4 p-4',
                            'grid-cols-4' => !$single,
                            'grid-cols-1' => $single,
                        ])>
                            @foreach ($files as $index => $uploaded)
                                <div class="rounded-md border border-black-50 dark:border-slate-700 relative" wire:key="attachment-{{ $index }}">
                                    <input type="hidden" name="{{ $name }}[]" value="{{ $uploaded['file'] }}">
                                    @if ($uploaded['type'] === 'image')
                                        <div class="aspect-[4/3] p-4 rounded-t-md bg-black-50 dark:bg-slate-700">
                                            <div class="w-full h-full bg-contain bg-no-repeat bg-center" style="background-image: url({{ array_key_exists($uploaded['file'], $previews) ? $previews[$uploaded['file']] : asset('storage/' . $folder . '/' . $uploaded['file']) }})"></div>
                                        </div>
                                    @else
                                        <div class="aspect-[4/3] rounded mx-4 mt-4 flex items-center justify-center">
                                            <x-ext-type :ext="$uploaded['ext']" class="opacity-75 text-6xl" />
                                        </div>
                                    @endif
                                    <div class="p-4 text-sm">
                                        <p class="font-medium text-black dark:text-white truncate" title="{{ $uploaded['file'] }}">{{ $uploaded['file'] }}</p>

                                        <div class="flex items-center justify-between mt-1">
                                            <p class="text-gray-700 dark:text-slate-300">{{ $uploaded['size'] }}</p>

                                            <button type="button" wire:click="remove('{{ $uploaded['file'] }}')" wire:confirm="Are you sure you want to delete this?">
                                                <em class="far fa-times"></em>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="space-y-1 text-sm p-4">
                            @foreach ($files as $index => $uploaded)
                                <div class="flex items-center justify-between rounded-md border border-black-50 dark:border-slate-700 py-2 px-4" wire:key="attachment-{{ $index }}">
                                    <input type="hidden" name="{{ $name }}[]" value="{{ $uploaded['file'] }}">
                                    <span class="flex items-center space-x-4">
                                        <x-ext-type :ext="$uploaded['ext']" class="opacity-75 text-2xl" />
                                        <div>
                                            <p class="font-medium text-black dark:text-white">{{ $uploaded['file'] }}</p>
                                            <p class="text-gray-700 dark:text-slate-300 mt-0.5">{{ $uploaded['size'] }}</p>
                                        </div>
                                    </span>
                                    <button type="button" wire:click="remove('{{ $uploaded['file'] }}')" wire:confirm="Are you sure you want to delete this?">
                                        <em class="far fa-times"></em>
                                    </button>
                                </div>
                            @endforeach
                        </div>
                    @endif
                @else
                    <div class="py-6 px-4 text-center dark:text-slate-300 text-sm">
                        @if ($icon)
                            <div class="flex items-center justify-center w-10 h-10 rounded-full bg-primary-700/10 dark:bg-primary-900 mx-auto mb-3 text-primary-800 dark:text-primary-200">
                                <em class="far fa-{{ $icon }} text-xl font-normal" aria-hidden="true"></em>
                            </div>
                        @endif
                        <p x-show="!highlight" class="text-gray-700 dark:text-slate-300">{{ $empty }}</p>
                        <p x-show="highlight" class="text-gray-700 dark:text-slate-300" style="display: none;">{{ $hover }}</p>
                        <ul class="text-gray-500 dark:text-slate-400 text-xs mt-1.5 divide-x divide-black-50 dark:divide-slate-700 flex items-center justify-center">
                            <li class="px-4">Max: {{ $maxSizeStr }}</li>
                            @if (!empty($extensions))
                                <li class="px-4">{{ $this->exts }}</li>
                            @endif
                        </ul>
                    </div>
                @endif
            </div>
        @endif

        <input @focus="focused = true" @blur="focused = false" class="sr-only" type="file" id="{{ $id }}" @if ($single) wire:model="upload" @else multiple wire:model="uploads" @endif>

        <div class="flex items-center space-x-4">
            @if ($button)
                <label for="{{ $id }}" :class="{ 'outline-none ring-1 ring-primary-500 border-primary-500': focused }" class="bg-white dark:bg-slate-900 border border-black-50 dark:border-slate-700 rounded-lg focus:outline-none focus:border-gray-700 focus:ring-gray transition leading-5 py-2 px-4 text-gray-700 dark:text-white hover:bg-primary-100 dark:hover:bg-slate-800 hover:text-primary-900 hover:border-primary-700 group cursor-pointer">
                    <em class="far fa-paperclip mr-2 text-gray-500 dark:text-slate-300"></em>
                    <span>{{ $button }}</span>
                </label>
            @endif

            <div x-show="isUploading" class="flex-1" style="display: none;">
                <div class="relative flex items-center space-x-4">
                    <span class="text-sm text-gray-600 dark:text-slate-300 flex-shrink-0">
                        <em class="far fa-spinner fa-pulse mr-1 text-gray-400 dark:text-slate-400"></em>
                        <span>Uploading</span>
                    </span>

                    <div class="h-2 bg-white dark:bg-slate-900 rounded shadow-sm overflow-hidden flex-1">
                        <div class="h-full bg-primary-700 dark:bg-primary-800 rounded" :style="`width: ${progress}%`"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @pushOnce('scripts')
    <script>
        function fileUpload(id, multiple = true) {
            return {
                isUploading: false,
                progress: 0,
                highlight: false,
                timeout: null,
                focused: false,
                multiple,
                processUpload(e) {
                    if (event.dataTransfer.files.length > 0) {
                        const files = e.dataTransfer.files;

                        this.isUploading = true;

                        if (this.multiple) {
                            Livewire.find(id).uploadMultiple('uploads', files,
                                (uploadedFilename) => {
                                    this.isUploading = false;
                                    this.progress = 0;
                                },
                                () => {
                                    this.isUploading = false;
                                },
                                (event) => {
                                    this.progress = event.detail.progress;
                                },
                                () => {
                                    this.isUploading = false;
                                }
                            );
                        } else {
                            Livewire.find(id).upload('upload', files[0],
                                (uploadedFilename) => {
                                    this.isUploading = false;
                                    this.progress = 0;
                                },
                                () => {
                                    this.isUploading = false;
                                },
                                (event) => {
                                    this.progress = event.detail.progress;
                                },
                                () => {
                                    this.isUploading = false;
                                }
                            );
                        }
                    }
                },
                onHover(e) {
                    this.highlight = true;

                    if (this.timeout !== null) {
                        clearTimeout(this.timeout);
                    }
                },
                onExit(e) {
                    this.timeout = setTimeout(() => {
                        this.highlight = false;
                        this.timeout = null;
                    }, 50);
                },
            };
        }
    </script>
    @endpushOnce
</div>
