<x-auth title="Password Confirmation">
    <x-form method="post" action="{{ route('login') }}">
        <x-input type="password" name="password" required :label="__('rapture::login.field_password')" />

        <x-button color="primary" type="submit" class="w-full mt-6">
            Confirm Action
        </x-button>
    </x-form>
</x-auth>
