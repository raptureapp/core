<x-auth :title="__('rapture::resetpassword.title')">
    <x-form method="post" action="{{ route('password.update') }}">
        <input type="hidden" name="token" value="{{ request()->route('token') }}">

        <x-input type="email" name="email" :value="old('email', request()->email)" :label="__('rapture::resetpassword.field_email')" />
        <x-input type="password" name="password" autofocus :label="__('rapture::resetpassword.field_password')" class="mt-4" />
        <x-input type="password" name="password_confirmation" :label="__('rapture::resetpassword.field_password')" class="mt-4" />

        <x-button color="primary" type="submit" class="w-full mt-6">
            @lang('rapture::resetpassword.form_submit')
        </x-button>
    </x-form>
</x-auth>
