<x-auth title="2 Factor Confirmation">
    <x-form method="post" action="{{ route('two-factor.login') }}">
        <x-input type="code" name="code" required label="Code" />

        <x-button color="primary" type="submit" class="w-full mt-6">
            Confirm Action
        </x-button>
    </x-form>
</x-auth>
