<x-auth :title="__('rapture::forgot.title')">
    <x-form method="post" action="{{ route('password.email') }}">
        <x-input type="email" name="email" required autofocus :label="__('rapture::forgot.field_email')" />

        <x-button color="primary" type="submit" class="w-full mt-6">
            @lang('rapture::forgot.form_submit')
        </x-button>
    </x-form>
</x-auth>
