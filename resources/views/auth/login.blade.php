<x-auth :title="__('rapture::login.title')">
    <x-form method="post" action="{{ route('login') }}">
        <x-input type="email" name="email" autofocus :label="__('rapture::login.field_email')" />
        <x-input type="password" name="password" class="mt-4" :label="__('rapture::login.field_password')" />

        <div class="mt-4 flex items-center justify-between">
            <x-checkbox name="remember" :label="__('rapture::login.field_remember')" inline value="1" />

            <div class="text-sm">
                <a href="{{ route('password.request') }}" class="font-medium text-primary-600 hover:text-primary-500 focus:outline-none focus:underline transition dark:text-slate-200 dark:hover:text-slate-300">
                    @lang('rapture::login.forgot_password')
                </a>
            </div>
        </div>

        <x-button color="primary" type="submit" class="w-full mt-6">
            @lang('rapture::login.form_submit')
        </x-button>
    </x-form>
</x-auth>
