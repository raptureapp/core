@if (!empty($actions))
    <div class="border-t border-black-50 dark:border-gray-800 flex items-center justify-end space-x-6 text-sm mt-4 sm:mt-6 pt-4 sm:pt-6">
        @foreach ($actions as $route => $label)
            <a href="{{ $route }}" class="inline-flex items-center space-x-2 transition hover:text-primary-700 dark:hover:text-primary-600">
                <span>{{ $label }}</span>
                <em class="far fa-angle-right"></em>
            </a>
        @endforeach
    </div>
@endif
