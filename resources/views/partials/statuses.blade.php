@if (session('status'))
    <div {{ $attributes->class(['bg-green-700 dark:bg-green-900 mt-6 lg:mt-8']) }}>
        <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8 py-4">
            <div class="flex">
                <div class="flex-shrink-0">
                    <svg class="h-5 w-5 text-white opacity-75" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                    </svg>
                </div>
                <div class="ml-3">
                    <h3 class="text-sm leading-5 font-medium text-white">
                        {{ session('status') }}
                    </h3>
                </div>
            </div>
        </div>
    </div>
@endif

<x-errors class="mt-6 lg:mt-8" />
