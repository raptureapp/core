<x-box @class([
    'col-span-1',
    'lg:col-span-2' => $columns >= 2,
    'xl:col-span-3' => $columns >= 3,
    'row-span-2' => $rows >= 2,
    'row-span-3' => $rows >= 3,
    'row-span-4' => $rows >= 4,
])>
    <x-h2>{{ $label }}</x-h2>
    @if (!empty($description))
        <p class="text-sm mt-1 text-gray-700 dark:text-slate-300">{{ $description }}</p>
    @endif

    <div class="mt-4" x-data='{
        chart: null,
        init() {
            this.chart = new ApexCharts(this.$el, {
                chart: {
                    type: "pie",
                    height: 300,
                },
                series: @json($data),
                labels: @json($labels),
                legend: {
                    markers: {
                        strokeWidth: 0,
                        offsetX: -6,
                    },
                    position: "bottom",
                    itemMargin: {
                        horizontal: 10,
                        vertical: 8,
                    },
                    labels: {
                        colors: @json($theme === 'dark' ? '#d3d6dc' : '#334155'),
                    },
                },
                stroke: {
                    width: 1,
                    colors: @json($theme === 'dark' ? '#334155' : '#d3d6dc'),
                },
            });

            this.chart.render();
        }
    }'></div>

    @include('rapture::partials.widget-actions')
</x-box>
