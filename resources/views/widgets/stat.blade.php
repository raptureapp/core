<x-box @class([
    'col-span-1 flex flex-col',
])>
    <div class="flex justify-between">
        <div>
            <h2 class="text-sm mb-1 text-gray-700 dark:text-slate-300">{{ $label }}</h2>
            <p class="font-medium text-3xl text-black-500 dark:text-white">{{ $value }}</p>
        </div>
        @if (!empty($icon))
            <div class="flex items-center justify-center w-12 h-12 rounded-full bg-primary-700/10 dark:bg-slate-800">
                <em class="far fa-{{ $icon }} text-primary-800 dark:text-primary-600 text-2xl font-normal" aria-hidden="true"></em>
            </div>
        @endif
    </div>
    @if (!is_null($trend))
        <div class="border-t border-black-50 dark:border-gray-800 flex items-center justify-between mt-4 sm:mt-6 pt-3">
            <div @class([
                'flex items-center space-x-3',
                'text-green-500 dark:text-green-400' => $trend > 0,
                'text-red-500 dark:text-red-400' => $trend < 0,
            ])>
                @if ($trend == 0)
                    <em class="far fa-hyphen"></em>
                @elseif ($trend > 0)
                    <em class="far fa-arrow-up-right"></em>
                @else
                    <em class="far fa-arrow-down-right"></em>
                @endif
                <span>{{ $trend }}</span>
            </div>
            <p class="text-sm text-gray-500 dark:text-slate-300">Since last period</p>
        </div>
    @endif
</x-box>
