<x-box @class([
    'col-span-1',
    'lg:col-span-2' => $columns >= 2,
    'xl:col-span-3' => $columns >= 3,
    'row-span-2' => $rows >= 2,
    'row-span-3' => $rows >= 3,
    'row-span-4' => $rows >= 4,
    'hidden' => empty($empty) && count($value) === 0,
    'flex flex-col' => !empty($empty) || count($value) > 0,
])>
    <div class="flex-1">
        <div class="flex justify-between">
            <div>
                <x-h2>{{ $label }}</x-h2>
                @if (!empty($description))
                    <p class="text-sm mt-1 text-gray-700 dark:text-slate-300">{{ $description }}</p>
                @endif
            </div>
            @if (!empty($icon))
                <div class="flex items-center justify-center w-12 h-12 rounded-full bg-primary-700/10 dark:bg-slate-800">
                    <em class="far fa-{{ $icon }} text-primary-800 dark:text-primary-600 text-2xl font-normal" aria-hidden="true"></em>
                </div>
            @endif
        </div>
        <ul @class([
            'mt-4 sm:mt-6 grid gap-x-6 gap-y-4',
            'lg:grid-cols-2' => $columns >= 2,
            'xl:grid-cols-3' => $columns >= 3,
            ])>
            @foreach ($value as $item)
                <li>
                    <div class="flex justify-between space-x-4">
                        <div class="flex items-start space-x-3 flex-1">
                            @if (array_key_exists('icon', $item))
                                <em class="far fa-{{ $item['icon'] }}"></em>
                            @elseif (array_key_exists('image', $item))
                                <img src="{{ asset($item['image']) }}" alt="" class="w-6 h-6 rounded-full">
                            @elseif (array_key_exists('customIcon', $item))
                                {!! $item['customIcon'] !!}
                            @endif
                            <div>
                                @if (array_key_exists('url', $item))
                                    <a href="{{ $item['url'] }}" class="font-medium transition hover:text-primary-700 dark:hover:text-primary-600">{!! $item['label'] !!}</a>
                                @else
                                    <span class="font-medium">{!! $item['label'] !!}</span>
                                @endif
                                @if (array_key_exists('subtext', $item))
                                    <div class="mt-1 text-sm text-gray-500 dark:text-slate-300">
                                        {{ $item['subtext'] }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        @if (array_key_exists('context', $item))
                            <span class="flex-shrink-0">{{ $item['context'] }}</span>
                        @endif
                    </div>
                    @if (array_key_exists('body', $item))
                        <div class="mt-1 text-sm text-gray-700 dark:text-slate-300">
                            {{ $item['body'] }}
                        </div>
                    @endif
                </li>
            @endforeach

            @if (count($value) === 0)
                {{ $empty }}
            @endif
        </ul>
    </div>

    @include('rapture::partials.widget-actions')
</x-box>
