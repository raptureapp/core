<x-dashboard :title="__('rapture::dashboard.title')">
    <x-heading class="flex items-center justify-between">
        <x-h1>Dashboard</x-h1>

        <livewire:widget-selector :slug="$key" />
    </x-heading>

    <x-container>
        <livewire:dashboard-widgets :slug="$key" />
    </x-container>
</x-dashboard>
