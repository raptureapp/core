<x-dashboard :title="__('rapture::account.title')">
    <x-heading no-status :label="__('rapture::account.title')" />

    @if (!in_array(session('status'), ['two-factor-authentication-confirmed', 'two-factor-authentication-enabled', 'two-factor-authentication-disabled', 'recovery-codes-generated']))
        <x-statuses />
    @endif

    <x-container>
        <x-form method="post" action="{{ route('dashboard.account.update') }}" id="account">
            @hook('account.before')

            <x-form-section title="Personal Information" desc="Update your personal details.">
                <div class="flex space-x-6">
                    <x-input name="name" label="First Name" class="flex-1" :value="auth()->user()->name" required />
                    <x-input name="lname" label="Last Name" class="flex-1" :value="auth()->user()->lname" />
                </div>

                <x-input type="email" name="email" class="mt-4" :label="__('rapture::field.email')" required :value="auth()->user()->email" />

                <x-combobox name="timezone" placeholder="- Select a timezone -" :options="$timezones" :value="auth()->user()->timezone" label="Timezone" class="mt-4" />

                <livewire:user-avatar
                    :preview="auth()->user()->avatar()"
                    :avatar="auth()->user()->avatar"
                    :fallback="auth()->user()->defaultAvatar()" />

                @hook('account.personal')
            </x-form-section>

            @hook('account.between')

            <x-form-section title="Change your password" desc="Leave blank to keep your existing password." class="mt-8">
                <x-password :label="__('rapture::field.password')" :generate="false" />

                @hook('account.credentials')
            </x-form-section>

            @hook('account.after')
        </x-form>

        <x-form-section title="Two Factor Authentication" class="mt-8">
            <x-slot name="description">
                <div class="mt-2">
                    @if (auth()->user()->two_factor_confirmed_at)
                        <x-badge color="green">Enabled</x-badge>
                    @else
                        <x-badge color="red">Disabled</x-badge>
                    @endif
                </div>
            </x-slot>

            @if (auth()->user()->two_factor_confirmed_at)
                <p class="text-sm">Two factor authentication confirmed and enabled successfully. In the event your authentication becomes disconnected you can use one of these single use codes to regain entry into your website.</p>

                <ul class="mt-6 space-y-1 text-sm">
                    @foreach (auth()->user()->recoveryCodes() as $code)
                    <li class="font-mono">{{ $code }}</li>
                    @endforeach
                </ul>

                <div class="flex items-center mt-6 space-x-4">
                    <x-form action="{{ route('two-factor.recovery-codes') }}" method="post">
                        <x-button type="submit" outline>Regenerate Codes</x-button>
                    </x-form>

                    <x-form action="{{ route('two-factor.disable') }}" method="delete">
                        <x-button type="submit" outline>Disable</x-button>
                    </x-form>
                </div>
            @elseif (auth()->user()->two_factor_secret)
                @if (count($errors->confirmTwoFactorAuthentication) > 0)
                    <div class="bg-red-800 py-2 px-4 mb-4">
                        <div class="text-sm leading-5 text-white">
                            <ul class="space-y-1">
                                @foreach ($errors->confirmTwoFactorAuthentication->all() as $error)
                                <li><em class="far fa-caret-right mr-1"></em> {{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
                <p class="text-sm">
                    You will require an authenticator such as Google Authenticator to finish configuring two factor authentication.
                </p>

                <div class="mt-4 border-gray-900 border-2 bg-white inline-block p-2">
                    {!! request()->user()->twoFactorQrCodeSvg() !!}
                </div>

                <x-form action="{{ route('two-factor.confirm') }}" method="post" class="mt-6" id="confirm">
                    <x-input name="code" autofocus label="Confirmation Code" />
                </x-form>

                <div class="flex items-center mt-4 space-x-4">
                    <x-button color="primary" type="submit" form="confirm">Confirm</x-button>

                    <x-form action="{{ route('two-factor.disable') }}" method="delete">
                        <x-button type="submit" outline>Cancel</x-button>
                    </x-form>
                </div>
            @else
                <p class="text-sm">When active a code will be required from an authenticator application to log in.</p>
                <x-form action="{{ route('two-factor.enable') }}" method="post" class="mt-4">
                    <x-button type="submit" outline>Enable</x-button>
                </x-form>
            @endif
        </x-form-section>

        @if (config('session.driver') === 'database')
            <livewire:browser-sessions />
        @endif

        <div class="mt-8 text-right">
            <x-button color="primary" type="submit" class="py-3 px-5" form="account">
                @langAction('update', __('rapture::account.menu_label'))
            </x-button>
        </div>
    </x-container>
</x-dashboard>
