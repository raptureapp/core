window.colorPicker = function(selected) {
    return {
        hex: 'ff0000',
        base: 'rgb(255, 0, 0)',
        opacity: 100,
        selected: selected,

        hue: 0,
        lightness: 0.5,
        saturation: 1,

        x: 100,
        y: 0,
        hueBase: 0,
        opacityEditing: false,
        hueEditing: false,
        paletteEditing: false,

        init() {
            if (!this.selected) {
                return;
            }

            const rgb = this.selected.split("(")[1].split(")")[0].split(",");
            let opacity = 1;

            if (rgb.length > 3) {
                opacity = parseFloat(rgb.pop().trim());

                if (opacity < 1) {
                    rgb.push(opacity * 255);
                }
            }

            this.hex = rgb.map((value) => {
                let hex = parseInt(value.trim(), 10).toString(16);

                if (hex.length == 1) {
                    hex = `0${hex}`;
                }

                return hex;
            }).join('');

            this.hexToHSL();
        },

        opacityStyle() {
            return `background: rgba(0, 0, 0, ${this.opacity / 100}); left: ${this.opacity}%;`;
        },
        hueStyle() {
            return `background: ${this.base}; left: ${this.hueBase}%;`;
        },
        paletteStyle() {
            if (this.opacity < 100) {
                const opacity = this.opacity / 100;
                return `background: linear-gradient(to top, rgba(0, 0, 0, ${opacity}), transparent), linear-gradient(to left, ${this.HSLToRGB(this.hue, 1, 0.5)}, rgba(255, 255, 255, ${opacity}));`;
            }

            return `background: linear-gradient(to top, rgb(0, 0, 0), transparent), linear-gradient(to left, ${this.base}, rgb(255, 255, 255));`;
        },
        palettePointer() {
            return `background-color: ${this.selected}; top: ${this.y}%; left: ${this.x}%;`;
        },

        updateSelection() {
            this.selected = this.HSLToRGB(this.hue, this.saturation, this.lightness);
            this.hex = this.HSLToHex(this.hue, this.saturation, this.lightness);

            this.$el.dispatchEvent(new CustomEvent('input', {
                bubbles: true,
                detail: this.selected,
            }));
        },

        enablePaletteEditing(event) {
            this.updatePalette(event);
            this.paletteEditing = true;
        },
        disablePaletteEditing() {
            this.paletteEditing = false;
        },
        adjustPalette(event) {
            if (this.paletteEditing) {
                this.updatePalette(event);
            }
        },
        updatePalette(event) {
            this.x = Math.round(event.offsetX / event.target.clientWidth * 100);
            this.y = Math.round(event.offsetY / event.target.clientHeight * 100);

            const hsv_value = 1 - (this.y / 100);
            const hsv_saturation = this.x / 100;

            this.lightness = (hsv_value / 2) * (2 - hsv_saturation);
            this.saturation = (hsv_value * hsv_saturation) / (1 - Math.abs(2 * this.lightness - 1));
            this.updateSelection();
        },

        enableHueEditing(event) {
            this.updateHue(event);
            this.hueEditing = true;
        },
        disableHueEditing() {
            this.hueEditing = false;
        },
        adjustHue(event) {
            if (this.hueEditing) {
                this.updateHue(event);
            }
        },
        updateHue(event) {
            const difference = event.offsetX / event.target.clientWidth;

            this.hue = Math.round(difference * 360);
            this.hueBase = Math.round(difference * 100);
            this.base = this.HSLToRGB(this.hue, 1, 0.5, false);
            this.updateSelection();
        },

        enableOpacityEditing(event) {
            this.updateOpacity(event);
            this.opacityEditing = true;
        },
        disableOpacityEditing() {
            this.opacityEditing = false;
        },
        adjustOpacity(event) {
            if (this.opacityEditing) {
                this.updateOpacity(event);
            }
        },
        updateOpacity(event) {
            this.opacity = Math.round(event.offsetX / event.target.clientWidth * 100);
            this.updateSelection();
        },

        HSLToRGB(h, s = 1, l = 0.5, withAlpha = true) {
            const channels = this.convertHSLToRGB(h, s, l);

            if (this.opacity < 100 && withAlpha) {
                const opacity = this.opacity / 100;

                return `rgba(${channels.join(', ')}, ${opacity})`;
            }

            return `rgb(${channels.join(', ')})`;
        },

        HSLToHex(h, s = 1, l = 0.5, withAlpha = true) {
            const channels = this.convertHSLToRGB(h, s, l);

            if (this.opacity < 100 && withAlpha) {
                const opacity = Math.round(this.opacity / 100 * 255);
                channels.push(opacity);
            }

            return channels.map((value) => {
                let hex = value.toString(16);

                if (hex.length == 1) {
                    hex = `0${hex}`;
                }

                return hex;
            }).join('');
        },

        convertHSLToRGB(h, s, l) {
            let c = (1 - Math.abs(2 * l - 1)) * s,
                x = c * (1 - Math.abs((h / 60) % 2 - 1)),
                m = l - c/2,
                r = 0,
                g = 0,
                b = 0;

            if (0 <= h && h < 60) {
                r = c; g = x; b = 0;
            } else if (60 <= h && h < 120) {
                r = x; g = c; b = 0;
            } else if (120 <= h && h < 180) {
                r = 0; g = c; b = x;
            } else if (180 <= h && h < 240) {
                r = 0; g = x; b = c;
            } else if (240 <= h && h < 300) {
                r = x; g = 0; b = c;
            } else if (300 <= h && h < 360) {
                r = c; g = 0; b = x;
            }

            r = Math.round((r + m) * 255);
            g = Math.round((g + m) * 255);
            b = Math.round((b + m) * 255);

            return [r, g, b];
        },

        hexToHSL() {
            if (![3,6,8].includes(this.hex.length)) {
                return;
            }

            const H = this.hex;

            let r = 0, g = 0, b = 0, o = 255;

            if (H.length == 3) {
                r = "0x" + H[0] + H[0];
                g = "0x" + H[1] + H[1];
                b = "0x" + H[2] + H[2];
            } else if (H.length == 6) {
                r = "0x" + H[0] + H[1];
                g = "0x" + H[2] + H[3];
                b = "0x" + H[4] + H[5];
            } else if (H.length == 8) {
                r = "0x" + H[0] + H[1];
                g = "0x" + H[2] + H[3];
                b = "0x" + H[4] + H[5];
                o = "0x" + H[6] + H[7];
            }

            r /= 255;
            g /= 255;
            b /= 255;
            o /= 255;

            let cmin = Math.min(r,g,b),
                cmax = Math.max(r,g,b),
                delta = cmax - cmin,
                h = 0,
                s = 0,
                l = 0;

            if (delta == 0)
                h = 0;
            else if (cmax == r)
                h = ((g - b) / delta) % 6;
            else if (cmax == g)
                h = (b - r) / delta + 2;
            else
                h = (r - g) / delta + 4;

            h = Math.round(h * 60);

            if (h < 0)
                h += 360;

            l = (cmax + cmin) / 2;
            s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));

            this.hue = h;
            this.saturation = s;
            this.lightness = l;

            this.hueBase = Math.round(this.hue / 3.6);
            this.opacity = Math.round(o * 100);
            this.base = this.HSLToRGB(this.hue, 1, 0.5, false);

            const hsv_value = this.saturation * Math.min(this.lightness, 1 - this.lightness) + this.lightness;
            const hsv_saturation = hsv_value ? 2 - 2 * this.lightness / hsv_value : 0;

            this.y = Math.round((1 - hsv_value) * 100);
            this.x = Math.round(hsv_saturation * 100);
        }
    };
}
